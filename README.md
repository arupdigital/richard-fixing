# See Wiki Tab for Project Information

REFERENCES:
-----------

Contributing third-party work includes:

+ [Flask](http://flask.pocoo.org/)
+ [jQuery](http://jquery.com/)
+ [Polymer](http://www.polymer-project.org/)
+ [Highcharts](http://www.highcharts.com/)
+ [Oasys GSA](http://www.oasys-software.com/products/engineering/gsa-suite.html)
+ [pywin32](http://sourceforge.net/projects/pywin32/)
+ [RabbitMQ](http://www.rabbitmq.com/)
+ [AMQP](http://www.amqp.org/)
+ [Celery](http://www.celeryproject.org/)
+ [MySQL](http://www.mysql.com/)
+ [SQLAlchemy](http://www.sqlalchemy.org/)

[Source Code Management @ Arup](http://networks.intranet.arup.com/software/technical/source-code-management/)