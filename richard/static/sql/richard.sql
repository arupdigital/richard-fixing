DROP TABLE if exists richard_v2.section_design;
CREATE TABLE richard_v2.section_design (
  system_id varchar(4) NOT NULL,
  effective_length int(11) NOT NULL AUTO_INCREMENT,
  name varchar(30) NOT NULL,
  section_type varchar(3) NOT NULL,
  fy int(11) NOT NULL,
  E int(11) NOT NULL,
  phi_Pnt BIGINT(20) NOT NULL,
  phi_Pnc BIGINT(20) NOT NULL,
  phi_Vnyy BIGINT(20) NOT NULL,
  phi_Vnzz BIGINT(20) NOT NULL,
  phi_Mnyy BIGINT(20) NOT NULL,
  phi_Mnzz BIGINT(20) NOT NULL,
  PRIMARY KEY (system_id, effective_length, name, section_type),
  KEY name_length_system (name, effective_length, system_id),
) ENGINE=MyISAM AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

DELETE FROM richard_v2.section_design WHERE system_id = 'aisc' AND section_type = 'wf';
LOAD DATA LOCAL INFILE 'C:/Dev/richard-v2/richard/static/sql/section_w_aisc.csv' INTO TABLE richard_v2.section_design FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;
OPTIMIZE TABLE richard_v2.section_design;

DELETE FROM richard_v2.section_design WHERE system_id = 'aisc' AND section_type = 'rhs';
LOAD DATA LOCAL INFILE 'C:/Dev/richard-v2/richard/static/sql/section_rhs_aisc.csv' INTO TABLE richard_v2.section_design FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;
OPTIMIZE TABLE richard_v2.section_design;

DELETE FROM richard_v2.section_design WHERE system_id = 'aisc' AND section_type = 'chs';
LOAD DATA LOCAL INFILE 'C:/Dev/richard-v2/richard/static/sql/section_chs_aisc.csv' INTO TABLE richard_v2.section_design FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;
OPTIMIZE TABLE richard_v2.section_design;

DROP TABLE IF EXISTS richard_v2.section_library;
CREATE TABLE richard_v2.section_library(
  system_id varchar(4) NOT NULL,
  name varchar(30) NOT NULL,
  section_type varchar(4) NOT NULL,
  are bigint(20),
  PRIMARY KEY (system_id, name, section_type)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
LOAD DATA LOCAL INFILE 'C:/Dev/richard-v2/richard/static/sql/section_library.csv' INTO TABLE richard_v2.section_library FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n';

DROP TABLE If Exists richard_v2.app_arg;
CREATE TABLE richard_v2.app_arg (
  module_id varchar(4) NOT NULL,
  system_id varchar(4) NOT NULL,
  arg_id varchar(100) NOT NULL,
  description varchar(1000) DEFAULT NULL,
  validation varchar(100) DEFAULT NULL,
  error varchar(100) DEFAULT NULL,
  data_type varchar(30) DEFAULT NULL,
  field_type varchar(100) DEFAULT NULL,
  units varchar(10) DEFAULT NULL,
  pos int(11) NOT NULL,
  visibility varchar(6) DEFAULT NULL,
  `default` varchar(100) DEFAULT NULL,
  unit_conversion float NOT NULL DEFAULT 1,
  PRIMARY KEY (module_id,system_id,arg_id),
  KEY arg_module_id_system_id_arg_id_units (module_id,system_id,arg_id,units),
  KEY arg_module_id_system_id_arg_id_validation (module_id,system_id,arg_id,validation),
  KEY arg_module_id_system_id_arg_id_data_type (module_id,system_id,arg_id,data_type),
  KEY arg_module_id_system_id_arg_id_error (module_id,system_id,arg_id,error),
  KEY arg_module_id_system_id_arg_id_field_type (module_id,system_id,arg_id,field_type),
  KEY arg_module_id_system_id_arg_id_order (module_id,system_id,arg_id,pos),
  KEY arg_module_id_system_id_arg_id_visibility (module_id,system_id,arg_id,visibility)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
LOAD DATA LOCAL INFILE 'C:/Dev/richard-v2/richard/static/sql/app_arg.csv' INTO TABLE richard_v2.app_arg FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;

DROP TABLE If Exists richard_v2.app_module;
CREATE TABLE richard_v2.app_module (
  module_id varchar(4) NOT NULL,
  name varchar(40) DEFAULT NULL,
  description varchar(200) DEFAULT NULL,
  pos int(11) NOT NULL,
  PRIMARY KEY (module_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
LOAD DATA LOCAL INFILE 'C:/Dev/richard-v2/richard/static/sql/app_module.csv' INTO TABLE richard_v2.app_module FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;

DROP TABLE If Exists richard_v2.app_system;
CREATE TABLE richard_v2.app_system (
  module_id varchar(4) NOT NULL DEFAULT '0',
  system_id varchar(100) NOT NULL,
  title varchar(100) NOT NULL,
  description varchar(500) NOT NULL,
  pos int(11) NOT NULL,
  opt varchar(8) DEFAULT NULL,
  `add` varchar(8) DEFAULT NULL,
  chk varchar(8) DEFAULT NULL,
  colour varchar(7) DEFAULT NULL,
  object varchar(10) DEFAULT NULL,
  position_count int(11) NOT NULL,
  force_x int(11) NOT NULL,
  force_y int(11) NOT NULL,
  force_z int(11) NOT NULL,
  force_srss int(11) NOT NULL,
  moment_xx int(11) NOT NULL,
  moment_yy int(11) NOT NULL,
  moment_zz int(11) NOT NULL,
  moment_srss int(11) NOT NULL,
  PRIMARY KEY (module_id,system_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
LOAD DATA LOCAL INFILE 'C:/Dev/richard-v2/richard/static/sql/app_system.csv' INTO TABLE richard_v2.app_system FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
