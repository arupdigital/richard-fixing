define(function (require) {
  var $ = require('jquery'),
    moment = require('moment'),
    richarts = require('richarts');

  $(function() {

    var template = $('template')[0];
    var schema_name = template.attributes.schemaname.value;

    // get list names for drop downs
    list_types = ["ELEMENT", "NODE", "MEMBER", "CASE"];
    for(var i = 0; i < list_types.length; i++) { getListNames(schema_name, list_types[i]) }

    // get analysis cases for drop downs
    getLoadCaseNames(schema_name);

    // submit form to server on optimize button press
    $('paper-fab#optimize')[0].addEventListener('click', function() { $('form#optimize')[0].submit() });

    // open new task form on new task button press
    $('paper-fab#new_task')[0].addEventListener('click', function() { $('paper-dialog#new_task')[0].open() });

    // toggle collapse on checkbox selection
    $('paper-checkbox').each( function() {
      var module = this.attributes.module.value;
      var system = this.attributes.system.value;
      this.addEventListener('click', function() {
        $("iron-collapse[module=" + module + "][system=" + system + "]")[0].toggle();
      });
    });

    // task selection handler
    $('richard-task-item').each( function() { taskClickHandler(this) });

    // step selection handler
    $("richard-step-item").each( function() { stepClickHandler(this) });

    // initial rendering of task and step status
    $("paper-material.queue > paper-material > richard-task-item, paper-menu > div.content > richard-step-item").each(function() { this.check_status() });

    // module selection
    $('iron-selector[name="module"]')[0].addEventListener('click', function() {
      var module = this.children[this.selected].value;

      // show / hide available checkboxes
      $('paper-checkbox').each( function() {
        if (this.attributes.module.value == module) {
        $(this).show();
        if (this.attributes.system.value == 'glbl') {
          this.checked = true;
        }
        } else {
        $(this).hide();
        if (this.attributes.system.value != 'glbl') {
          this.checked = false;
        }
        }
      });

      // show / hide collapse menus
      $("iron-collapse.system").each( function() {
        if (this.attributes.module.value == module) {
        $(this).show();
        if (this.attributes.system.value == 'glbl') {
          this.opened = true;
        }
        } else {
        $(this).hide();
        if (this.attributes.system.value == 'glbl') {
          this.opened = false;
        }
        }
      });
    });

    // task item selection
    function taskClickHandler(e) {
      e.addEventListener('click', function() {
      $('iron-collapse[taskid=' + e.attributes.taskid.value + ']')[0].toggle();
      });  
    }

    // get update meta data for task step combination
    function updateMeta(schema_name, task_id, step_id) {

      $.ajax({
        url: "/get_step_args",
        data: {
          'task_id': task_id,
          'step_id': step_id,
          'schema_name': schema_name
        },
        async: true,
        success: function (data) {
          $("div.meta").show();
          $("div.args").empty();

          var step = data.step,
              systems = data.systems;

          $("div.args").append("<ul id='step'></ul>");
          for (id in step) {
            $("ul#step").append("<li>" + id + ": " + step[id] + "</li>");
          }

          for (system_id in systems) {
            args = systems[system_id]['args'];
            $("div.args").append("<ul id='" + system_id + "' style='list-style-type: none;'></ul>");
            $("ul#" + system_id).append("<li>system: " + system_id + "</li>");
            for (arg_id in args) {
            $("ul#" + system_id).append("<li>" + arg_id + ": " + args[arg_id] + "</li>");
            }
          }

          $("div.args").append("<a href='/send_gwb/" + schema_name + "/" + task_id + "/" + step_id + "'>gwb</a>");
        }
      });
    }

    // step item selection
    function stepClickHandler(e) {
      e.addEventListener('click', function() {
        var task_id = e.taskid
        var step_id = e.stepid
        var module = e.module
        var systems = e.systems

        template.attributes.taskid.value = task_id
        template.attributes.stepid.value = step_id

        // unselect all other steps
        $("richard-step-item").each( function() {
          if ((this.taskid != task_id) || (this.stepid != step_id)) {
            this.setAttribute("class","x-scope richard-step-item-0");
            this.removeAttribute('aria-selected');
          }
        });

        // display args for system and step combination
        updateMeta(schema_name, task_id, step_id)

        // kill all existing charts
        $("paper-material.chart").remove();

        // get all the charts for the step_id and task_id selected
        $.ajax({
          dataType: "json",
          url: "/get_charts",
          data: {
            'schema_name': schema_name,
            'task_id': task_id,
            'step_id': step_id
          },
          async: true,
          success: function (data) {

            for (chart_id in data) {
              chart = data[chart_id]

              // append individual paper divs to main div
              var html = "<paper-material \
                           elevation='1' \
                           class=chart \
                           task-id=" + task_id + " \
                           step-id=" + step_id + " \
                           chart-id=" + chart_id + " \
                          ></paper-material>";
              $("div#main > div").append(html);

              richarts.draw(chart_id, chart.data);

            }
          }
        });
      });
    }

    // update result_case_id list based on what list_name is selected
    $("select[name='case_list']").click(function() {

      var list_name = this.value
      var system_id = this.parentElement.parentElement.attributes.system.value
      var module_id = this.parentElement.parentElement.attributes.module.value

      $.ajax({
        url: '/get_result_case_ids',
        data: {
          'schema_name': schema_name,
          'list_name': list_name
        },
        async: true,
        success: function (data) {
          result_case_ids = data.json_list;
          $("richard-input[name='midspan_load_result_case_id'][system='" + system_id + "'][module='" + module_id + "']").children('div').children('select').each(function() {
            $(this).empty()
            for(var i = 0; i < result_case_ids.length; i++) {
              $(this).append($('<option value="'+result_case_ids[i]+'"><paper-item>'+result_case_ids[i]+'</paper-item></option>'));
            }
          });
        }
      });
    });

    function getListNames(schema_name, list_type) {
      $.ajax({
        url: '/get_list_names',
        data: {
          'schema_name': schema_name,
          'list_type': list_type
        },
        async: true,
        success: function (data) {
          lists = data.json_list;
          $("select[name$=" + list_type.toLowerCase() + "_list]").each(function() {
            for(var i = 0; i < lists.length; i++) {
              $(this).append($('<option value="'+lists[i]+'"><paper-item>'+lists[i]+'</paper-item></option>'));
            }
          });
        }
      });
    }

    function getLoadCaseNames(schema_name) {
      $.ajax({
        url: '/get_load_cases',
        data: {
          'schema_name': schema_name
        },
        async: true,
        success: function (data) {
          lists = data.json_list;
          $("select[name=node_case_name]").each(function() {
            for(var i = 0; i < lists.length; i++) {
              $(this).append($('<option value="'+lists[i]+'"><paper-item>'+lists[i]+'</paper-item></option>'));
            }
          });
        }
      });
    }

    // listening for updates on tasks
    var source = new EventSource('/update_queue/ ' + schema_name);
    source.onmessage = function(event) {
      var tasks = JSON.parse(event.data);
      console.log(tasks)

      var active = false;
      for (taskid in tasks) {
      task = tasks[taskid];
      task_elem = $("richard-task-item[taskid='" + taskid + "']")[0];
      if (!task_elem) {      
        // append new task element to drawer
        $("paper-material.queue").prepend(      
          "<paper-material elevation='1'><richard-task-item taskid=" + taskid + " module=" + task.module + " status=" + task.status + " systems=" + task.system_ids.toString() + "></richard-task-item></paper-material><iron-collapse taskid=" + taskid + "><paper-menu taskid=" + taskid + " class=step style='background-color: #EEEEEE;'></paper-menu></iron-collapse>"
        );
        task_elem = $("richard-task-item[taskid='" + taskid + "']")[0];
        taskClickHandler(task_elem);
        task_elem.status = task.status;
        task_elem.check_status();
      }
      if (task_elem.status != task.status) {
        task_elem.status = task.status;
        task_elem.check_status();
      } 

      for (stepid in task.steps) {
        step = task.steps[stepid];
        var step_time_0 = moment.utc(step.time_0.toString()).utcOffset(-5).format("DD MMM YYYY, H:mm:ss");
        step_elem = $("richard-step-item[stepid='" + stepid + "'][taskid='" + taskid + "']")[0];
        if (!step_elem) {
        // append new step element to task
        $("paper-menu[taskid='" + taskid + "'] > div.content").prepend(
          "<richard-step-item stepid=" + stepid + " taskid=" + taskid + " primary='" + step_time_0 + "' status=" + step.status + " module=" + task.module + " systems='" + task.systems.toString() + "' tabindex='0'></richard-step-item>"
          );
        step_elem = $("richard-step-item[stepid='" + stepid + "'][taskid='" + taskid + "']")[0];
        stepClickHandler(step_elem);
        step_elem.status = step.status;
        step_elem.check_status();
        }
        step_elem.primary = step_time_0;
        if (step_elem.status != step.status) {
        step_elem.status = step.status;
        step_elem.check_status();
        }
      }
      }
    }
    window.onbeforeunload = function() { source.close() };

    // form submission:
    // build custom dict and submit via ajax post
    // not familiar with how to build dict with standard html form submission
    // cannot post flat list, because of duplicate variable keys across systems
    function submitTask() {

      var task = {'module': null,
            'step_limit': null,
            'systems': {}};

      // check that a module and system are selected
      var invalid = true;
      $('paper-checkbox.system').each(function() {
      var system = this.attributes.system.value;
      if (this.checked) {
        invalid = false;
      }
      });
      if (invalid) {
      console.log('select a system');
      return;
      }

      // check that step limit is specified and valid
      var s = $("paper-input#step_limit")[0];
      if ((s.value == "") || (s.invalid)) {
      console.log('specify a valid step limit');
      return;
      } else {
      task['step_limit'] = s.value;
      }

      task['module'] = $('iron-selector.module').children('.iron-selected')[0].value;

      // for each menu that is expanded
      $('iron-collapse.system[aria-expanded="true"]').each(function() {

        var system = this.attributes.system.value;
        task['systems'][system] = {'args': {}};

        // for each drop_down field in expanded menu
        $(this).children('richard-input[fieldtype=drop_down]').each(function() {
          var drop_down = $(this).children("div").children("select")[0]
          var i = drop_down.selectedIndex;
          var k = drop_down.name;
          var v = drop_down.options[i].value;
          task['systems'][system]['args'][k] = v;
        });

        // for each text field in expanded menu
        $(this).children("richard-input[fieldtype=text]").each(function() {
          var k = this.name;
          var v = this.value;

          // check text validation
          if (this.invalid) {
          console.log(v + " is not valid input for " + k);
          return;
          }

          // check for empty fields
          if (v == "") {
          console.log(k + " is a required input");
          return;
          }

          task['systems'][system]['args'][k] = v;
        });

        // for each file upload in expanded menu
        $(this).children(".file_upload").each(function() {
          $(this).children("form")[0].submit();
        });
      });
      
      $.ajax({
      url: "/new_task",
      data: {
        'task_data': JSON.stringify(task),
        'schema_name': schema_name
      }
      });

      // uncheck all checked boxes and collapse menus
      $('paper-checkbox[aria-checked=true]').each( function() { this.checked = false });
      $('iron-collapse.system[aria-expanded="true"]').each(function() { this.toggle() });

      // $('paper-dialog#new_task')[0]._renderClosed();
      $('paper-dialog#new_task')[0].close();
    }

    // update result_case_id list based on what list_name is selected
    $("paper-button#submit").click(function() { submitTask() });

  });

});