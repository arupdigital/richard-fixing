define(function (require) {
    var $ = require('jquery');

    $(function() {

      // new proj button press
      $("paper-fab#new_proj").click( function() {
        $("paper-dialog#new_proj")[0].open();
      });

      // new project submit
      $("paper-button#new_proj").click( function() {
        $("form#new_proj")[0].submit();
      });

      // delete button hover and press
      $("li.square").each(function() {
        $(this).hover(function() {
          $(this).children("paper-icon-button").fadeIn(100);
        }, function() {
          $(this).children("paper-icon-button").fadeOut(100);
        });

        $(this).children("paper-icon-button").click( function() {
          $("paper-dialog#delete")[0].open();
          $("paper-dialog#delete").find("form")[0].action = "/delete/" + this.id ;
        });
      });

      // delete project confirm
      $("paper-button#delete").click( function() {
        $("form#delete")[0].submit();
      });

      // square button press
      $("paper-button.square").each(function() {
        this.addEventListener('click', function() {
          window.location = "/richard/diary/" + this.id;
        });
      });

    });

});