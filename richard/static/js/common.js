//The build will inline common dependencies into this file.

//For any third party dependencies, like jQuery, place them in the lib folder.

//Configure loading modules from the lib directory,
//except for 'app' ones, which are in a sibling
//directory.
requirejs.config({
    paths: {    	
		jquery: 'node_modules/jquery/dist/jquery',
		highcharts: 'node_modules/highcharts/highcharts',
		drilldown: 'node_modules/highcharts/modules/drilldown',
		heatmap: 'node_modules/highcharts/modules/heatmap',
		data: 'node_modules/highcharts/modules/data',
		moment: 'node_modules/moment/moment',
		richarts: 'node_modules/richarts/richarts',
		templates: '../templates'
    },
	shim: {
		'highcharts': {
			'exports': 'Highcharts',
			'deps': ['jquery']
		},
		'drilldown': {
			'deps': ['highcharts'],
			'exports': 'Highcharts'
		},
		'heatmap': {
			'deps': ['highcharts'],
			'exports': 'Highcharts'
		}
	}
});