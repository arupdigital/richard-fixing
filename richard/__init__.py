#!/usr/bin/env python
import os
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from celery import Celery
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy import create_engine
from richard.database import db_session

app = Flask(__name__)

app.config['SQLALCHEMY_BINDS'] = {'richard': 'mysql+mysqlconnector://readwrite:arupNYsql@10.160.156.226/richard'}

app.config['SCHEMA_NAME'] = ''
app.config.update(
    CELERY_BROKER_URL='amqp://guest:guest@localhost:5672//',
    CELERY_RESULT_BACKEND='amqp://guest:guest@localhost:5672//',
    CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml'],
    CELERY_IMPORTS = ("richard.gsa", ),
    CELERY_RDB_HOST = os.environ.get('CELERY_RDB_HOST') or '127.0.0.1',
    CELERY_RDB_PORT = int(os.environ.get('CELERY_RDB_PORT') or 6899)
)

def make_celery(app):
    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery

celery = make_celery(app)

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

app.secret_key = os.urandom(24)

from richard import views