#!python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'ipython==3.2.0','console_scripts','ipengine'
__requires__ = 'ipython==3.2.0'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('ipython==3.2.0', 'console_scripts', 'ipengine')()
    )
