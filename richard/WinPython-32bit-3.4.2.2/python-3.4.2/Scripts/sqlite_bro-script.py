#!python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'sqlite-bro==0.8.7.4','console_scripts','sqlite_bro'
__requires__ = 'sqlite-bro==0.8.7.4'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('sqlite-bro==0.8.7.4', 'console_scripts', 'sqlite_bro')()
    )
