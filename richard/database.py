from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine import url

sa_config = {
    'drivername': 'mysql+mysqlconnector',
    'host': '10.160.156.226',
    'username': 'readwrite',
    'password': 'arupNYsql'
}

engine = create_engine(url.URL(**sa_config))
session_factory = sessionmaker(autocommit=False,
                               autoflush=True,
                               bind=engine)
db_session = scoped_session(session_factory)

Base = declarative_base()
Base.query = db_session.query_property()


def init_db(schema_name):

    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    import richard.models
    sa_config['database'] = schema_name
    engine_s = create_engine(url.URL(**sa_config))

    Base.metadata.create_all(engine_s)