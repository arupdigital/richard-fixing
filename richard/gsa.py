# -*- coding: utf-8 -*-
"""
Created on Tue Jun	2 15:49:57 2015

@author: thomas.shouler, kristen.strobel
"""
from richard import app, celery
from richard.models import *
from richard.database import init_db, sa_config
from pythoncom import CoInitialize
import win32com.client
import csv
import os
import sys, traceback
from decimal import Decimal, ROUND_UP
import mysql.connector
from mysql.connector.constants import ClientFlag
from time import gmtime, strftime, time
from shutil import copy
from enum import Enum
from os.path import basename
from sqlalchemy.orm import aliased, sessionmaker, scoped_session
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import case
from sqlalchemy.engine import url
from sqlalchemy import and_, desc, asc, distinct, create_engine
from collections import OrderedDict
import math
import pickle
import pdb








# Optimization Wrappers

@celery.task
def model_init(task, step, schema_name):
	"""Initialize new step"""
	sa_config['database'] = schema_name
	engine = create_engine(url.URL(**sa_config))
	session_factory = sessionmaker(autocommit=False, autoflush=True, bind=engine)
	global db_session
	db_session = scoped_session(session_factory)

	global config
	config = {
		'user': 'readwrite',
		'password': 'arupNYsql',
		'host': '10.160.156.226',
		'client_flags': [ClientFlag.LOCAL_FILES]
	}
	config['db'] = schema_name

	global gsa_cache_folder
	dir = os.path.dirname(__file__)
	gsa_cache_folder = os.path.join(dir, 'gsa_cache', schema_name)

	try:
		# build schema
		init_db(schema_name)

		# task is active
		step.status = 'ACTIVE'
		system = System(task.id, step.id, 'glbl', 0, 0)
		db_session.add(task)
		db_session.add(step)
		db_session.add(system)
		db_session.commit()
		
		# Initialize COM
		CoInitialize()

		# create gsa_obj
		gsa_obj = get_gsa_obj(step)
		
		# set units of gsa model
		set_units(gsa_obj, 'mm', 'N')
		
		# todo: clean up gsa to database loading - skip csv step...
		# get input data for gsa model
		get_elements(gsa_obj, step)
		get_members(gsa_obj, step)
		get_nodes(gsa_obj, step)
		get_section_properties(gsa_obj)
		get_materials(gsa_obj)
		get_lists(gsa_obj, step)
		get_node_loads(gsa_obj, step)
		get_dynamic_response_tasks(gsa_obj, step)
		get_analysis_tasks(gsa_obj, step)
		get_analysis_cases(gsa_obj, step)
		get_load_cases(gsa_obj, step)
		get_combination_cases(gsa_obj, step)

		# load input data
		tables = ['element',
					'member',
					'node',
					'material',
					'list',
					'node_load',
					'section_property',
					'dynamic_response_task',
					'analysis_case',
					'analysis_task',
					'load_case',
					'combination_case']
		load_data(tables)

		#delete temporary csv files
		csv_path = os.path.join(gsa_cache_folder, 'element.csv')
		os.remove(csv_path)
		csv_path = os.path.join(gsa_cache_folder, 'member.csv')
		os.remove(csv_path)
		csv_path = os.path.join(gsa_cache_folder, 'node.csv')
		os.remove(csv_path)
		csv_path = os.path.join(gsa_cache_folder, 'material.csv')
		os.remove(csv_path)
		csv_path = os.path.join(gsa_cache_folder, 'list.csv')
		os.remove(csv_path)
		csv_path = os.path.join(gsa_cache_folder, 'node_load.csv')
		os.remove(csv_path)
		csv_path = os.path.join(gsa_cache_folder, 'section_property.csv')
		os.remove(csv_path)
		csv_path = os.path.join(gsa_cache_folder, 'dynamic_response_task.csv')
		os.remove(csv_path)
		csv_path = os.path.join(gsa_cache_folder, 'analysis_case.csv')
		os.remove(csv_path)
		csv_path = os.path.join(gsa_cache_folder, 'analysis_task.csv')
		os.remove(csv_path)
		csv_path = os.path.join(gsa_cache_folder, 'load_case.csv')
		os.remove(csv_path)
		csv_path = os.path.join(gsa_cache_folder, 'combination_case.csv')
		os.remove(csv_path)

		# cache relationships
		node_2_element()
		update_element_geo()
		update_member_geo()

		# insert record into model history on completion of task
		cache_gsa(gsa_obj, step)
		gsa_obj = None

		step.time_1 = gmtime()
		step.status = 'COMPLETE'
		task.status = 'COMPLETE'
		db_session.commit()

	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)
	finally:
		db_session.close()
		engine.dispose()

@celery.task
def model_optimize(schema_name):
	"""Call task to run GSA model"""
	try:
		sa_config['database'] = schema_name
		engine = create_engine(url.URL(**sa_config))
		session_factory = sessionmaker(autocommit=False,
										 autoflush=True,
										 bind=engine)
		global db_session
		db_session = scoped_session(session_factory)

		global config
		config = {
			'user': 'readwrite',
			'password': 'arupNYsql',
			'host': '10.160.156.226',
			'client_flags': [ClientFlag.LOCAL_FILES]
		}
		config['db'] = schema_name

		global gsa_cache_folder
		dir = os.path.dirname(__file__)
		gsa_cache_folder = os.path.join(dir, 'gsa_cache', schema_name)

		for task in db_session.query(Task).filter_by(status='PENDING').order_by(asc('id')).all():
			if task.module_id == 'strg':
				task.status = 'ACTIVE'
				module_strg(task)
			elif task.module_id == 'fomo':
				task.status = 'ACTIVE'
				module_fomo(task)

			task.status = 'COMPLETE'
			db_session.commit()
			db_session.close()

	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)
	
	finally:
		db_session.close()
		engine.dispose()

def module_strg(task):
	"""size model for strength module"""
	
	try:
		# get latest step
		step = db_session.query(Step).filter_by(task_id=task.id).order_by(desc('id')).first()
		step.gwb_path = db_session.query(Step).filter_by(status='COMPLETE').order_by(desc('task_id'),desc('id')).first().gwb_path
		step.status = 'ACTIVE'
		db_session.add(step)
		db_session.commit()

		# Initialize COM
		CoInitialize()
		
		# get gsa obj
		gsa_obj = get_gsa_obj(step)
		
		# cache design elements + cases in data store
		update_systems(gsa_obj, step)
		
		# build elem and case lists
		result_case_ids = []
		for system in db_session.query(System).filter_by(task_id=task.id, step_id=step.id):
			c = [r.id for r in db_session.query(ResultCase).filter_by(system_id=system.id, task_id=task.id)]
			result_case_ids += c
			system.cases = pickle.dumps(c)
			system.nodes = pickle.dumps([r.id for r in db_session.query(Node).filter_by(system_id=system.id, task_id=task.id)])
			system.elements = pickle.dumps([r.id for r in db_session.query(Element).filter_by(system_id=system.id, task_id=task.id)])
		result_case_ids = list(set(result_case_ids))
		db_session.commit()
		
		while True:
			gsa_obj = analyze(gsa_obj, result_case_ids)
			
			# get results
			for system in db_session.query(System).filter_by(task_id=task.id, step_id=step.id):
				get_forces_moments(gsa_obj, step, pickle.loads(system.elements), system.position_count, pickle.loads(system.cases), pickle.loads(system.result_list))
				load_data(['force_moment'])
			
			# calculate demands and utilisations for current elements
			insert_strg_elements()

			update_strg_lengths()
			update_strg_forces()
			update_strg_utils()

			# cache data for current state
			cache_chart_mass(step)
			cache_chart_element_length(step)
			cache_chart_element_section_property(step)
			cache_chart_element_util(step)
			
			# save gsa model prior to analysis
			# save time is fastest without results
			# minimize download time for user
			cache_gsa(gsa_obj, step)
			gsa_obj = None
			
			# get next step
			step = next_step(step, task)
			if step is None: return
			
			# get gsa obj
			gsa_obj = get_gsa_obj(step)
			
			# design new section properties
			update_strg_section_properties()
			
			# calculate design utilisations
			update_strg_design_utils()
			
			# cache new section properties
			update_strg_element_section_properties()
			set_element_gsa(gsa_obj, step)
			set_member_gsa(gsa_obj, step)
			
	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)

def module_fomo(task):
	"""
	dump out forces and moments
	"""
	try:
		task.step_limit = 0

		# get latest step
		step = db_session.query(Step).filter_by(task_id=task.id).order_by(desc('id')).first()
		step.gwb_path = db_session.query(Step).filter_by(status='COMPLETE').order_by(desc('task_id'),desc('id')).first().gwb_path
		step.status = 'ACTIVE'
		db_session.add(step)
		db_session.commit()

		# Initialize COM
		CoInitialize()
			
		# get gsa obj
		gsa_obj = get_gsa_obj(step)

		# cache design elements + cases in data store
		update_systems(gsa_obj, step)
		
		# todo: turn this into its own method?
		# build elem and case lists
		result_case_ids = []
		for system in db_session.query(System).filter_by(task_id=task.id, step_id=step.id):
			c = [r.id for r in db_session.query(ResultCase).filter_by(system_id=system.id, task_id=task.id)]
			result_case_ids += c
			system.cases = pickle.dumps(c)
			system.nodes = pickle.dumps([r.id for r in db_session.query(Node).filter_by(system_id=system.id, task_id=task.id)])
			system.elements = pickle.dumps([r.id for r in db_session.query(Element).filter_by(system_id=system.id, task_id=task.id)])
		result_case_ids = list(set(result_case_ids))
		db_session.commit()

		gsa_obj = analyze(gsa_obj, result_case_ids)

		# get node disp for elements in lists
		for system in db_session.query(System).filter_by(task_id=task.id, step_id=step.id):
			get_forces_moments(gsa_obj, step, pickle.loads(system.elements), system.position_count, pickle.loads(system.cases), pickle.loads(system.result_list))
			load_data(['force_moment'])
		cache_gsa(gsa_obj, step)
		gsa_obj = None

		# get next step
		step = next_step(step, task)
		if step is None: return

	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)

def cache_gsa(gsa_obj, step):
	"""
	Cache Generic Data After a Model Iteration
	rename gwb_path accordingly and save .gwb file
	"""

	# dump out all saved graphic views
	gsa_obj.SaveViewToFile('ALL_SGV','PNG')

	# dump out all saved output views
	gsa_obj.SaveViewToFile('ALL_SOV','CSV')

	# if this is the last task, delete results
	if db_session.query(Task).filter_by(id=step.task_id+1).first() is None: gsa_obj.Delete('RESULTS')

	# if this is not the last step, delete results
	if db_session.query(Task).filter_by(id=step.task_id).first().step_limit != step.id: gsa_obj.Delete('RESULTS')

	# save gwb file in cache for future access
	set_units(gsa_obj, 'mm', 'N')
	gsa_obj.SaveAs(step.gwb_path)
	gsa_obj.Close()
	gsa_obj = None

def next_step(step, task):
	"""
	cache current step and return next step
	"""

	# commit step to history
	step.time_1 = gmtime()
	step.status = 'COMPLETE'
	db_session.commit()

	# if the final step, then return, else continue designing
	if task.step_limit == step.id: return None

	last_step = step
	step = Step(task.id, last_step.id + 1)
	step.gwb_path = last_step.gwb_path
	step.status = 'ACTIVE'
	db_session.add(step)
	db_session.commit()

	for s in db_session.query(System).filter_by(step_id=last_step.id, task_id=last_step.task_id):
		system = System(step.task_id, step.id, s.id, s.position_count, s.result_list)
		system.cases = s.cases
		system.nodes = s.nodes
		system.elements = s.elements
		db_session.add(system)
	db_session.commit()

	return step

def get_gsa_obj(step):
	"""Initialize GSA Object
	"""

	gsa_obj = win32com.client.Dispatch('Gsa_8_7.ComAuto')
	gsa_obj.Open(step.gwb_path)
	step.gwb_path = os.path.join(gsa_cache_folder, str(step.task_id) + '_' + str(step.id) + '.gwb')
	gsa_obj.SaveAs(step.gwb_path)
	
	return gsa_obj



# GSA API Wrappers

def analyze(gsa_obj, cases):
	"""Analyze a GSA Model
	input list of combination and/or analysis cases as strings
	get minimum needed list of analysis tasks to be run for given list of cases
	analyze minimum number of tasks to get results out

	analyze pre-requisite modal tasks for associated response spectrum analyses
	"""
	
	try:
		# build list of analysis cases from composite list of analysis and combination cases
		# if a combination case already has results, ignore it
		anal_case_refs = []
		for case in cases:
			caseop = case[0]
			caseref = case[1:]
			if caseop == 'C':
				results_exist = gsa_obj.CaseResultsExist(caseop, caseref, 0)
				if not results_exist: anal_case_refs += get_anal_cases_from_comb_cases(gsa_obj, case)
			elif caseop == 'A':
				anal_case_refs += [caseref]

		# for all needed analysis cases, determine if they exist
		# if they exist, determine if they already have results
		# if they have results, remove them from the list of cases to be analyzed
		# if any single case does not already exist, continue and analyze everything
		cases_exist = True
		for anal_case_ref in anal_case_refs:
			cases_exist = gsa_obj.CaseExist('A', anal_case_ref)
			if not cases_exist: continue
			#TODO: log error if case doesnt exist
			results_exist = gsa_obj.CaseResultsExist('A', anal_case_ref, 0)
			if results_exist: anal_case_refs.remove(anal_case_ref)

		# delete duplicate analysis cases
		anal_case_refs = list(set(anal_case_refs))
		
		# if analysis tasks are built in the model...
		# find the minimum number of tasks necessary to run
		# if any single analysis task is missing...
		# build all tasks and analyze everything
		if cases_exist:
			anal_tasks = get_tasks_from_anal_cases(gsa_obj, anal_case_refs)

			# analyze pre-requisite modal tasks for associated response spectrum analyses
			for d in db_session.query(DynamicResponseTask):
				if d.id in anal_tasks and d.mode_task not in anal_tasks: gsa_obj.Analyse(d.mode_task)

			for anal_task in anal_tasks: gsa_obj.Analyse(anal_task)
		else: gsa_obj.Analyse()
	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)
	
	return gsa_obj

def get_analysis_cases(gsa_obj, step):
	
	gwa_strings = gsa_obj.GwaCommand("GET_ALL,TASK").split('\n')

	csv_path = os.path.join(gsa_cache_folder, 'analysis_case.csv')
	with open(csv_path,'w') as csv:
		csv.write("task_id, step_id, keyword, id, name, analysis_task_id, description" + '\n')
		if gwa_strings == ['']: return
		for gwa_string in gwa_strings:
			if gwa_string.split(',')[0] == 'ANAL': csv.write(str(step.task_id) + ',' + str(step.id) + ',' + gwa_string + '\n')

def get_analysis_tasks(gsa_obj, step):
	
	gwa_strings = gsa_obj.GwaCommand("GET_ALL,TASK").split('\n')

	csv_path = os.path.join(gsa_cache_folder, 'analysis_task.csv')
	with open(csv_path,'w') as csv:
		csv.write("task_id, step_id, keyword, id, name, stage, solver" + '\n')
		if gwa_strings == ['']: return
		for gwa_string in gwa_strings:
			gwa_list = gwa_string.split(',')
			if gwa_list[0] == 'TASK':csv.write(str(step.task_id) + ',' + str(step.id) + ',' + ','.join(gwa_list[0:5]) + '\n')

def get_anal_cases_from_comb_cases(gsa_obj, comb_cases):
	
	try:
		if not isinstance(comb_cases, list): comb_cases = [comb_cases]

		anal_cases = []

		for comb_case in comb_cases:

			i = comb_case.index('C')
			caseop = comb_case[i]
			caseref = comb_case[i+1:]
			
			# number of permutations in combinations case
			# returns 0 if the combination case is not an enveloping case
			numPerm = gsa_obj.CaseNumPerm(caseop, caseref)
			
			for perm in range(1, numPerm + 1):

				# returns the description of the permutation
				perm_desc = gsa_obj.CasePermDesc(caseop, caseref, perm)
				
				perm_desc_array = perm_desc.split()
				del perm_desc_array[1::2]
				
				for perm_desc_item in perm_desc_array:
					perm_desc_item = perm_desc_item.replace(')','')
					perm_desc_item = perm_desc_item.replace('(','')
					perm_desc_item = perm_desc_item.replace('max','')
					perm_desc_item = perm_desc_item.replace('min','')
					perm_desc_item = perm_desc_item.replace('abs','')
					perm_desc_item = perm_desc_item.replace('signabs','')

					# if combination case, add to input list and continue
					if 'C' in perm_desc_item:
						comb_cases.append(perm_desc_item)
						continue

					# if analysis case, add to output list
					pos_A = perm_desc_item.index('A')
					anal_case = perm_desc_item[+pos_A+1:]
					anal_cases.append(anal_case)
			
		anal_cases = list(set(anal_cases))

	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)
	
	return anal_cases

def get_buckling_load_factors(gsa_obj, step, case_list):
	csv_path = os.path.join(gsa_cache_folder, "buckling_load_factor.csv")
	if not isinstance(case_list, list): case_list = [case_list]
	
	with open(csv_path,'w') as buckling_load_factor:
		try:
			buckling_load_factor.write('task_id,step_id,keyword,case_id,load_factor' + '\n')
			for case in case_list: 
				caseref = case[1:]
				result_string = gsa_obj.GwaCommand("GET,LOAD_FAC," + caseref)
				if result_string == '': continue
				result_string = result_string.split(',')
				result_string[1] = case
				csv_string = ','.join([str(step.task_id), str(step.id)] + result_string)
				buckling_load_factor.write(csv_string + '\n')
			buckling_load_factor.flush()

		except:
			exc_type, exc_value, tb = sys.exc_info()
			db_session.rollback()
			step = db_session.query(Step).filter_by(status='ACTIVE').first()
			task = db_session.query(Task).filter_by(status='ACTIVE').first()
			if step is not None: step.status = 'ERROR'
			if task is not None: task.status = 'ERROR'
			db_session.commit()
			if tb is not None:
				prev = tb
				curr = tb.tb_next
				while curr is not None:
					prev = curr
					curr = curr.tb_next
				print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)

def get_cases_from_desc(gsa_obj, case_desc):

	try:
		case_desc_array = case_desc.split()

		# if case list description is a single item
		if len(case_desc_array) == 1:
			list_items = tuple(case_desc_array)
			return list_items

		comb_cases = ''
		anal_cases = ''
		t0 = case_desc_array[0]
		if 'C' in t0:
			comb_cases += t0
			comb_flag = 1
		elif 'A' in t0:
			anal_cases += t0
			comb_flag = 0
			
		for i in range(0, len(case_desc_array)-1):
			t0 = case_desc_array[i]
			t1 = case_desc_array[i+1]
			
			if 'C' in t0 and 'A' in t1:
				# switch to A
				comb_flag = 0
			elif 'A' in t0 and 'C' in t1:
				# switch to C
				comb_flag = 1
			
			if comb_flag == 1:
				comb_cases += (' ' + t1)
			else:
				anal_cases += (' ' + t1)
		
		# dissolve generic list description for comb cases
		if comb_cases is not '':
			comb_caserefs = comb_cases.replace('C','')
			a,b,caserefs = gsa_obj.EntitiesInList(comb_caserefs, 0)
			if caserefs is None:
				caserefs = tuple(comb_caserefs.replace(' ',''))
			comb_cases = ['C' + str(caseref) for caseref in list(caserefs)] 
		else: comb_cases = []
		
		# dissolve generic list description for anal cases
		if anal_cases is not '':
			anal_caserefs = anal_cases.replace('A','')
			a,b,caserefs = gsa_obj.EntitiesInList(anal_caserefs, 0)
			if caserefs is None:
				caserefs = tuple(anal_caserefs.replace(' ',''))		
			anal_cases = ['A' + str(caseref) for caseref in list(caserefs)]	
		else: anal_cases = []	
		
		list_items = tuple(comb_cases + anal_cases)
	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)
	
	return list_items

def get_load_cases(gsa_obj, step):
	csv_path = os.path.join(gsa_cache_folder, 'load_case.csv')

	with open(csv_path,'w') as csv:
		csv.write("task_id, step_id, keyword, id, name, type, bridge" + '\n')
		gwa_strings = gsa_obj.GwaCommand("GET_ALL,LOAD_TITLE.1").split('\n')
		if gwa_strings == ['']: return
		for gwa_string in gwa_strings:
			csv.write(str(step.task_id) + ',' + str(step.id) + ',' + gwa_string + '\n')

def get_combination_cases(gsa_obj, step):
	csv_path = os.path.join(gsa_cache_folder, 'combination_case.csv')

	with open(csv_path,'w') as csv:
		csv.write("task_id, step_id, keyword, id, name, description, bridge" + '\n')
		gwa_strings = gsa_obj.GwaCommand("GET_ALL,COMBINATION").split('\n')
		if gwa_strings == ['']: return
		for gwa_string in gwa_strings:
			csv.write(str(step.task_id) + ',' + str(step.id) + ',' + gwa_string + '\n')

def get_dynamic_response_tasks(gsa_obj, step):
	csv_path = os.path.join(gsa_cache_folder, 'dynamic_response_task.csv')

	with open(csv_path,'w') as csv:
		csv.write("task_id,step_id,id,name,stage,engine,solver,mode_task,comb,axis,include_x,spectrum_x,modes_x,include_y,spectrum_y,modes_y,include_z,spectrum_z,modes_z,rigorous,rigid,f_rigid,f1,f2,damp_calc,damp_scale,damping,result,source,drift " + '\n')
		gwa_strings = gsa_obj.GwaCommand("GET_ALL,TASK").split('\n')
		if gwa_strings == ['']: return
		for gwa_string in gwa_strings:
			if 'DYNAMIC_RESP' not in gwa_string: continue
			csv.write(str(step.task_id) + ',' + str(step.id) + ',' + gwa_string + '\n')

def get_elements(gsa_obj, step):
	csv_path = os.path.join(gsa_cache_folder, 'element.csv')
	
	with open(csv_path,'w') as element:
		gwa_strings = gsa_obj.GwaCommand("GET_ALL,EL").split('\n')
		element.write('task_id,step_id,system_id,keyword,id,geoname,colour,type,section_property_id,group_id,node_id_1,node_id_2,orient_node,orient_angle,released,release_1,release_2,dummy,member_id' + '\n')
		for gwa_string in gwa_strings:
			if 'TRI3' in gwa_string: continue
			if 'QUAD4' in gwa_string: continue
			gwa_string = gwa_string.replace('NO_RLS', 'RLS,FFFFFF,FFFFFF').replace(',NO_OFFSET', '')
			if len(gwa_string.split(',')) == 9: gwa_string += ',0,0,RLS,FFFFFF,FFFFFF'
			if len(gwa_string.split(',')) == 10: gwa_string += ',0,RLS,FFFFFF,FFFFFF'
			if len(gwa_string.split(',')) == 11: gwa_string += ',RLS,FFFFFF,FFFFFF'
			if 'DUMMY' not in gwa_string: gwa_string += ','
			element_id = gwa_string.split(',')[1]
			member_id = gsa_obj.ElemMembNum(element_id)
			element.write(str(step.task_id) + ',' + str(step.id) + ',,')
			element.write(gwa_string + ',' + str(member_id) + '\n')

def get_forces_moments(gsa_obj, step, element_list, position_count, case_list, result_list):
	"""get force and moment results from GSA model
	"""
	csv_path = os.path.join(gsa_cache_folder, "force_moment.csv")
	
	# for case in case_list:
		# res_flag = gsa_obj.CaseExist(case[:1], int(case[1:]))
		# if res_flag == 0: raise ValueError('Analysis case, ' + case + ', does not exist')

		# res_flag = gsa_obj.CaseResultsExist(case[:1], int(case[1:]), 0)
		# if res_flag == 0: raise ValueError('Results do not exist for case, ' + case)
	
	# add'l positions on a 1D element to extract data for
	additional_positions = max(position_count - 2, 1)	
	
	result_options = ["FX", "FY", "FZ", "FRC", "MXX", "MYY", "MZZ", "MOM"]
	
	with open(csv_path, 'w', newline='') as outfile:
		try:
			outfile.write('task_id,step_id,element_id,position,case_id,envelope_method,force_x,force_y,force_z,force_srss,moment_xx,moment_yy,moment_zz,moment_srss' + '\n')
			writer = csv.writer(outfile, delimiter=',')
			for element_id in element_list:
				if "DUMMY" in gsa_obj.GwaCommand("GET,EL," + str(element_id)): continue
				for case_id in case_list:

					envelope_methods = ['']
					caseop = case_id[0]
					caseref = case_id[1:]
					permutations = gsa_obj.CaseNumPerm(caseop, caseref)
					if permutations > 1: envelope_methods = ['min', 'max']
					for envelope_method in envelope_methods:
						
						result_case = case_id + envelope_method

						for pos in range(0, position_count):
							line = [str(step.task_id), str(step.id)]
							line.extend([element_id, pos, case_id, envelope_method])

							for resOp in result_options:
								if any(resOp == res.upper() for res in result_list):
									dataref = outputRef['REF_FORCE_EL1D_' + resOp].value								
									flag = unicode_to_int(initFlag['OP_INIT_INFINITY'].value) or unicode_to_int(initFlag['OP_INIT_1D_AUTO_PTS'].value)
									res_flag = gsa_obj.Output_Init_Arr(flag, "default", result_case, dataref, additional_positions)
									if res_flag != 0: raise ValueError('Output_Init_Arr failed')
									iStat = gsa_obj.Output_NumElemPos(element_id)								
									if (position_count == 1 or (position_count == 2 and pos == 1)): pos += 1
									value = round_away(gsa_obj.Output_Extract(element_id, pos))

								else: value = ''
								
								line.append(value)
								
							writer.writerow(line)
			outfile.flush()

		except:
			exc_type, exc_value, tb = sys.exc_info()
			db_session.rollback()
			step = db_session.query(Step).filter_by(status='ACTIVE').first()
			task = db_session.query(Task).filter_by(status='ACTIVE').first()
			if step is not None: step.status = 'ERROR'
			if task is not None: task.status = 'ERROR'
			db_session.commit()
			if tb is not None:
				prev = tb
				curr = tb.tb_next
				while curr is not None:
					prev = curr
					curr = curr.tb_next
				print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)

def get_lists(gsa_obj, step):
	csv_path = os.path.join(gsa_cache_folder, 'list.csv')

	with open(csv_path,'w') as csv:
		gwa_strings = gsa_obj.GwaCommand("GET_ALL,LIST").split('\n')
		csv.write("task_id, step_id, keyword, id, name, type, description" + '\n')
		for gwa_string in gwa_strings:
			csv.write(str(step.task_id) + ',' + str(step.id) + ',' + gwa_string + '\n')

def get_list_items(gsa_obj, list_name):
	"""Expand GSA list
	input GSA list name of any type
	return tuple of list entities
	specify list_name='all' to return tuple of all elements in GSA model
	"""
	try:

		if list_name == None:
			return ()

		# get description and type of given list name
		if list_name == 'all':
			list_desc = 'all'
			list_type = gsaEntity['ELEMENT'].value
		else:
			listStrings = gsa_obj.GwaCommand("GET_ALL,LIST").split('\n')
			for listString in listStrings:
				listArray = listString.split(',')
				if listArray[2] == list_name:
					list_type = gsaEntity[listArray[3]].value
					list_desc = listArray[4]
					continue
					
		# if list type is case, use custom function (GSA API does not currently support case list expansion)
		# check if a list is a single entity, if not...
		# use GSA API to get expanded description
		if list_type == 4:
			list_items = get_cases_from_desc(gsa_obj, list_desc)
		else:
			if is_number(list_desc):
				list_items = (int(list_desc),)
			else:
				a,b,list_items = gsa_obj.EntitiesInList(list_desc, list_type)

		return list_items

	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)

def get_nodes(gsa_obj, step):
	csv_path = os.path.join(gsa_cache_folder, 'node.csv')

	with open(csv_path,'w') as node:
		nodeStrings = gsa_obj.GwaCommand("GET_ALL,NODE.1").split('\n')
		node.write('task_id,step_id,system_id,keyword,id,x,y,z,axis,rx,ry,rz,rxx,ryy,rzz,kx,ky,kz,kxx,kyy,kzz' + '\n')
		for nodeString in nodeStrings:
			node.write(str(step.task_id) + ',' + str(step.id) + ',,')
			node.write(nodeString + '\n')

def get_node_displacements(gsa_obj, step, node_list, case_list):

	csv_path = os.path.join(gsa_cache_folder, 'node_displacement.csv')

	result_options = ['REF_DISP_DX',
						'REF_DISP_DY',
						'REF_DISP_DZ',
						'REF_DISP_TRANS']
	flag = unicode_to_int(initFlag['OP_INIT_INFINITY'].value) or unicode_to_int(initFlag['OP_INIT_1D_AUTO_PTS'].value)
	
	with open(csv_path, 'w', newline='') as outfile:
		try:
			outfile.write("task_id, step_id, node_id, case_id, envelope_method, translation_x, translation_y, translation_z, translation_srss" + '\n')
			writer = csv.writer(outfile, delimiter=',')

			for node_id in node_list:

				for case_id in case_list:

					envelope_methods = ['']
					caseop = case_id[0]
					caseref = case_id[1:]
					permutations = gsa_obj.CaseNumPerm(caseop, caseref)
					if permutations > 1: envelope_methods = ['min', 'max']
					for envelope_method in envelope_methods:
						
						result_case = case_id + envelope_method

						line = [str(step.task_id), str(step.id)]
						line.extend([node_id, case_id, envelope_method])

						for result in result_options:
							dataref = outputRef[result].value
							res_flag = gsa_obj.Output_Init(flag, "global", result_case, dataref, 0)
							if res_flag != 0: raise ValueError('Output_Init_Arr failed')
							value = round_away(gsa_obj.Output_Extract(node_id, 0))
							line.append(value)
						
						writer.writerow(line)

		except:
			exc_type, exc_value, tb = sys.exc_info()
			db_session.rollback()
			step = db_session.query(Step).filter_by(status='ACTIVE').first()
			task = db_session.query(Task).filter_by(status='ACTIVE').first()
			if step is not None: step.status = 'ERROR'
			if task is not None: task.status = 'ERROR'
			db_session.commit()
			if tb is not None:
				prev = tb
				curr = tb.tb_next
				while curr is not None:
					prev = curr
					curr = curr.tb_next
					print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)

def get_node_loads(gsa_obj, step):
	csv_path = os.path.join(gsa_cache_folder, 'node_load.csv')

	with open(csv_path,'w') as csv:
		csv.write("task_id, step_id, keyword, name, node_ids, analysis_case, axis, direction, value" + '\n')
		gwa_strings = gsa_obj.GwaCommand("GET_ALL,LOAD_NODE.2").split('\n')
		if gwa_strings == ['']: return
		for gwa_string in gwa_strings:
			csv.write(str(step.task_id) + ',' + str(step.id) + ',' + gwa_string + '\n')

def get_materials(gsa_obj):
	csv_path = os.path.join(gsa_cache_folder, 'material.csv')

	with open(csv_path,'w') as csv:
		csv.write("keyword,id,model,name,colour,,,youngs_modulus,poissons_ratio,density,temperature_coeff,shear_modulus,damping_ratio,,," + '\n')
				
		for id in range(-4, 1):
			gwa_string = gsa_obj.GwaCommand("GET,MAT," + str(id))
			csv.write(gwa_string + '\n')

		gwa_strings = gsa_obj.GwaCommand("GET_ALL,MAT").split('\n')
		for gwa_string in gwa_strings:
			csv.write(gwa_string + '\n')

def get_members(gsa_obj, step):
	csv_path = os.path.join(gsa_cache_folder, 'member.csv')

	with open(csv_path,'w') as csv:
		gwa_strings = gsa_obj.GwaCommand("GET_ALL,MEMB.3").split('\n')
		csv.write("task_id,step_id,system_id,keyword,id,material_type,type,section_property_id,design,restraint,group,node_id_1,node_id_2,orient_node,orient_angle,release_x,release_y,release_z,offset_x,offset_y,offset_z" + '\n')
		for gwa_string in gwa_strings:			
			gwa_string = gwa_string.replace('NO_RLS', 'RLS,FFFFFF,FFFFFF').replace(',NO_OFFSET', '')
			csv.write(str(step.task_id) + ',' + str(step.id) + ',,' + gwa_string + '\n')

def get_section_properties(gsa_obj):
	csv_path = os.path.join(gsa_cache_folder, "section_property.csv")
	
	secPropHighest = gsa_obj.GwaCommand("HIGHEST,PROP_SEC")
	
	with open(csv_path,'w') as section_property:
		section_property.write('keyword,id,geoname,name,material_id,description,area,Iyy,Izz,J,Ky,Kz' + '\n')
		for i in range(1,secPropHighest+1):
			secExists = gsa_obj.GwaCommand("EXIST,PROP_SEC," + str(i))
			if secExists:
				gwa_string = gsa_obj.GwaCommand("GET,SEC_PROP," + str(i))
				gwa_string = gwa_string.replace(",STEEL,",",0,")
				if len(gwa_string.split(',')) > 12:
					gwa_list = gwa_string.split('"')
					gwa_list[1] = gwa_list[1].replace(',', '\,')
					gwa_string = ''.join(gwa_list)
				gwa_string = gwa_string.replace(",STEEL,",",0,")
				section_property.write(gwa_string + '\n')

def get_strain_energy_densities(gsa_obj, step, element_list, position_count, case_list):
	csv_path = os.path.join(gsa_cache_folder, "strain_energy_density.csv")
	
	if not isinstance(element_list, list):
		element_list = [element_list]
		
	if not isinstance(case_list, list):
		case_list = [case_list]
	
	# add'l positions on a 1D element to extract data for
	additional_positions = max(position_count - 2, 1)	
	
	with open(csv_path,'w', newline='') as strain_energy_density:
		try:
			strain_energy_density.write('task_id,step_id,element,position,case_id,sed_total' + '\n')
			writer = csv.writer(strain_energy_density, delimiter=',')
			for element_id in element_list:
				if "DUMMY" in gsa_obj.GwaCommand("GET,EL," + str(element_id)): continue
				for case in case_list:
					for pos in range(0, position_count):

						line = [str(step.task_id), str(step.id)]
						line.extend([element_id, pos, case])
						dataref = outputRef['REF_SED_EL1D_TOT'].value
						
						uni = initFlag['OP_INIT_INFINITY'].value
						flag = unicode_to_int(uni)

						res_flag = gsa_obj.Output_Init_Arr(flag, "default", case, dataref, additional_positions)
						iStat = gsa_obj.Output_NumElemPos(element_id)
						
						if ( position_count == 1 or (position_count == 2 and pos == 1) ):
							pos += 1
							
							value = gsa_obj.Output_Extract(element_id, pos)
							value = round_away(value * 10) / 10
						else:
							value = ''
						
						line.append(value)							
						writer.writerow(line)
			strain_energy_density.flush()

		except:
			exc_type, exc_value, tb = sys.exc_info()
			db_session.rollback()
			step = db_session.query(Step).filter_by(status='ACTIVE').first()
			task = db_session.query(Task).filter_by(status='ACTIVE').first()
			if step is not None: step.status = 'ERROR'
			if task is not None: task.status = 'ERROR'
			db_session.commit()
			if tb is not None:
				prev = tb
				curr = tb.tb_next
				while curr is not None:
					prev = curr
					curr = curr.tb_next
				print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)

def get_tasks_from_anal_cases(gsa_obj, anal_case_refs):
	
	try:
		if not isinstance(anal_case_refs, list):
			anal_case_refs = [anal_case_refss]

		anal_tasks = []
		for anal_case in anal_case_refs:	 
			anal_tasks.append(gsa_obj.CaseTask(anal_case))

		# make list distinct
		anal_tasks = list(set(anal_tasks))
	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)

	return anal_tasks

def set_units(gsa_obj, unit_distance, unit_force):

	gwaForce	= "UNIT_DATA,FORCE," + unit_force
	gwaLength	= "UNIT_DATA,LENGTH," + unit_distance
	gwaDisp	= "UNIT_DATA,DISP," + unit_distance
	gwaSection	= "UNIT_DATA,SECTION," + unit_distance
	
	if unit_force == "kN":
		if unit_distance == "mm":
			unitStress = "GPa"
		elif unit_distance == "m":
			unitStress = "kPa"
	elif unit_force == "N":
		if unit_distance == "mm":
			unitStress = "MPa"
		elif unit_distance == "m":
			unitStress = "Pa"
	elif unit_force == "kip":
		if unit_distance == "ft":
			unitStress = "ksf"
		elif unit_distance == "in":
			unitStress = "ksi"
	elif unit_force == "lbf":
		if unit_distance == "ft":
			unitStress = "psf"
		elif unit_distance == "in":
			unitStress = "psi"

	gwaStress	= "UNIT_DATA,STRESS," + unitStress
	
	# execute GwaCommand
	gsa_obj.GwaCommand(gwaForce)
	gsa_obj.GwaCommand(gwaLength)
	gsa_obj.GwaCommand(gwaDisp)
	gsa_obj.GwaCommand(gwaSection)
	gsa_obj.GwaCommand("UNIT_DATA,MASS,kg")
	gsa_obj.GwaCommand("UNIT_DATA,TIME,s")
	gsa_obj.GwaCommand("UNIT_DATA,TEMP,°C")
	gsa_obj.GwaCommand(gwaStress)
	gsa_obj.GwaCommand("UNIT_DATA,ACCEL,m/s2")
	gsa_obj.GwaCommand("UNIT_DATA,ENERGY,kJ")

def gwb_to_csv(gwb_path):
	"""Parse .gwb to .csv
	"""
	CoInitialize()
	gsa_obj = win32com.client.Dispatch('Gsa_8_7.ComAuto')
	gsa_obj.Open(gwb_path)
	csv_path = gwb_path.replace(".gwb",".csv")
	gsa_obj.SaveAs(csv_path)
	gsa_obj.Close()
	return csv_path



# Global Design

def update_element_geo():
	cnx = mysql.connector.connect(**config)
	cursor = cnx.cursor()

	# update element geometry attributes
	sql = """
		UPDATE element
			LEFT JOIN node node_1 ON (element.node_id_1 = node_1.id)
			LEFT JOIN node node_2 ON (element.node_id_2 = node_2.id) 
		SET 
			element.length = ROUND(SQRT(POW(node_1.x - node_2.x, 2) + POW(node_1.y - node_2.y, 2) + POW(node_1.z - node_2.z, 2)), 2),
			element.length_x = ROUND(node_1.x - node_2.x, 2),
			element.length_y = ROUND(node_1.y - node_2.y, 2),
			element.length_z = ROUND(node_1.z - node_2.z, 2),
			element.length_xy = ROUND(SQRT(POW(node_1.x - node_2.x, 2) + POW(node_1.y - node_2.y, 2)), 2),
			element.length_design_yy = CEILING(SQRT(POW(node_1.x - node_2.x, 2) + POW(node_1.y - node_2.y, 2) + POW(node_1.z - node_2.z, 2)) / 10) * 10,
			element.length_design_zz = CEILING(SQRT(POW(node_1.x - node_2.x, 2) + POW(node_1.y - node_2.y, 2) + POW(node_1.z - node_2.z, 2)) / 10) * 10,
			element.coordinates_1 = CONCAT(node_1.x, ',', node_1.y, ',', node_1.z),
			element.coordinates_2 = CONCAT(node_2.x, ',', node_2.y, ',', node_2.z),
			element.angle_of_incline = IFNULL(ROUND(ATAN(ABS(node_1.z - node_2.z) / SQRT(POW(node_1.x - node_2.x, 2) + POW(node_1.y - node_2.y, 2))) * 180 / PI(), 2), 90);
		"""
	cursor.execute(sql)

	cursor.close()
	cnx.close()

def update_member_geo():
	cnx = mysql.connector.connect(**config)
	cursor = cnx.cursor()

	# update element geometry attributes
	sql = """
			UPDATE member
			JOIN
			 (SELECT 
			 member_id,
				SUM(element.length) AS length,
				SUM(element.length_design_yy) AS length_design_yy,
				SUM(element.length_design_zz) AS length_design_zz,
				SUM(element.length_x) AS length_x,
				SUM(element.length_y) AS length_y,
				SUM(element.length_z) AS length_z,
				SUM(element.length_xy) AS length_xy
			 FROM element
			 GROUP BY member_id) AS temp
			ON temp.member_id = member.id 
			SET 
			 member.length = temp.length,
			 member.length_design_yy = temp.length_design_yy,
			 member.length_design_zz = temp.length_design_zz,
			 member.length_x = temp.length_x,
			 member.length_y = temp.length_y,
			 member.length_z = temp.length_z,
			 member.length_xy = temp.length_xy;
			"""
	cursor.execute(sql)

	cursor.close()
	cnx.close()

def update_systems(gsa_obj, step):
	"""update objects system_id
	"""
	try:
		cnx = mysql.connector.connect(**config)
		cursor = cnx.cursor()

		for a in db_session.query(Arg).filter_by(arg_id='element_list', task_id=step.task_id):
			element_ids = get_list_items(gsa_obj, a.arg_value)
			if element_ids is None: continue
			sql = "UPDATE element SET system_id = '{system_id}', task_id = '{task_id}' WHERE element.id IN {element_ids};" .format(system_id=str(a.system_id), element_ids=str(element_ids), task_id=str(step.task_id))
			cursor.execute(sql)

		# todo add member list input
		for a in db_session.query(Arg).filter_by(arg_id='member_list', task_id=step.task_id):
			member_ids = get_list_items(gsa_obj, a.arg_value)
			if member_ids is None: continue
			sql = "UPDATE member SET system_id = '{system_id}', task_id = '{task_id}' WHERE member.id IN {member_ids};" .format(system_id=str(a.system_id), member_ids=str(member_ids), task_id=str(step.task_id))
			cursor.execute(sql)

		sql = """
				UPDATE element
				JOIN member ON element.member_id = member.id
				JOIN step on step.status = 'ACTIVE'
				JOIN arg on arg.arg_id = 'member_list' AND arg.system_id = member.system_id
				SET element.system_id = member.system_id,
					element.task_id = step.task_id,
					element.step_id = step.id;
				"""
		cursor.execute(sql)

		for a in db_session.query(Arg).filter_by(arg_id='case_list', task_id=step.task_id):
			case_ids = get_list_items(gsa_obj, a.arg_value)
			if case_ids is None: continue
			for c in list(case_ids):
				case = ResultCase(a.task_id, a.system_id, c)
				db_session.add(case)
		db_session.commit()

		for a in db_session.query(Arg).filter_by(arg_id='node_list', task_id=step.task_id):
			node_ids = get_list_items(gsa_obj, a.arg_value)
			sql = "UPDATE node SET system_id = '{system_id}', task_id = '{task_id}' WHERE node.id IN {node_ids};" .format(system_id=str(a.system_id), node_ids=str(node_ids), task_id=str(step.task_id))
			cursor.execute(sql)

		cursor.close()
		cnx.close()

	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)

def set_member_gsa(gsa_obj, step):
	"""
	Avoid a MemoryError loading large results sets by
	using 'yield_per()' to batch ORM results into subcollections
	"""
	try:
		count = 0
		gsa_obj.Delete('RESULTS')
		for e in db_session.query(Member).filter_by(task_id=step.task_id, step_id=step.id).yield_per(1000).enable_eagerloads(False):
			try: gsa_obj.GwaCommand(e.gwaSet)
			except: print('error_line=' + str(tb.tb_lineno), 'member=' + str(e))

			count += 1
			if count == 100000:
				gsa_obj.Save()
				count = 0

		gsa_obj.Save()

	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)

def set_element_gsa(gsa_obj, step):
	"""
	Avoid a MemoryError loading large results sets by
	using 'yield_per()' to batch ORM results into subcollections
	"""
	try:
		count = 0
		gsa_obj.Delete('RESULTS')
		for e in db_session.query(Element).filter_by(task_id=step.task_id, step_id=step.id).yield_per(500).enable_eagerloads(False):
			try: gsa_obj.GwaCommand(e.gwaSet)
			except: print('error_line=' + str(tb.tb_lineno), 'element=' + str(e))

			count += 1
			if count == 100000:
				gsa_obj.Save()
				count = 0

		gsa_obj.Save()

	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)

def element_2_member_section_properties():
	"""global design
	"""
	cnx = mysql.connector.connect(**config)
	cursor = cnx.cursor()

	sql = """
			UPDATE member
			JOIN
				(SELECT 
				member.id as member_id,
				element.task_id,
				element.step_id,
				MAX(element.section_property_id) AS section_property_id
				FROM element
				JOIN member ON member.id = element.member_id
				GROUP BY member.id) AS temp 
			ON member.id = temp.member_id
			SET member.section_property_id = temp.section_property_id,
				member.task_id = temp.task_id,
				member.step_id = temp.step_id;
			"""
	cursor.execute(sql)

	cursor.close()
	cnx.close()

def member_2_element_section_properties():
	"""global design
	"""
	cnx = mysql.connector.connect(**config)
	cursor = cnx.cursor()

	sql = """
			UPDATE element
			JOIN member
			ON member.id = element.member_id
			SET element.section_property_id = member.section_property_id,
				element.task_id = member.task_id,
				element.step_id = member.step_id;
			"""
	cursor.execute(sql)

	cursor.close()
	cnx.close()



# Strength Design Algorithm

def insert_strg_elements():
	"""strg design module
	"""
	cnx = mysql.connector.connect(**config)
	cursor = cnx.cursor()

	# insert elements into design table
	sql = """
			INSERT INTO design_strg (task_id, step_id, system_id, section_type, element_id, member_id, section_property_id, result_case_id, position, envelope_method)
			SELECT
				element.task_id,
				step.id,
				element.system_id,
				section_library.section_type,
				element.id,
				element.member_id,
				element.section_property_id,
				force_moment.result_case_id,
				force_moment.position,
				force_moment.envelope_method
			FROM element
			JOIN step ON step.status = 'ACTIVE' AND element.task_id = step.task_id
			JOIN force_moment
				ON element.id = force_moment.element_id
				AND step.task_id = force_moment.task_id
				AND step.id = force_moment.step_id
			JOIN result_case
					ON result_case.task_id = force_moment.task_id
					AND result_case.id = force_moment.result_case_id
					AND result_case.system_id = element.system_id
			JOIN section_property ON element.section_property_id = section_property.id
			JOIN richard_v2.section_library ON element.system_id = section_library.system_id AND section_property.name = section_library.name;
			"""
	cursor.execute(sql)

	cursor.close()
	cnx.close()

def update_strg_lengths():
	"""strg design module
	"""
	cnx = mysql.connector.connect(**config)
	cursor = cnx.cursor()

	# update length values
	sql = """
			UPDATE design_strg
			JOIN step
				ON status = 'ACTIVE'
				AND step.id = design_strg.step_id
				AND step.task_id = design_strg.task_id
			JOIN element
					ON element.id = design_strg.element_id
					AND design_strg.system_id IN ('aisc')
			JOIN arg effective_length_factor
				ON effective_length_factor.system_id = element.system_id
				AND effective_length_factor.task_id = element.task_id
				AND effective_length_factor.arg_id = 'effective_length'
			LEFT JOIN member on design_strg.member_id = member.id
			SET design_strg.effective_length_yy = ROUND(greatest(element.length_design_yy, ifnull(member.length_design_yy, 0)) * effective_length_factor.arg_value / 10) * 10,
				design_strg.effective_length_zz = ROUND(greatest(element.length_design_zz, ifnull(member.length_design_zz, 0)) * effective_length_factor.arg_value / 10) * 10;
			"""
	cursor.execute(sql)

	cursor.close()
	cnx.close()

def update_strg_forces():
	"""strg design module
	"""
	cnx = mysql.connector.connect(**config)
	cursor = cnx.cursor()

	# update tension values
	sql = """
			UPDATE design_strg
			JOIN step 
				ON step.status = 'ACTIVE' 
				AND step.id = design_strg.step_id 
				AND step.task_id = design_strg.task_id
			JOIN system 
				ON system.task_id = step.task_id 
				AND design_strg.system_id = system.id 
				AND system.id IN ('aisc')
			JOIN force_moment 
				ON force_moment.element_id = design_strg.element_id
				AND force_moment.task_id = design_strg.task_id
				AND force_moment.step_id = design_strg.step_id
				AND force_moment.position = design_strg.position
				AND force_moment.result_case_id = design_strg.result_case_id
				AND force_moment.envelope_method = design_strg.envelope_method
			SET design_strg.tension = GREATEST(force_moment.force_x, 0),
				design_strg.compression = ABS(LEAST(force_moment.force_x, 0)),
				design_strg.shear_y = ABS(force_moment.force_y),
				design_strg.shear_z = ABS(force_moment.force_z),
				design_strg.moment_yy = ABS(force_moment.moment_yy),
				design_strg.moment_zz = ABS(force_moment.moment_zz);
			"""
	cursor.execute(sql)

	cursor.close()
	cnx.close()

def update_strg_utils():
	"""strg design module
	"""
	cnx = mysql.connector.connect(**config)
	cursor = cnx.cursor()

	# update util values
	sql = """
			UPDATE design_strg
				JOIN section_property 
						ON design_strg.section_property_id = section_property.id
				JOIN richard_v2.section_design AS section_design_yy
					ON section_property.name = section_design_yy.name
					AND design_strg.effective_length_yy = section_design_yy.effective_length
					AND section_design_yy.system_id = design_strg.system_id
				JOIN richard_v2.section_design AS section_design_zz
					ON section_property.name = section_design_zz.name
					AND design_strg.effective_length_zz = section_design_zz.effective_length
					AND section_design_zz.system_id = design_strg.system_id
				JOIN step
					ON status = 'ACTIVE'
					AND step.id = design_strg.step_id
					AND step.task_id = design_strg.task_id
			SET 
				util_Pt = design_strg.tension / section_design_yy.phi_Pnt,
				util_Pcy = design_strg.compression / section_design_yy.phi_Pnc,
				util_Pcz = design_strg.compression / section_design_zz.phi_Pnc,
				util_Vy = design_strg.shear_y / section_design_yy.phi_Vnyy,
				util_Vz = design_strg.shear_z / section_design_zz.phi_Vnzz,
				util_Myy = design_strg.moment_yy / section_design_yy.phi_Mnyy,
				util_Mzz = design_strg.moment_zz / section_design_zz.phi_Mnzz;
			"""
	cursor.execute(sql)

	# calculate combined utilizations
	sql = """
			UPDATE design_strg
			SET util_Ct = 
				CASE WHEN (util_Pt >= 0.2 AND system_id = 'aisc') THEN util_Pt + 8 * (util_Myy + util_Mzz) / 9
					 WHEN (util_Pt < 0.2 AND system_id = 'aisc') THEN 0.5 * util_Pt + util_Myy + util_Mzz
				END,
				util_Ccy = 
				CASE WHEN (util_Pcy >= 0.2 AND system_id = 'aisc') THEN util_Pcy + 8 * (util_Myy + util_Mzz) / 9
					 WHEN (util_Pcy < 0.2 AND system_id = 'aisc') THEN 0.5 * util_Pcy + util_Myy + util_Mzz
				END,
				util_Ccz = 
				CASE WHEN (util_Pcz >= 0.2 AND system_id = 'aisc') THEN util_Pcz + 8 * (util_Myy + util_Mzz) / 9
					 WHEN (util_Pcz < 0.2 AND system_id = 'aisc') THEN 0.5 * util_Pcz + util_Myy + util_Mzz
				END;
			"""
	cursor.execute(sql)
	
	cursor.close()
	cnx.close()

def update_strg_section_properties():
	"""strg design module
	"""
	
	cnx = mysql.connector.connect(**config)
	cursor = cnx.cursor()

	# Finds the section with the lowest section_property_id that meets strength requirements
	sql = """
			UPDATE design_strg
			JOIN(SELECT design_strg.element_id,
								design_strg.task_id,
								design_strg.step_id,
								design_strg.result_case_id,
								design_strg.position,
								design_strg.envelope_method,
								min(section_property.id) AS section_property_id
							FROM design_strg
							JOIN step ON status = 'active'
								AND step.id - 1 = design_strg.step_id
								AND step.task_id = design_strg.task_id
							JOIN arg AS target_util
								ON target_util.system_id = design_strg.system_id
								AND target_util.task_id = design_strg.task_id
								AND target_util.arg_id = 'target_util'
							JOIN richard_v2.section_design AS section_design_y
								ON section_design_y.system_id = design_strg.system_id
								AND design_strg.section_type = section_design_y.section_type
								AND design_strg.effective_length_yy = section_design_y.effective_length
								AND design_strg.tension / section_design_y.phi_Pnt <= target_util.arg_value
								AND design_strg.compression / section_design_y.phi_Pnc <= target_util.arg_value
								AND design_strg.moment_yy / section_design_y.phi_Mnyy <= target_util.arg_value
								AND design_strg.shear_z / section_design_y.phi_Vnzz
							JOIN richard_v2.section_design AS section_design_z
								ON section_design_z.system_id = design_strg.system_id
								AND design_strg.section_type = section_design_z.section_type
								AND design_strg.effective_length_zz = section_design_z.effective_length
								AND section_design_y.name = section_design_z.name
								AND design_strg.compression / section_design_z.phi_Pnc <= target_util.arg_value
								AND design_strg.moment_zz / section_design_z.phi_Mnzz <= target_util.arg_value
								AND design_strg.shear_y / section_design_z.phi_Vnyy
							JOIN section_property ON section_design_y.name = section_property.name
							WHERE CASE WHEN design_strg.tension / section_design_y.phi_Pnt >= 0.2 
									THEN (design_strg.tension / section_design_y.phi_Pnt + 8 * (design_strg.moment_yy / section_design_y.phi_Mnyy + design_strg.moment_zz / section_design_z.phi_Mnzz) / 9) 
									ELSE (0.5 * design_strg.tension / section_design_y.phi_Pnt + design_strg.moment_yy / section_design_y.phi_Mnyy + design_strg.moment_zz / section_design_z.phi_Mnzz) 
									END <= target_util.arg_value
								AND CASE WHEN design_strg.compression / section_design_y.phi_Pnc >= 0.2 
										THEN (design_strg.compression / section_design_y.phi_Pnc + 8 * (design_strg.moment_yy / section_design_y.phi_Mnyy + design_strg.moment_zz / section_design_z.phi_Mnzz) / 9) 
										ELSE (0.5 * design_strg.compression / section_design_y.phi_Pnc + design_strg.moment_yy / section_design_y.phi_Mnyy + design_strg.moment_zz / section_design_z.phi_Mnzz) 
										END <= target_util.arg_value
								AND CASE WHEN design_strg.compression / section_design_z.phi_Pnc >= 0.2 
										THEN (design_strg.compression / section_design_z.phi_Pnc + 8 * (design_strg.moment_yy / section_design_y.phi_Mnyy + design_strg.moment_zz / section_design_z.phi_Mnzz) / 9) 
										ELSE (0.5 * design_strg.compression / section_design_z.phi_Pnc + design_strg.moment_yy / section_design_y.phi_Mnyy + design_strg.moment_zz / section_design_z.phi_Mnzz) 
										END <= target_util.arg_value
							GROUP BY design_strg.element_id, design_strg.task_id, design_strg.step_id, design_strg.result_case_id, design_strg.position, design_strg.envelope_method) AS temp
					ON design_strg.element_id = temp.element_id 
							AND design_strg.task_id = temp.task_id
							AND design_strg.step_id = temp.step_id
							AND design_strg.result_case_id = temp.result_case_id
							AND design_strg.position = temp.position
							AND design_strg.envelope_method = temp.envelope_method
			SET design_strg.design_section_property_id = temp.section_property_id;
			"""
	cursor.execute(sql)

	# if there is nothing in the catalogue large enough to meet the demand, then give it the biggest size (by area) with matching system and section type that is in the gsa section property list
	sql = """
			UPDATE design_strg
			JOIN(SELECT design_strg.element_id,
						step.id - 1 AS step_id,
						step.task_id AS task_id,
						design_strg.system_id,
						max(section_property.id) AS section_property_id 
				FROM design_strg
				JOIN step ON status = 'ACTIVE'
					AND step.id - 1 = design_strg.step_id
					AND step.task_id = design_strg.task_id
				JOIN section_property
				JOIN richard_v2.section_library 
					ON design_strg.system_id = section_library.system_id
					AND design_strg.section_type = section_library.section_type
					AND section_library.name = section_property.name
				WHERE design_strg.design_section_property_id IS NULL
				GROUP BY design_strg.element_id) AS max_prop
				ON design_strg.element_id = max_prop.element_id
				AND design_strg.step_id = max_prop.step_id
				AND design_strg.task_id = max_prop.task_id
				AND design_strg.system_id = max_prop.system_id
			SET design_strg.design_section_property_id = max_prop.section_property_id;
			"""
	cursor.execute(sql)

	cursor.close()
	cnx.close()

def update_strg_design_utils():
	"""strg design module
	"""
	cnx = mysql.connector.connect(**config)
	cursor = cnx.cursor()

	# update design util values
	sql = """
			UPDATE design_strg
				JOIN section_property 
						ON design_strg.design_section_property_id = section_property.id
				JOIN richard_v2.section_design AS section_design_yy
					ON section_property.name = section_design_yy.name
					AND design_strg.effective_length_yy = section_design_yy.effective_length
					AND section_design_yy.system_id = design_strg.system_id
				JOIN richard_v2.section_design AS section_design_zz
					ON section_property.name = section_design_zz.name
					AND design_strg.effective_length_zz = section_design_zz.effective_length
					AND section_design_zz.system_id = design_strg.system_id
				JOIN step
					ON status = 'ACTIVE'
					AND step.id - 1 = design_strg.step_id
					AND step.task_id = design_strg.task_id
			SET 
				design_util_Pt = design_strg.tension / section_design_yy.phi_Pnt,
				design_util_Pcy = design_strg.compression / section_design_yy.phi_Pnc,
				design_util_Pcz = design_strg.compression / section_design_zz.phi_Pnc,
				design_util_Vy = design_strg.shear_y / section_design_yy.phi_Vnyy,
				design_util_Vz = design_strg.shear_z / section_design_zz.phi_Vnzz,
				design_util_Myy = design_strg.moment_yy / section_design_yy.phi_Mnyy,
				design_util_Mzz = design_strg.moment_zz / section_design_zz.phi_Mnzz;
			"""
	cursor.execute(sql)

	# calculate combined utilizations
	sql = """
			UPDATE design_strg
			SET design_util_Ct = 
				CASE WHEN (design_util_Pt >= 0.2 AND system_id = 'aisc') THEN design_util_Pt + 8 * (design_util_Myy + design_util_Mzz) / 9
					 WHEN (design_util_Pt < 0.2 AND system_id = 'aisc') THEN 0.5 * design_util_Pt + design_util_Myy + design_util_Mzz
				END,
				design_util_Ccy = 
				CASE WHEN (design_util_Pcy >= 0.2 AND system_id = 'aisc') THEN design_util_Pcy + 8 * (design_util_Myy + design_util_Mzz) / 9
					 WHEN (design_util_Pcy < 0.2 AND system_id = 'aisc') THEN 0.5 * design_util_Pcy + design_util_Myy + design_util_Mzz
				END,
				design_util_Ccz = 
				CASE WHEN (design_util_Pcz >= 0.2 AND system_id = 'aisc') THEN design_util_Pcz + 8 * (design_util_Myy + design_util_Mzz) / 9
					 WHEN (design_util_Pcz < 0.2 AND system_id = 'aisc') THEN 0.5 * design_util_Pcz + design_util_Myy + design_util_Mzz
				END;
			"""
	cursor.execute(sql)
	
	cursor.close()
	cnx.close()
	
def update_strg_element_section_properties():
	"""strg design module
	"""
	cnx = mysql.connector.connect(**config)
	cursor = cnx.cursor()

	# update ELEMENTS
	sql = """
			UPDATE element
			JOIN (SELECT
					element.id as element_id,
					step.task_id as task_id,
					step.id as step_id,
					MAX(design_strg.design_section_property_id) AS design_section_property_id
				FROM
					element
				JOIN step
					ON status = 'ACTIVE'
				JOIN design_strg
					ON step.id - 1 = design_strg.step_id
					AND step.task_id = design_strg.task_id
					AND element.id = design_strg.element_id
				WHERE design_strg.system_id IN ('aisc')
			GROUP BY design_strg.element_id) AS temp
			ON element.id = temp.element_id 
			SET 
				element.section_property_id = design_section_property_id,
				element.task_id = temp.task_id,
				element.step_id = temp.step_id
			WHERE design_section_property_id <> element.section_property_id;
			"""
	cursor.execute(sql)
	element_2_member_section_properties()
	
	# update MEMBERS
	sql = """
			UPDATE member
			JOIN (SELECT
					member.id as member_id,
					step.task_id as task_id,
					step.id as step_id,
					MAX(design_strg.design_section_property_id) AS design_section_property_id
				FROM
					member
				JOIN step
					ON step.status = 'ACTIVE'
				JOIN design_strg
					ON step.id - 1 = design_strg.step_id
					AND step.task_id = design_strg.task_id
					AND member.id = design_strg.member_id
				WHERE design_strg.system_id IN ('aisc')
			GROUP BY design_strg.member_id) AS temp
				ON member.id = temp.member_id 
			SET 
				member.section_property_id = design_section_property_id,
				member.task_id = temp.task_id,
				member.step_id = temp.step_id
			WHERE design_section_property_id <> member.section_property_id;
			"""
	cursor.execute(sql)
	member_2_element_section_properties()

	cursor.close()
	cnx.close()



# CHARTING

def cache_chart_element_length(step):
	"""
	build json for length chart
	element data is only held for most recent step
	"""

	try:
		cnx = mysql.connector.connect(**config)
		cursor = cnx.cursor()

		sql = """
				SELECT DISTINCT ROUND(length / 100, 0) * 100
				FROM element
				WHERE system_id <> ''
				ORDER BY ROUND(length / 100, 0) * 100 ASC;
				"""
		cursor.execute(sql)

		chart = {'categories': [i[0] for i in cursor],
				 'series': []}

		for s in db_session.query(distinct(System.id)).filter_by(task_id=step.task_id).order_by(asc('id')):
			system_id = s[0]
			sql = ("SELECT temp_2.count "
					 "FROM"
					 "		(SELECT DISTINCT ROUND(length / 100, 0) * 100 AS length"
					 "		FROM element"
					 "		WHERE system_id <> ''"
					 "		ORDER BY ROUND(length / 100, 0) * 100 ASC) AS temp_1 "
					 "LEFT JOIN"
					 "		(SELECT system_id, COUNT(element.id) AS count, ROUND(length / 100, 0) * 100 AS length"
					 "		FROM element"
					 "		WHERE system_id = '" + system_id + "'"
					 "		GROUP BY element.system_id , ROUND(length / 100, 0) * 100) AS temp_2 "
					 "ON temp_1.length = temp_2.length")
			cursor.execute(sql)
			chart['series'].append({
				'name': system_id,
				'data': [i[0] for i in cursor]
			})

		db_session.add(Chart(step.task_id, step.id, 'element_length', pickle.dumps(chart)))
		db_session.commit()

	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)
	finally:
		cursor.close()
		cnx.close()

def cache_chart_mass(step):
	"""
	build json for mass chart
	for every system in current task, get mass
	"""
	
	try:
		cnx = mysql.connector.connect(**config)
		cursor = cnx.cursor()
		sql = """
				UPDATE system
				JOIN
					(SELECT 
						step.id as step_id,
									step.task_id,
						system_id,
						ROUND(SUM(length * section_property.area * material.rho)) as mass
					FROM element
					JOIN step ON status = 'ACTIVE' AND element.task_id = step.task_id
					JOIN section_property ON element.section_property_id = section_property.id
					JOIN material ON section_property.material_id = material.id
					WHERE system_id <> ''
					GROUP BY element.system_id) as temp
				ON system.id = temp.system_id
				AND system.step_id = temp.step_id
				AND system.task_id = temp.task_id
				SET system.mass = temp.mass;
				"""
		cursor.execute(sql)

		# build json #
		##############
		# chart = {'categories': [0, 1, 2, 3], 'series': [{'name': 'cchs', 'data': [0.3, 8.41, 9.3, 11.27]}]}
		# length of chart['categories'] = length of chart['series'][i]['data']

		sql = """SELECT DISTINCT step_id FROM system JOIN task ON task.status = 'ACTIVE' and task.id = task_id;"""
		cursor.execute(sql)
		
		chart = {'categories': [i[0] for i in cursor],
				 'series': []}

		for s in db_session.query(distinct(System.id)).filter_by(task_id=step.task_id).order_by(asc('id')):
			system_id = s[0]
			chart['series'].append({
				'name': system_id,
				'data': [round(m[0]/1000, 2) for m in db_session.query(System.mass).filter_by(task_id=step.task_id, id=system_id).order_by(asc('step_id'))]
			})

		db_session.add(Chart(step.task_id, step.id, 'mass', pickle.dumps(chart)))
		db_session.commit()

	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)
	finally:
		cursor.close()
		cnx.close()

def cache_chart_element_section_property(step):
	"""
	build json for length chart
	element data is only held for most recent step
	"""

	try:
		cnx = mysql.connector.connect(**config)
		cursor = cnx.cursor()

		chart = {'series':[]}
		
		for s in db_session.query(distinct(System.id)).filter_by(task_id=step.task_id).order_by(asc('id')):
			system_id = s[0]
			
			sql = ("SELECT section_property.name,"
					"	count(element.id) AS count "
					"FROM element "
					"JOIN section_property ON section_property.id = element.section_property_id "
					"WHERE element.system_id = '" + system_id + "' "
					"GROUP BY element.section_property_id;")
					
			cursor.execute(sql)
			temp = cursor.fetchall()
			
			chart['series'].append({'name': system_id,'data': temp})

		db_session.add(Chart(step.task_id, step.id, 'element_section_property', pickle.dumps(chart)))
		db_session.commit()

	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)
	finally:
		cursor.close()
		cnx.close()

def cache_chart_element_util(step):
	""" 
	BUILD ELEMENT UTILIZATION CHART DATA
	"""
	
	try:
		cnx = mysql.connector.connect(**config)
		cursor = cnx.cursor()

		util_cat = ['Pt','Pcy','Pcz','Vy','Vz','Myy','Mzz','Ct','Ccy','Ccz']
		chart = {'categories':[],'drilldown':[]}
		for s in db_session.query(distinct(System.id)).filter_by(task_id=step.task_id).order_by(asc('id')):
			system_id = s[0]
			# COUNT NUMBER OF ELEMENTS
			sql = ("SELECT COUNT(0) "
					"FROM(SELECT DISTINCT element.id AS elems"
					"		FROM element"
					"		WHERE system_id = '" + system_id + "') AS temp_1;")
			cursor.execute(sql)
			nels = cursor.fetchall()

			# COUNT WHAT % OF ELEMENTS ARE CONTROLLED BY EACH LIMIT CASE FOR EACH SYSTEM
			sql = ("SELECT temp_2.nPt / " + str(nels[0][0]) + " * 100 AS pPt,"
					"		temp_2.nPcy / " + str(nels[0][0]) + " * 100 AS pPcy,"
					"		temp_2.nPcz / " + str(nels[0][0]) + " * 100 AS pPcz,"
					"		temp_2.nPcy / " + str(nels[0][0]) + " * 100 AS pVy,"
					"		temp_2.nPcz / " + str(nels[0][0]) + " * 100 AS pVz,"
					"		temp_2.nMyy / " + str(nels[0][0]) + " * 100 AS pMyy,"
					"		temp_2.nMzz / " + str(nels[0][0]) + " * 100 AS pMzz,"
					"		temp_2.nCt / " + str(nels[0][0]) + " * 100 AS pCt,"
					"		temp_2.nCcy / " + str(nels[0][0]) + " * 100 AS pCcy,"
					"		temp_2.nCcz / " + str(nels[0][0]) + " * 100 AS pCcz "
					"FROM(SELECT SUM(CASE WHEN temp_1.util_Pt_max = temp_1.util_max THEN 1 ELSE 0 END) AS nPt,"
					"						SUM(CASE WHEN temp_1.util_Pcy_max = temp_1.util_max THEN 1 ELSE 0 END) AS nPcy,"
					"						SUM(CASE WHEN temp_1.util_Pcz_max = temp_1.util_max THEN 1 ELSE 0 END) AS nPcz,"
					"						SUM(CASE WHEN temp_1.util_Vy_max = temp_1.util_max THEN 1 ELSE 0 END) AS nVy,"
					"						SUM(CASE WHEN temp_1.util_Vz_max = temp_1.util_max THEN 1 ELSE 0 END) AS nVz,"
					"						SUM(CASE WHEN temp_1.util_Myy_max = temp_1.util_max THEN 1 ELSE 0 END) AS nMyy,"
					"						SUM(CASE WHEN temp_1.util_Mzz_max = temp_1.util_max THEN 1 ELSE 0 END) AS nMzz,"
					"						SUM(CASE WHEN temp_1.util_Ct_max = temp_1.util_Pt_max THEN 0 WHEN temp_1.util_Ct_max = temp_1.util_Myy_max THEN 0 WHEN temp_1.util_Ct_max = temp_1.util_Mzz_max THEN 0 WHEN temp_1.util_Ct_max = temp_1.util_max THEN 1 ELSE 0 END) AS nCt,"
					"						SUM(CASE WHEN temp_1.util_Ccy_max = temp_1.util_Pcy_max THEN 0 WHEN temp_1.util_Ccy_max = temp_1.util_Myy_max THEN 0 WHEN temp_1.util_Ccy_max = temp_1.util_Mzz_max THEN 0 WHEN temp_1.util_Ccy_max = temp_1.util_max THEN 1 ELSE 0 END) AS nCcy,"
					"						SUM(CASE WHEN temp_1.util_Ccz_max = temp_1.util_Pcz_max THEN 0 WHEN temp_1.util_Ccz_max = temp_1.util_Myy_max THEN 0 WHEN temp_1.util_Ccz_max = temp_1.util_Mzz_max THEN 0 WHEN temp_1.util_Ccz_max = temp_1.util_max THEN 1 ELSE 0 END) AS nCcz"
					"		FROM"
					"				(SELECT element_id,"
					"						GREATEST(MAX(COALESCE(util_Pt, 0)), MAX(COALESCE(util_Pcy, 0)), MAX(COALESCE(util_Pcz, 0)), MAX(COALESCE(util_Myy, 0)), MAX(COALESCE(util_Mzz, 0)), MAX(COALESCE(util_Ct, 0)), MAX(COALESCE(util_Ccy, 0)), MAX(COALESCE(util_Ccz, 0))) AS util_max,"
					"						MAX(COALESCE(util_Pt, 0)) AS util_Pt_max,"
					"						MAX(COALESCE(util_Pcy, 0)) AS util_Pcy_max,"
					"						MAX(COALESCE(util_Pcz, 0)) AS util_Pcz_max,"
					"						MAX(COALESCE(util_Vy, 0)) AS util_Vy_max,"
					"						MAX(COALESCE(util_Vz, 0)) AS util_Vz_max,"
					"						MAX(COALESCE(util_Myy, 0)) AS util_Myy_max,"
					"						MAX(COALESCE(util_Mzz, 0)) AS util_Mzz_max,"
					"						MAX(COALESCE(util_Ct, 0)) AS util_Ct_max,"
					"						MAX(COALESCE(util_Ccy, 0)) AS util_Ccy_max,"
					"						MAX(COALESCE(util_Ccz, 0)) AS util_Ccz_max"
					"		FROM design_strg"
					"		JOIN step ON step.status = 'ACTIVE'"
					"				AND design_strg.task_id = step.task_id"
					"				AND design_strg.step_id = step.id"
					"		WHERE design_strg.system_id = '" + system_id + "'"
					"		GROUP BY element_id) AS temp_1) AS temp_2;")

			cursor.execute(sql)
			util_qty = cursor.fetchall()

			# PUT THIS INTO A CHART
			temp = []
			for i in range(len(util_cat)):			
				temp.append({'name': util_cat[i],'y': util_qty[0][i],'drilldown': (system_id + "_" + util_cat[i])})
			
			chart['categories'].append({'name': system_id, 'data': temp})

			#COUNT HOW MANY ELEMENTS HAVE EACH UTILIZATION FOR EACH SYSTEM AND EACH LIMIT CASE TO POPULATE DRILLDOWN CHARTS
			for i in range(len(util_cat)):
				temp_util = util_cat[i]
				sql = ("SELECT (ROUND(temp_1.util_" + temp_util + ", 2)) AS util,"
						"	IFNULL(COUNT(0), 0) / " + str(nels[0][0]) + " * 100 AS count "
						"FROM(SELECT element_id,"
						"	step.id AS step_id,"
						"	step.task_id AS task_id,"
						"	system_id,"
						"	IFNULL(MAX(util_Pt), 0) AS util_Pt,"
						"	IFNULL(MAX(util_Pcy), 0) AS util_Pcy,"
						"	IFNULL(MAX(util_Pcz), 0) AS util_Pcz,"
						"	IFNULL(MAX(util_Vy), 0) AS util_Vy,"
						"	IFNULL(MAX(util_Vz), 0) AS util_Vz,"
						"	IFNULL(MAX(util_Myy), 0) AS util_Myy,"
						"	IFNULL(MAX(util_Mzz), 0) AS util_Mzz,"
						"	IFNULL(MAX(util_Ct), 0) AS util_Ct,"
						"	IFNULL(MAX(util_Ccy), 0) AS util_Ccy,"
						"	IFNULL(MAX(util_Ccz), 0) AS util_Ccz "
						"FROM design_strg"
						"	JOIN step ON step.status = 'ACTIVE'"
						"	AND design_strg.step_id = step.id"
						"	AND design_strg.task_id = step.task_id"
						" WHERE design_strg.system_id = '" + system_id + "'"
						"	GROUP BY element_id) AS temp_1"
						"	GROUP BY system_id, util;")
				cursor.execute(sql)
				util_qty = cursor.fetchall()
				temp = []
				for j in range(len(util_qty)):
					temp.append((str(util_qty[j][0]),util_qty[j][1]))
				chart['drilldown'].append({'id': (system_id + "_" + temp_util), 'data': temp})
				
		db_session.add(Chart(step.task_id,step.id,'element_util',pickle.dumps(chart)))
		db_session.commit()
	except:
		exc_type, exc_value, tb = sys.exc_info()
		db_session.rollback()
		step = db_session.query(Step).filter_by(status='ACTIVE').first()
		task = db_session.query(Task).filter_by(status='ACTIVE').first()
		if step is not None: step.status = 'ERROR'
		if task is not None: task.status = 'ERROR'
		db_session.commit()
		if tb is not None:
			prev = tb
			curr = tb.tb_next
			while curr is not None:
				prev = curr
				curr = curr.tb_next
			print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)
	finally:
		cursor.close()
		cnx.close()



# Database management
	
def get_module():
	"""returns dict of app arguments
	"""
	sa_config['database'] = 'richard_v2'
	engine = create_engine(url.URL(**sa_config))
	Session = sessionmaker(bind=engine)
	db_session = Session()

	modules = {}
	for m in AppModule.query.order_by(asc('pos')).all():
		module = {'systems': {},
					'args': {},
					'name': m.name,
					'description': m.description}

		for sy in AppSystem.query.filter_by(module_id=m.module_id).order_by(asc('pos')).all():
			system = {
						'args': {},
						'title': sy.title,
						'description': sy.description,
						'colour': sy.colour,
						'opt': sy.opt,
						'add': sy.add,
						'chk': sy.chk
					 }

			for a in AppArg.query.filter_by(module_id=m.module_id,system_id=sy.system_id).order_by(asc('pos')).all():
				arg = {
						 'description': a.description,
						 'validation': a.validation,
						 'error': a.error,
						 'data_type': a.data_type,
						 'field_type': a.field_type,
						 'units': a.units,
						 'visibility': a.visibility,
						 'default': a.default
						}

				system['args'][a.arg_id] = arg
			system['args'] = OrderedDict(sorted(system['args'].items()))
			module['systems'][sy.system_id] = system

		for aa in db_session.query(AppArg.arg_id, AppArg.description, AppArg.data_type, AppArg.visibility, func.group_concat(AppSystem.colour).label('systems')).\
							 filter(AppArg.module_id==m.module_id).\
							 join(AppSystem, and_(AppArg.system_id==AppSystem.system_id, AppSystem.module_id==m.module_id)).\
							 group_by(AppArg.arg_id).\
							 order_by(AppArg.pos.asc()):
			aa.systems = aa.systems.split(',')
			module['args'][aa.arg_id] = aa

		modules[m.module_id] = module

	db_session.close()
	engine.dispose()

	return modules

def get_tasks(schema_name, ordered=True):
	"""returns dict of current task structure
	"""

	sa_config['database'] = schema_name
	engine = create_engine(url.URL(**sa_config))
	Session = sessionmaker(bind=engine)
	db_session = Session()

	tasks = {}
	for t in db_session.query(Task).order_by(desc('id')).all():
		task = {'module': t.module_id,
				'status' : t.status,
				'systems' : {},
				'steps': {},
				'system_ids': []}

		for sy in db_session.query(System).filter_by(task_id=t.id, step_id=0).all():
			system = {'task_id': t.id,
						'module': t.module_id,
						'args': {}}

			for a in db_session.query(Arg).filter_by(task_id=t.id,system_id=sy.id).all():
				system['args'][a.arg_id] = a.arg_value

			for a in db_session.query(AppSystem).filter_by(system_id=sy.id, module_id=t.module_id).all():
				result_list = []
				if a.force_x == 1: result_list.append('fx')
				if a.force_y == 1: result_list.append('fy')
				if a.force_z == 1: result_list.append('fz')
				if a.force_srss == 1: result_list.append('frc')
				if a.moment_xx == 1: result_list.append('mxx')
				if a.moment_yy == 1: result_list.append('myy')
				if a.moment_zz == 1: result_list.append('mzz')
				if a.moment_srss == 1: result_list.append('mom')
				system['result_list'] = result_list
				system['position_count'] = a.position_count

			task['systems'][sy.id] = system
			task['system_ids'].append(sy.id)

		task['system_ids'] = ','.join(task['system_ids'])

		for st in db_session.query(Step).filter_by(task_id=t.id).all():
			step = {'task_id': st.task_id,
					'status': st.status,
					'time_0': str(st.time_0),
					'time_1': str(st.time_1)}

			task['steps'][st.id] = step
		if ordered: task['steps'] = order_dict(task['steps'])

		tasks[t.id] = task
	if ordered: tasks = order_dict(tasks)

	db_session.close()
	engine.dispose()

	return tasks

def create_db(schema_name):
	config = {
		'user': 'readwrite',
		'password': 'arupNYsql',
		'host': '10.160.156.226',
		'client_flags': [ClientFlag.LOCAL_FILES]
	}
	cnx = mysql.connector.connect(**config)
	cursor = cnx.cursor()

	query = "DROP DATABASE IF EXISTS `" + schema_name + "`;CREATE DATABASE IF NOT EXISTS `" + schema_name + "`;CREATE TABLE `" + schema_name + "`._meta (meta int)"

	sqlList = query.split(';')

	for sql in sqlList:
		cursor.execute(sql)

	cursor.close()
	cnx.close()

def load_data(tables):

	cnx = mysql.connector.connect(**config)	
	cursor = cnx.cursor()
	
	# build sql load data script file
	filename = os.path.join(gsa_cache_folder, "insert_input.sql")
	file = open(filename, 'w')
	for table in tables: file.write("LOAD DATA LOCAL INFILE '" + os.path.join(gsa_cache_folder, table + '.csv').replace('\\','/') + "' INTO TABLE "+table+" FIELDS TERMINATED BY ',' ESCAPED BY '\\\\' LINES TERMINATED BY '\\n' IGNORE 1 LINES;")
	file.close()

	# execute load data script file
	file = open(filename, 'r')
	sqlList = file.read().split(';')
	for sql in sqlList: cursor.execute(sql)
	file.close()
	
	cursor.close()	
	cnx.close()

def get_schemata():
	schemata = {}

	config = {
		'user': 'readwrite',
		'password': 'arupNYsql',
		'host': '10.160.156.226',
		'client_flags': [ClientFlag.LOCAL_FILES]
	}
	config['db'] = 'information_schema'
	cnx = mysql.connector.connect(**config)
	cursor = cnx.cursor()
	query = "SELECT DISTINCT TABLE_SCHEMA FROM TABLES WHERE table_name in ('_meta')"
	cursor.execute(query)
	for row in cursor: schemata[row[0]] = None	

	for schema in schemata:
		query = "SELECT count(id) FROM `{schema}`.step WHERE status = 'ACTIVE'".format(schema=schema)
		try:
			cursor.execute(query)
			for row in cursor:
				if row[0] == 0: schemata[schema] = 'COMPLETE'
				else: schemata[schema] = 'ACTIVE'
		except: schemata[schema] = 'ACTIVE'

	cursor.close()
	cnx.close()

	return schemata

def node_2_element():
	"""At each node finds connected elements and x, y, z vector to opposite end of connecting element"""
	
	cnx = mysql.connector.connect(**config)	
	cursor = cnx.cursor()
	
	# build node_2_element mapper
	sql = """
			INSERT INTO node_2_element 
			SELECT 
				node.task_id,
				node.step_id,
				node.id AS node_id,
				element.id AS element_id,
				node_prime.x - node.x as element_x,
				node_prime.y - node.y as element_y,
				node_prime.z - node.z as element_z
			FROM
				node
			JOIN element ON (node.id = element.node_id_2)
			JOIN node node_prime ON (element.node_id_1 = node_prime.id)
			UNION
			SELECT 
				node.task_id,
				node.step_id,
				node.id AS node_id,
				element.id AS element_id,
				node_prime.x - node.x as element_x,
				node_prime.y - node.y as element_y,
				node_prime.z - node.z as element_z
			FROM
				node
			JOIN element ON (node.id = element.node_id_1)
			JOIN node node_prime ON (element.node_id_2 = node_prime.id);
			"""

	cursor.execute(sql)	
	cursor.close()	
	cnx.close()



# Generic Functions

def order_dict(d, asc=False):

	od = OrderedDict(sorted(d.items()))
	if asc: return od

	items = list(od.items())
	items.reverse()
	od = OrderedDict(items)
	return od

def unique(items):
	found = set()
	keep = []

	for item in items:
		if item not in found:
			found.add(item)
			keep.append(item)

	return keep

def is_number(s):
	try:
		float(s)
		return True
	except ValueError:
		return False

def round_away(d):
	if d is None: return None
	num = Decimal(d).quantize(Decimal('1.'), rounding=ROUND_UP)
	num = int(num)
	return num 
	
def unicode_to_int(string):
	intList = [ord(char) for char in string]
	ref = int("".join(str(i) for i in intList))
	return ref



# GSA Enums

class gsaEntity(Enum):
	NODE = 1
	ELEMENT = 2
	MEMBER = 3
	CASE = 4
	GRID_PT = 5
	LINE = 6
	AREA = 7
	REGION = 8

class initFlag(Enum):
	
	OP_INIT_2D_BOTTOM = '+H1'								# output 2D stresses at bottom layer
	OP_INIT_2D_MIDDLE = '+H2'								# output 2D stresses at middle layer
	OP_INIT_2D_TOP = '+H4'								# output 2D stresses at top layer
	OP_INIT_2D_BENDING = '+H8'								# output 2D stresses at bending layer
	OP_INIT_2D_AVGE = '+H10'								# average 2D element stresses at nodes
	OP_INIT_1D_AUTO_PTS = '+H20'						# calculate 1D results at interesting points
	OP_INIT_INFINITY = '+H40'								# return infinity and NaN values as such, else as zero
	OP_INIT_1D_WALL_RES_SECONDARY = '+H80'				# output secondary stick of wall equivalent beam results, else primary

	# REF_FOOTFALL_RESON = 12009000
	REF_FOOTFALL_RESON_RESP_FACTOR = 12009001
	REF_FOOTFALL_RESON_PEAK_VELOCITY = 12009002
	REF_FOOTFALL_RESON_RMS_VELOCITY = 12009003
	REF_FOOTFALL_RESON_PEAK_ACC = 12009004
	REF_FOOTFALL_RESON_RMS_ACC = 12009005
	REF_FOOTFALL_RESON_CRIT_NODE = 12009006
	REF_FOOTFALL_RESON_CRIT_FREQ = 12009007

	# REF_FOOTFALL_TRANS = 12009100
	REF_FOOTFALL_TRANS_RESP_FACTOR = 12009101
	REF_FOOTFALL_TRANS_PEAK_VELOCITY = 12009102
	REF_FOOTFALL_TRANS_RMS_VELOCITY = 12009103
	REF_FOOTFALL_TRANS_PEAK_ACC = 12009104
	REF_FOOTFALL_TRANS_RMS_ACC = 12009105
	REF_FOOTFALL_TRANS_CRIT_NODE = 12009106
	REF_FOOTFALL_TRANS_CRIT_FREQ = 12009107

	# REF_FOOTFALL_SUM = 12009200
	REF_FOOTFALL_SUM_RESON_RESP_FACTOR = 12009201
	REF_FOOTFALL_SUM_TRANS_RESP_FACTOR = 12009202
	REF_FOOTFALL_SUM_MAX_RESP_FACTOR = 12009203

class outputRef(Enum):
	
	#REF_FIRST_MODEL
	
	#REF_FIRST_GR_ENT
	
	#REF_EL_1D_SHAPE
	REF_EL_1D_SHAPE_PER_LEN = 801001
	REF_EL_1D_SHAPE_AREA_PER_LEN = 801002
	
	#REF_EL_2D_SHAPE
	REF_EL_2D_SHAPE_ANG = 802001
	REF_EL_2D_SHAPE_WARP = 802002
	REF_EL_2D_SHAPE_PROJ_AX = 802003
	
	#REF_LAST_GR_ENT
	
	REF_AXIS = 1001000
	REF_AXIS_NAME = 1001001
	REF_AXIS_TYPE = 1001002
	REF_AXIS_OX = 1001000
	REF_AXIS_OY = 1001001
	REF_AXIS_OZ = 1001002
	REF_AXIS_XX = 1001000
	REF_AXIS_XY = 1001001
	REF_AXIS_XZ = 1001002
	REF_AXIS_XYX = 1001000
	REF_AXIS_XYY = 1001001
	REF_AXIS_XYZ = 1001000
	
	REF_NODE = 1002000
	REF_NODE_NAME = 1002001
	REF_NODE_X = 1002002
	REF_NODE_Y = 1002003
	REF_NODE_Z = 1002004
	REF_NODE_COOR = 1002005
	REF_NODE_AXIS = 1002006
	REF_NODE_RESTR = 1002007
	REF_NODE_GEN_RESTR = 1002008
	REF_NODE_STF = 1002009
	
	REF_SUPT = 1003000
	REF_SUPT_AXIS = 1003001
	REF_SUPT_RESTR = 1003002
	REF_SUPT_STF_TRANS_X = 1003003
	REF_SUPT_STF_TRANS_Y = 1003004
	REF_SUPT_STF_TRANS_Z = 1003005
	REF_SUPT_STF_TRANS_VECT = 1003006
	REF_SUPT_STF_ROT_XX = 1003007
	REF_SUPT_STF_ROT_YY = 1003008
	REF_SUPT_STF_ROT_ZZ = 1003009
	REF_SUPT_STF_ROT_VECT = 1003010
	
	REF_ELEM = 1011000
	REF_ELEM_NAME = 1011001
	REF_ELEM_TYPE = 1011002
	REF_ELEM_PROP = 1011003
	REF_ELEM_GROUP = 1011004
	REF_ELEM_NODE = 1011005
	REF_ELEM_ANGLE = 1011006
	REF_ELEM_VERT = 1011007
	REF_ELEM_LEN = 1011008
	REF_ELEM_TOPO = 1011009
	
	REF_ELEM_RLS = 1012000
	REF_ELEM_RLS_X = 1012001
	REF_ELEM_RLS_Y = 1012002
	REF_ELEM_RLS_Z = 1012003
	REF_ELEM_RLS_XX = 1012004
	REF_ELEM_RLS_YY = 1012005
	REF_ELEM_RLS_ZZ = 1012006
	
	REF_ELEM_OFFSET = 1013000
	REF_ELEM_OFFSET_X = 1013001
	REF_ELEM_OFFSET_Y = 1013002
	REF_ELEM_OFFSET_Z = 1013003
	REF_ELEM_OFFSET_VECT = 1013004
	
	REF_ELEM_DC = 1014000
	REF_ELEM_DC_XX = 1014001
	REF_ELEM_DC_XY = 1014002
	REF_ELEM_DC_XZ = 1014003
	REF_ELEM_DC_X = 1014004
	REF_ELEM_DC_YX = 1014005
	REF_ELEM_DC_YY = 1014006
	REF_ELEM_DC_YZ = 1014007
	REF_ELEM_DC_Y = 1014008
	REF_ELEM_DC_ZX = 1014009
	REF_ELEM_DC_ZY = 1014010
	REF_ELEM_DC_ZZ = 1014011
	REF_ELEM_DC_Z = 1014012
	
	REF_MEMB = 1021000
	REF_MEMB_NAME = 1021001
	REF_MEMB_TYPE = 1021002
	REF_MEMB_PROP = 1021003
	REF_MEMB_DES_PROP = 1021004
	REF_MEMB_REST_PROP = 1021005
	REF_MEMB_BAR_LAYOUT_LEFT = 1021006
	REF_MEMB_BAR_LAYOUT_MIDDLE = 1021007
	REF_MEMB_BAR_LAYOUT_RIGHT = 1021008
	REF_MEMB_GROUP = 1021009
	REF_MEMB_NODE = 1021010
	REF_MEMB_ANGLE = 1021011
	REF_MEMB_VERT = 1021012
	REF_MEMB_LEN = 1021013
	REF_MEMB_TOPO = 1021014
	
	REF_MEMB_RLS = 1022000
	REF_MEMB_RLS_X = 1022001
	REF_MEMB_RLS_Y = 1022002
	REF_MEMB_RLS_Z = 1022003
	REF_MEMB_RLS_XX = 1022004
	REF_MEMB_RLS_YY = 1022005
	REF_MEMB_RLS_ZZ = 1022006
	
	REF_MEMB_OFFSET = 1023000
	REF_MEMB_OFFSET_X = 1023001
	REF_MEMB_OFFSET_Y = 1023002
	REF_MEMB_OFFSET_Z = 1023003
	REF_MEMB_OFFSET_VECT = 1023004
	
	REF_RC_BEAM = 1025000
	REF_RC_BEAM_MEMB = 1025001
	REF_RC_BEAM_SUP_FIX = 1025002
	REF_RC_BEAM_LWR_SEC = 1025003
	REF_RC_BEAM_LWR_LEN = 1025004
	REF_RC_BEAM_LWR_FIX = 1025005
	REF_RC_BEAM_UPR_SEC = 1025006
	REF_RC_BEAM_UPR_LEN = 1025007
	REF_RC_BEAM_UPR_FIX = 1025008
	
	REF_GRID_PLANE = 1031000
	REF_GRID_PLANE_NAME = 1031001
	REF_GRID_PLANE_TYPE = 1031002
	REF_GRID_PLANE_AXIS = 1031003
	REF_GRID_PLANE_ELEV = 1031004
	REF_GRID_PLANE_LIST = 1031005
	REF_GRID_PLANE_TOL = 1031006
	REF_GRID_PLANE_BELOW = 1031007
	REF_GRID_PLANE_ABOVE = 1031008
	REF_GRID_PLANE_SPAN_TYPE = 1031009
	REF_GRID_PLANE_SPAN_ANG = 1031010
	REF_GRID_PLANE_EXPANSION = 1031011
	REF_GRID_PLANE_AREA = 1031012
	
	REF_STOREY = 1032000
	REF_STOREY_NAME = 1032001
	REF_STOREY_ELEV = 1032002
	REF_STOREY_ABOVE = 1032003
	REF_STOREY_BELOW = 1032004
	
	REF_POLYLINE_2D = 1033000
	REF_POLYLINE_2D_NAME = 1033001
	REF_POLYLINE_2D_PERIM = 1033002
	REF_POLYLINE_2D_AREA = 1033003
	REF_POLYLINE_2D_CENTRE_X = 1033004
	REF_POLYLINE_2D_CENTRE_Y = 1033005
	REF_POLYLINE_2D_X = 1033006
	REF_POLYLINE_2D_Y = 1033007
	REF_POLYLINE_2D_LENGTH = 1033008
	
	#REF_FIRST_GR_PROP									
	
	REF_ND_SPR_STF = 1041000
	REF_ND_SPR_STF_TRANS_X = 1041001
	REF_ND_SPR_STF_TRANS_Y = 1041002
	REF_ND_SPR_STF_TRANS_Z = 1041003
	REF_ND_SPR_STF_TRANS_VECT = 1041004
	REF_ND_SPR_STF_ROT_XX = 1041005
	REF_ND_SPR_STF_ROT_YY = 1041006
	REF_ND_SPR_STF_ROT_ZZ = 1041007
	REF_ND_SPR_STF_ROT_VECT = 1041008
	
	REF_EL_MAT = 1041500
	REF_EL_MAT_E = 1041501
	REF_EL_MAT_NU = 1041502
	REF_EL_MAT_G = 1041503
	REF_EL_MAT_RHO = 1041504
	REF_EL_MAT_ALPHA = 1041505
	REF_EL_MAT_DAMP = 1041506
	
	REF_EL_PROP_BEAM = 1042000
	REF_EL_PROP_BEAM_AREA = 1042001
	REF_EL_PROP_BEAM_IYY = 1042002
	REF_EL_PROP_BEAM_IZZ = 1042003
	REF_EL_PROP_BEAM_TORS = 1042004
	REF_EL_PROP_BEAM_KY = 1042005
	REF_EL_PROP_BEAM_KZ = 1042006
	REF_EL_PROP_BEAM_A_KY = 1042007
	REF_EL_PROP_BEAM_A_KZ = 1042008
	REF_EL_PROP_BEAM_E_A = 1042009
	REF_EL_PROP_BEAM_E_IYY = 1042010
	REF_EL_PROP_BEAM_E_IZZ = 1042011
	REF_EL_PROP_BEAM_G_J = 1042012
	REF_EL_PROP_BEAM_G_A_KY = 1042013
	REF_EL_PROP_BEAM_G_A_KZ = 1042014
	
	REF_EL_PROP_BEAM_MOD = 1042500
	REF_EL_PROP_BEAM_MOD_AREA = 1042501
	REF_EL_PROP_BEAM_MOD_IYY = 1042502
	REF_EL_PROP_BEAM_MOD_IZZ = 1042503
	REF_EL_PROP_BEAM_MOD_TORS = 1042504
	REF_EL_PROP_BEAM_MOD_KY = 1042505
	REF_EL_PROP_BEAM_MOD_KZ = 1042506
	
	REF_EL_PROP_MASS = 1043000
	REF_EL_PROP_MASS_MASS = 1043001
	
	REF_EL_PROP_2DEL = 1044000
	REF_EL_PROP_2DEL_THK = 1044001
	REF_EL_PROP_2DEL_BENDING = 1044002
	REF_EL_PROP_2DEL_INPLANE = 1044003
	REF_EL_PROP_2DEL_WEIGHT = 1044004
	REF_EL_PROP_2DEL_MASS = 1044005
	
	REF_EL_ENV_IMPACT_1D = 1051000
	REF_EL_ENV_IMPACT_1D_E = 1051001
	REF_EL_ENV_IMPACT_1D_CO2 = 1051002
	REF_EL_ENV_IMPACT_1D_RC = 1051003
	REF_EL_ENV_IMPACT_1D_USER = 1051004
	
	REF_EL_ENV_IMPACT_2D = 1051100
	REF_EL_ENV_IMPACT_2D_E = 1051101
	REF_EL_ENV_IMPACT_2D_CO2 = 1051102
	REF_EL_ENV_IMPACT_2D_RC = 1051103
	REF_EL_ENV_IMPACT_2D_USER = 1051104
	
	REF_EL_ENV_IMPACT_MEMB = 1051200
	REF_EL_ENV_IMPACT_MEMB_E = 1051201
	REF_EL_ENV_IMPACT_MEMB_CO2 = 1051202
	REF_EL_ENV_IMPACT_MEMB_RC = 1051203
	REF_EL_ENV_IMPACT_MEMB_USER = 1051204
	
	#REF_LAST_GR_PROP
	
	REF_MAT_STD = 1061000
	REF_MAT_STD_NAME = 1061001
	REF_MAT_STD_TYPE = 1061002
	REF_MAT_STD_E = 1061003
	REF_MAT_STD_NU = 1061004
	REF_MAT_STD_G = 1061005
	REF_MAT_STD_RHO = 1061006
	REF_MAT_STD_ALPHA = 1061007
	REF_MAT_STD_DAMP = 1061008
	
	REF_PROP_BEAM = 1071000
	REF_PROP_BEAM_NAME = 1071001
	REF_PROP_BEAM_MAT = 1071002
	REF_PROP_BEAM_DESC = 1071003
	REF_PROP_BEAM_MOD = 1071004
	REF_PROP_BEAM_AREA = 1071005
	REF_PROP_BEAM_IYY = 1071006
	REF_PROP_BEAM_IZZ = 1071007
	REF_PROP_BEAM_IYZ = 1071008
	REF_PROP_BEAM_TORS = 1071009
	REF_PROP_BEAM_KY = 1071010
	REF_PROP_BEAM_KZ = 1071011
	REF_PROP_BEAM_TYPE = 1071012
	REF_PROP_BEAM_COST = 1071013
	
	REF_PROP_BEAM_DEFN = 1071200
	REF_PROP_BEAM_DEFN_NAME = 1071201
	REF_PROP_BEAM_DEFN_DESC = 1071202
	REF_PROP_BEAM_DEFN_AREA_BASE = 1071203
	REF_PROP_BEAM_DEFN_AREA_MOD = 1071204
	REF_PROP_BEAM_DEFN_IYY_BASE = 1071205
	REF_PROP_BEAM_DEFN_IYY_MOD = 1071206
	REF_PROP_BEAM_DEFN_IZZ_BASE = 1071207
	REF_PROP_BEAM_DEFN_IZZ_MOD = 1071208
	REF_PROP_BEAM_DEFN_TORS_BASE = 1071209
	REF_PROP_BEAM_DEFN_TORS_MOD = 1071210
	REF_PROP_BEAM_DEFN_KY_BASE = 1071211
	REF_PROP_BEAM_DEFN_KY_MOD = 1071212
	REF_PROP_BEAM_DEFN_KZ_BASE = 1071213
	REF_PROP_BEAM_DEFN_KZ_MOD = 1071214
	REF_PROP_BEAM_DEFN_MOD_MASS = 1071215
	REF_PROP_BEAM_DEFN_STRS_CALC = 1071216
	
	REF_PROP_BEAM_SUM = 1071400
	REF_PROP_BEAM_SUM_NAME = 1071401
	REF_PROP_BEAM_SUM_DESC = 1071402
	REF_PROP_BEAM_SUM_EL_NUM = 1071403
	REF_PROP_BEAM_SUM_EL_LEN = 1071404
	REF_PROP_BEAM_SUM_EL_MASS = 1071405
	REF_PROP_BEAM_SUM_EL_SURF = 1071406
	REF_PROP_BEAM_SUM_EL_COST = 1071407
	REF_PROP_BEAM_SUM_MB_NUM = 1071408
	REF_PROP_BEAM_SUM_MB_LEN = 1071409
	REF_PROP_BEAM_SUM_MB_MASS = 1071410
	REF_PROP_BEAM_SUM_MB_SURF = 1071411
	REF_PROP_BEAM_SUM_MB_COST = 1071412
	
	REF_PROP_BEAM_EXT = 1071600
	REF_PROP_BEAM_EXT_NAME = 1071601
	REF_PROP_BEAM_EXT_DESC = 1071602
	REF_PROP_BEAM_EXT_REF_PT = 1071603
	REF_PROP_BEAM_EXT_AREA = 1071604
	REF_PROP_BEAM_EXT_IYY = 1071605
	REF_PROP_BEAM_EXT_IZZ = 1071606
	REF_PROP_BEAM_EXT_IYZ = 1071607
	REF_PROP_BEAM_EXT_IUU = 1071608
	REF_PROP_BEAM_EXT_IVV = 1071609
	REF_PROP_BEAM_EXT_ANGLE = 1071610
	REF_PROP_BEAM_EXT_KY = 1071611
	REF_PROP_BEAM_EXT_KZ = 1071612
	REF_PROP_BEAM_EXT_J = 1071613
	REF_PROP_BEAM_EXT_CTOR = 1071614
	REF_PROP_BEAM_EXT_ZY = 1071615
	REF_PROP_BEAM_EXT_ZZ = 1071616
	REF_PROP_BEAM_EXT_ZPY = 1071617
	REF_PROP_BEAM_EXT_ZPZ = 1071618
	REF_PROP_BEAM_EXT_CY = 1071619
	REF_PROP_BEAM_EXT_CZ = 1071620
	REF_PROP_BEAM_EXT_RY = 1071621
	REF_PROP_BEAM_EXT_RZ = 1071622
	REF_PROP_BEAM_EXT_MASS = 1071623
	REF_PROP_BEAM_EXT_PERIM = 1071624
	
	REF_PROP_SPR = 1073000
	REF_PROP_SPR_NAME = 1073001
	REF_PROP_SPR_AXIS = 1073002
	REF_PROP_SPR_TYPE = 1073003
	REF_PROP_SPR_REF_X = 1073004
	REF_PROP_SPR_KX = 1073005
	REF_PROP_SPR_REF_Y = 1073006
	REF_PROP_SPR_KY = 1073007
	REF_PROP_SPR_REF_Z = 1073008
	REF_PROP_SPR_KZ = 1073009
	REF_PROP_SPR_MATRIX = 1073010
	REF_PROP_SPR_DAMP = 1073011
	
	REF_PROP_SPRMAT = 1073600
	REF_PROP_SPRMAT_NAME = 1073601
	REF_PROP_SPRMAT_K11 = 1073602
	REF_PROP_SPRMAT_K12 = 1073603
	REF_PROP_SPRMAT_K13 = 1073604
	REF_PROP_SPRMAT_K14 = 1073605
	REF_PROP_SPRMAT_K15 = 1073606
	REF_PROP_SPRMAT_K16 = 1073607
	REF_PROP_SPRMAT_K22 = 1073608
	REF_PROP_SPRMAT_K23 = 1073609
	REF_PROP_SPRMAT_K24 = 1073610
	REF_PROP_SPRMAT_K25 = 1073611
	REF_PROP_SPRMAT_K26 = 1073612
	REF_PROP_SPRMAT_K33 = 1073613
	REF_PROP_SPRMAT_K34 = 1073614
	REF_PROP_SPRMAT_K35 = 1073615
	REF_PROP_SPRMAT_K36 = 1073616
	REF_PROP_SPRMAT_K44 = 1073617
	REF_PROP_SPRMAT_K45 = 1073618
	REF_PROP_SPRMAT_K46 = 1073619
	REF_PROP_SPRMAT_K55 = 1073620
	REF_PROP_SPRMAT_K56 = 1073621
	REF_PROP_SPRMAT_K66 = 1073622
	
	REF_PROP_MASS = 1075000
	REF_PROP_MASS_NAME = 1075001
	REF_PROP_MASS_AXIS = 1075002
	REF_PROP_MASS_MASS = 1075003
	REF_PROP_MASS_IXX = 1075004
	REF_PROP_MASS_IYY = 1075005
	REF_PROP_MASS_IZZ = 1075006
	REF_PROP_MASS_IXY = 1075007
	REF_PROP_MASS_IYZ = 1075008
	REF_PROP_MASS_IZX = 1075009
	REF_PROP_MASS_ADD_X = 1075010
	REF_PROP_MASS_ADD_Y = 1075011
	REF_PROP_MASS_ADD_Z = 1075012
	
	REF_PROP_2DEL = 1076000
	REF_PROP_2DEL_NAME = 1076001
	REF_PROP_2DEL_AXIS = 1076002
	REF_PROP_2DEL_TYPE = 1076003
	REF_PROP_2DEL_MAT = 1076004
	REF_PROP_2DEL_THICK = 1076005
	REF_PROP_2DEL_BENDING = 1076006
	REF_PROP_2DEL_INPLANE = 1076007
	REF_PROP_2DEL_WEIGHT = 1076008
	REF_PROP_2DEL_SUPT_PAT = 1076009
	REF_PROP_2DEL_REF_EDGE = 1076010
	REF_PROP_2DEL_MASS = 1076011
	
	REF_PROP_LINK = 1077000
	REF_PROP_LINK_NAME = 1077001
	REF_PROP_LINK_TYPE = 1077002
	
	REF_PROP_CABLE = 1078000
	REF_PROP_CABLE_NAME = 1078001
	REF_PROP_CABLE_STIFF = 1078002
	REF_PROP_CABLE_MASS = 1078003
	REF_PROP_CABLE_ALPHA = 1078004
	REF_PROP_CABLE_DAMP = 1078005
	
	REF_PROP_SPACE = 1079000
	REF_PROP_SPACE_NAME = 1079001
	REF_PROP_SPACE_AXIS = 1079002
	REF_PROP_SPACE_TYPE = 1079003
	REF_PROP_SPACE_LEGLEN = 1079004
	REF_PROP_SPACE_STIFF = 1079005
	REF_PROP_SPACE_RATIO = 1079006
	
	REF_PROP_DES_STL_BEAM = 1082000
	REF_PROP_DES_STL_BEAM_NAME = 1082001
	REF_PROP_DES_STL_BEAM_GRADE = 1082002
	REF_PROP_DES_STL_BEAM_L_OVERRIDE = 1082003
	REF_PROP_DES_STL_BEAM_LYY = 1082004
	REF_PROP_DES_STL_BEAM_LZZ = 1082005
	REF_PROP_DES_STL_BEAM_LLT = 1082006
	REF_PROP_DES_STL_BEAM_PLAS_ELAS_RATIO = 1082007
	REF_PROP_DES_STL_BEAM_AREA_RATIO = 1082008
	REF_PROP_DES_STL_BEAM_BETA_FACTOR = 1082009
	
	REF_PROP_DES_STL_REST = 1082200
	REF_PROP_DES_STL_REST_NAME = 1082201
	REF_PROP_DES_STL_REST_TYPE = 1082202
	REF_PROP_DES_STL_REST_REF = 1082203
	REF_PROP_DES_STL_REST_LD_HT = 1082204
	REF_PROP_DES_STL_REST_DESC = 1082205
	
	REF_PROP_DES_RC_BEAM = 1084000
	REF_PROP_DES_RC_BEAM_NAME = 1084001
	REF_PROP_DES_RC_BEAM_TYPE = 1084002
	REF_PROP_DES_RC_BEAM_CONC = 1084003
	REF_PROP_DES_RC_BEAM_REBAR_MAIN = 1084004
	REF_PROP_DES_RC_BEAM_REBAR_LINK = 1084005
	REF_PROP_DES_RC_BEAM_LINK_SIZE = 1084006
	REF_PROP_DES_RC_BEAM_AGGREGATE = 1084007
	REF_PROP_DES_RC_BEAM_COVER_TOP = 1084008
	REF_PROP_DES_RC_BEAM_COVER_BOT = 1084009
	REF_PROP_DES_RC_BEAM_COVER_LEFT = 1084010
	REF_PROP_DES_RC_BEAM_COVER_RIGHT = 1084011
	REF_PROP_DES_RC_BEAM_LAYOUT_LEFT_NAME = 1084012
	REF_PROP_DES_RC_BEAM_LAYOUT_MIDDLE_NAME = 1084013
	REF_PROP_DES_RC_BEAM_LAYOUT_RIGHT_NAME = 1084014
	
	REF_PROP_DES_RC2D_BS8110_A = 1089000
	REF_PROP_DES_RC2D_BS8110_A_NAME = 1089001
	REF_PROP_DES_RC2D_BS8110_A_THETA1 = 1089002
	REF_PROP_DES_RC2D_BS8110_A_THETA2 = 1089003
	REF_PROP_DES_RC2D_BS8110_A_ZT1 = 1089004
	REF_PROP_DES_RC2D_BS8110_A_ZB1 = 1089005
	REF_PROP_DES_RC2D_BS8110_A_ZT2 = 1089006
	REF_PROP_DES_RC2D_BS8110_A_ZB2 = 1089007
	REF_PROP_DES_RC2D_BS8110_A_THICK = 1089008
	REF_PROP_DES_RC2D_BS8110_A_MIN_AREA_T1 = 1089009
	REF_PROP_DES_RC2D_BS8110_A_MIN_AREA_T2 = 1089010
	REF_PROP_DES_RC2D_BS8110_A_MIN_AREA_B1 = 1089011
	REF_PROP_DES_RC2D_BS8110_A_MIN_AREA_B2 = 1089012
	
	REF_PROP_DES_RC2D_BS8110_B = 1089050
	REF_PROP_DES_RC2D_BS8110_B_NAME = 1089051
	REF_PROP_DES_RC2D_BS8110_B_FCU = 1089052
	REF_PROP_DES_RC2D_BS8110_B_GAMMA_MC = 1089053
	REF_PROP_DES_RC2D_BS8110_B_FCD1 = 1089054
	REF_PROP_DES_RC2D_BS8110_B_FCD2 = 1089055
	REF_PROP_DES_RC2D_BS8110_B_FT = 1089056
	REF_PROP_DES_RC2D_BS8110_B_EPS_CTRANS = 1089057
	REF_PROP_DES_RC2D_BS8110_B_EPS_CAX = 1089058
	REF_PROP_DES_RC2D_BS8110_B_EPS_CU = 1089059
	REF_PROP_DES_RC2D_BS8110_B_XOVERD_MIN = 1089060
	REF_PROP_DES_RC2D_BS8110_B_XOVERD_MAX = 1089061
	REF_PROP_DES_RC2D_BS8110_B_BETA = 1089062
	REF_PROP_DES_RC2D_BS8110_B_MOD_GAMMA_MC = 1089063
	REF_PROP_DES_RC2D_BS8110_B_MOD_PARAMS = 1089064
	
	REF_PROP_DES_RC2D_BS8110_C = 1089100
	REF_PROP_DES_RC2D_BS8110_C_NAME = 1089101
	REF_PROP_DES_RC2D_BS8110_C_force_y = 1089102
	REF_PROP_DES_RC2D_BS8110_C_GAMMA_MS = 1089103
	REF_PROP_DES_RC2D_BS8110_C_force_yD = 1089104
	REF_PROP_DES_RC2D_BS8110_C_force_yDC = 1089105
	REF_PROP_DES_RC2D_BS8110_C_ES = 1089106
	REF_PROP_DES_RC2D_BS8110_C_FLIM = 1089107
	REF_PROP_DES_RC2D_BS8110_C_EPS_PLAS = 1089108
	REF_PROP_DES_RC2D_BS8110_C_EPS_PLASC = 1089109
	REF_PROP_DES_RC2D_BS8110_C_MOD_GAMMA_MS = 1089110
	REF_PROP_DES_RC2D_BS8110_C_MOD_PARAMS = 1089111
	
	REF_PROP_DES_RC2D_BS5400_A = 1089150
	REF_PROP_DES_RC2D_BS5400_A_NAME = 1089151
	REF_PROP_DES_RC2D_BS5400_A_THETA1 = 1089152
	REF_PROP_DES_RC2D_BS5400_A_THETA2 = 1089153
	REF_PROP_DES_RC2D_BS5400_A_ZT1 = 1089154
	REF_PROP_DES_RC2D_BS5400_A_ZB1 = 1089155
	REF_PROP_DES_RC2D_BS5400_A_ZT2 = 1089156
	REF_PROP_DES_RC2D_BS5400_A_ZB2 = 1089157
	REF_PROP_DES_RC2D_BS5400_A_THICK = 1089158
	REF_PROP_DES_RC2D_BS5400_A_MIN_AREA_T1 = 1089159
	REF_PROP_DES_RC2D_BS5400_A_MIN_AREA_T2 = 1089160
	REF_PROP_DES_RC2D_BS5400_A_MIN_AREA_B1 = 1089161
	REF_PROP_DES_RC2D_BS5400_A_MIN_AREA_B2 = 1089162
	
	REF_PROP_DES_RC2D_BS5400_B = 1089200
	REF_PROP_DES_RC2D_BS5400_B_NAME = 1089201
	REF_PROP_DES_RC2D_BS5400_B_FCU = 1089202
	REF_PROP_DES_RC2D_BS5400_B_GAMMA_MC = 1089203
	REF_PROP_DES_RC2D_BS5400_B_FCD1 = 1089204
	REF_PROP_DES_RC2D_BS5400_B_FCD2 = 1089205
	REF_PROP_DES_RC2D_BS5400_B_FT = 1089206
	REF_PROP_DES_RC2D_BS5400_B_EPS_CTRANS = 1089207
	REF_PROP_DES_RC2D_BS5400_B_EPS_CAX = 1089208
	REF_PROP_DES_RC2D_BS5400_B_EPS_CU = 1089209
	REF_PROP_DES_RC2D_BS5400_B_XOVERD_MIN = 1089210
	REF_PROP_DES_RC2D_BS5400_B_XOVERD_MAX = 1089211
	REF_PROP_DES_RC2D_BS5400_B_BETA = 1089212
	REF_PROP_DES_RC2D_BS5400_B_MOD_GAMMA_MC = 1089213
	REF_PROP_DES_RC2D_BS5400_B_MOD_PARAMS = 1089214
	
	REF_PROP_DES_RC2D_BS5400_C = 1089250
	REF_PROP_DES_RC2D_BS5400_C_NAME = 1089251
	REF_PROP_DES_RC2D_BS5400_C_force_y = 1089252
	REF_PROP_DES_RC2D_BS5400_C_GAMMA_MS = 1089253
	REF_PROP_DES_RC2D_BS5400_C_force_yD = 1089254
	REF_PROP_DES_RC2D_BS5400_C_force_yDC = 1089255
	REF_PROP_DES_RC2D_BS5400_C_ES = 1089256
	REF_PROP_DES_RC2D_BS5400_C_FLIM = 1089257
	REF_PROP_DES_RC2D_BS5400_C_EPS_PLAS = 1089258
	REF_PROP_DES_RC2D_BS5400_C_EPS_PLASC = 1089259
	REF_PROP_DES_RC2D_BS5400_C_MOD_GAMMA_MS = 1089260
	REF_PROP_DES_RC2D_BS5400_C_MOD_PARAMS = 1089261
	
	REF_PROP_DES_RC2D_EC2_A = 1089300
	REF_PROP_DES_RC2D_EC2_A_NAME = 1089301
	REF_PROP_DES_RC2D_EC2_A_THETA1 = 1089302
	REF_PROP_DES_RC2D_EC2_A_THETA2 = 1089303
	REF_PROP_DES_RC2D_EC2_A_ZT1 = 1089304
	REF_PROP_DES_RC2D_EC2_A_ZB1 = 1089305
	REF_PROP_DES_RC2D_EC2_A_ZT2 = 1089306
	REF_PROP_DES_RC2D_EC2_A_ZB2 = 1089307
	REF_PROP_DES_RC2D_EC2_A_THICK = 1089308
	REF_PROP_DES_RC2D_EC2_A_MIN_AREA_T1 = 1089309
	REF_PROP_DES_RC2D_EC2_A_MIN_AREA_T2 = 1089310
	REF_PROP_DES_RC2D_EC2_A_MIN_AREA_B1 = 1089311
	REF_PROP_DES_RC2D_EC2_A_MIN_AREA_B2 = 1089312
	
	REF_PROP_DES_RC2D_EC2_B = 1089350
	REF_PROP_DES_RC2D_EC2_B_NAME = 1089351
	REF_PROP_DES_RC2D_EC2_B_FCU = 1089352
	REF_PROP_DES_RC2D_EC2_B_GAMMA_MC = 1089353
	REF_PROP_DES_RC2D_EC2_B_FCD1 = 1089354
	REF_PROP_DES_RC2D_EC2_B_FCD2 = 1089355
	REF_PROP_DES_RC2D_EC2_B_FT = 1089356
	REF_PROP_DES_RC2D_EC2_B_EPS_CTRANS = 1089357
	REF_PROP_DES_RC2D_EC2_B_EPS_CAX = 1089358
	REF_PROP_DES_RC2D_EC2_B_EPS_CU = 1089359
	REF_PROP_DES_RC2D_EC2_B_XOVERD_MIN = 1089360
	REF_PROP_DES_RC2D_EC2_B_XOVERD_MAX = 1089361
	REF_PROP_DES_RC2D_EC2_B_BETA = 1089362
	REF_PROP_DES_RC2D_EC2_B_MOD_GAMMA_MC = 1089363
	REF_PROP_DES_RC2D_EC2_B_MOD_PARAMS = 1089364
	
	REF_PROP_DES_RC2D_EC2_C = 1089400
	REF_PROP_DES_RC2D_EC2_C_NAME = 1089401
	REF_PROP_DES_RC2D_EC2_C_force_y = 1089402
	REF_PROP_DES_RC2D_EC2_C_GAMMA_MS = 1089403
	REF_PROP_DES_RC2D_EC2_C_force_yD = 1089404
	REF_PROP_DES_RC2D_EC2_C_force_yDC = 1089405
	REF_PROP_DES_RC2D_EC2_C_ES = 1089406
	REF_PROP_DES_RC2D_EC2_C_FLIM = 1089407
	REF_PROP_DES_RC2D_EC2_C_EPS_PLAS = 1089408
	REF_PROP_DES_RC2D_EC2_C_EPS_PLASC = 1089409
	REF_PROP_DES_RC2D_EC2_C_MOD_GAMMA_MS = 1089410
	REF_PROP_DES_RC2D_EC2_C_MOD_PARAMS = 1089411
	
	REF_PROP_DES_RC2D_ACI318_A = 1089450
	REF_PROP_DES_RC2D_ACI318_A_NAME = 1089451
	REF_PROP_DES_RC2D_ACI318_A_THETA1 = 1089452
	REF_PROP_DES_RC2D_ACI318_A_THETA2 = 1089453
	REF_PROP_DES_RC2D_ACI318_A_ZT1 = 1089454
	REF_PROP_DES_RC2D_ACI318_A_ZB1 = 1089455
	REF_PROP_DES_RC2D_ACI318_A_ZT2 = 1089456
	REF_PROP_DES_RC2D_ACI318_A_ZB2 = 1089457
	REF_PROP_DES_RC2D_ACI318_A_THICK = 1089458
	REF_PROP_DES_RC2D_ACI318_A_MIN_AREA_T1 = 1089459
	REF_PROP_DES_RC2D_ACI318_A_MIN_AREA_T2 = 1089460
	REF_PROP_DES_RC2D_ACI318_A_MIN_AREA_B1 = 1089461
	REF_PROP_DES_RC2D_ACI318_A_MIN_AREA_B2 = 1089462
	REF_PROP_DES_RC2D_ACI318_A_PHI_C = 1089463
	REF_PROP_DES_RC2D_ACI318_A_PHI_T = 1089464
	
	REF_PROP_DES_RC2D_ACI318_B = 1089500
	REF_PROP_DES_RC2D_ACI318_B_NAME = 1089501
	REF_PROP_DES_RC2D_ACI318_B_FCU = 1089502
	REF_PROP_DES_RC2D_ACI318_B_FCD1 = 1089503
	REF_PROP_DES_RC2D_ACI318_B_FCD2 = 1089504
	REF_PROP_DES_RC2D_ACI318_B_FT = 1089505
	REF_PROP_DES_RC2D_ACI318_B_EPS_CTRANS = 1089506
	REF_PROP_DES_RC2D_ACI318_B_EPS_CAX = 1089507
	REF_PROP_DES_RC2D_ACI318_B_EPS_CU = 1089508
	REF_PROP_DES_RC2D_ACI318_B_XOVERD_MIN = 1089509
	REF_PROP_DES_RC2D_ACI318_B_XOVERD_MAX = 1089510
	REF_PROP_DES_RC2D_ACI318_B_BETA = 1089511
	REF_PROP_DES_RC2D_ACI318_B_MOD_PARAMS = 1089512
	
	REF_PROP_DES_RC2D_ACI318_C = 1089550
	REF_PROP_DES_RC2D_ACI318_C_NAME = 1089551
	REF_PROP_DES_RC2D_ACI318_C_force_y = 1089552
	REF_PROP_DES_RC2D_ACI318_C_force_yD = 1089553
	REF_PROP_DES_RC2D_ACI318_C_force_yDC = 1089554
	REF_PROP_DES_RC2D_ACI318_C_ES = 1089555
	REF_PROP_DES_RC2D_ACI318_C_FLIM = 1089556
	REF_PROP_DES_RC2D_ACI318_C_EPS_PLAS = 1089557
	REF_PROP_DES_RC2D_ACI318_C_EPS_PLASC = 1089558
	REF_PROP_DES_RC2D_ACI318_C_MOD_PARAMS = 1089559
	
	REF_PROP_DES_RC2D_AS3600_A = 1089600
	REF_PROP_DES_RC2D_AS3600_A_NAME = 1089601
	REF_PROP_DES_RC2D_AS3600_A_THETA1 = 1089602
	REF_PROP_DES_RC2D_AS3600_A_THETA2 = 1089603
	REF_PROP_DES_RC2D_AS3600_A_ZT1 = 1089604
	REF_PROP_DES_RC2D_AS3600_A_ZB1 = 1089605
	REF_PROP_DES_RC2D_AS3600_A_ZT2 = 1089606
	REF_PROP_DES_RC2D_AS3600_A_ZB2 = 1089607
	REF_PROP_DES_RC2D_AS3600_A_THICK = 1089608
	REF_PROP_DES_RC2D_AS3600_A_MIN_AREA_T1 = 1089609
	REF_PROP_DES_RC2D_AS3600_A_MIN_AREA_T2 = 1089610
	REF_PROP_DES_RC2D_AS3600_A_MIN_AREA_B1 = 1089611
	REF_PROP_DES_RC2D_AS3600_A_MIN_AREA_B2 = 1089612
	REF_PROP_DES_RC2D_AS3600_A_PHI_C = 1089613
	REF_PROP_DES_RC2D_AS3600_A_PHI_T = 1089614
	
	REF_PROP_DES_RC2D_AS3600_B = 1089650
	REF_PROP_DES_RC2D_AS3600_B_NAME = 1089651
	REF_PROP_DES_RC2D_AS3600_B_FCU = 1089652
	REF_PROP_DES_RC2D_AS3600_B_FCD1 = 1089653
	REF_PROP_DES_RC2D_AS3600_B_FCD2 = 1089654
	REF_PROP_DES_RC2D_AS3600_B_FT = 1089655
	REF_PROP_DES_RC2D_AS3600_B_EPS_CTRANS = 1089656
	REF_PROP_DES_RC2D_AS3600_B_EPS_CAX = 1089657
	REF_PROP_DES_RC2D_AS3600_B_EPS_CU = 1089658
	REF_PROP_DES_RC2D_AS3600_B_XOVERD_MIN = 1089659
	REF_PROP_DES_RC2D_AS3600_B_XOVERD_MAX = 1089660
	REF_PROP_DES_RC2D_AS3600_B_BETA = 1089661
	REF_PROP_DES_RC2D_AS3600_B_MOD_PARAMS = 1089662
	
	REF_PROP_DES_RC2D_AS3600_C = 1089700
	REF_PROP_DES_RC2D_AS3600_C_NAME = 1089701
	REF_PROP_DES_RC2D_AS3600_C_force_y = 1089702
	REF_PROP_DES_RC2D_AS3600_C_force_yD = 1089703
	REF_PROP_DES_RC2D_AS3600_C_force_yDC = 1089704
	REF_PROP_DES_RC2D_AS3600_C_ES = 1089705
	REF_PROP_DES_RC2D_AS3600_C_FLIM = 1089706
	REF_PROP_DES_RC2D_AS3600_C_EPS_PLAS = 1089707
	REF_PROP_DES_RC2D_AS3600_C_EPS_PLASC = 1089708
	REF_PROP_DES_RC2D_AS3600_C_MOD_PARAMS = 1089709
	
	REF_PROP_DES_RC2D_IS456_A = 1089750
	REF_PROP_DES_RC2D_IS456_A_NAME = 1089751
	REF_PROP_DES_RC2D_IS456_A_THETA1 = 1089752
	REF_PROP_DES_RC2D_IS456_A_THETA2 = 1089753
	REF_PROP_DES_RC2D_IS456_A_ZT1 = 1089754
	REF_PROP_DES_RC2D_IS456_A_ZB1 = 1089755
	REF_PROP_DES_RC2D_IS456_A_ZT2 = 1089756
	REF_PROP_DES_RC2D_IS456_A_ZB2 = 1089757
	REF_PROP_DES_RC2D_IS456_A_THICK = 1089758
	REF_PROP_DES_RC2D_IS456_A_MIN_AREA_T1 = 1089759
	REF_PROP_DES_RC2D_IS456_A_MIN_AREA_T2 = 1089760
	REF_PROP_DES_RC2D_IS456_A_MIN_AREA_B1 = 1089761
	REF_PROP_DES_RC2D_IS456_A_MIN_AREA_B2 = 1089762
	
	REF_PROP_DES_RC2D_IS456_B = 1089800
	REF_PROP_DES_RC2D_IS456_B_NAME = 1089801
	REF_PROP_DES_RC2D_IS456_B_FCU = 1089802
	REF_PROP_DES_RC2D_IS456_B_GAMMA_MC = 1089803
	REF_PROP_DES_RC2D_IS456_B_FCD1 = 1089804
	REF_PROP_DES_RC2D_IS456_B_FCD2 = 1089805
	REF_PROP_DES_RC2D_IS456_B_FT = 1089806
	REF_PROP_DES_RC2D_IS456_B_EPS_CTRANS = 1089807
	REF_PROP_DES_RC2D_IS456_B_EPS_CAX = 1089808
	REF_PROP_DES_RC2D_IS456_B_EPS_CU = 1089809
	REF_PROP_DES_RC2D_IS456_B_XOVERD_MIN = 1089810
	REF_PROP_DES_RC2D_IS456_B_XOVERD_MAX = 1089811
	REF_PROP_DES_RC2D_IS456_B_BETA = 1089812
	REF_PROP_DES_RC2D_IS456_B_MOD_GAMMA_MC = 1089813
	REF_PROP_DES_RC2D_IS456_B_MOD_PARAMS = 1089814
	
	REF_PROP_DES_RC2D_IS456_C = 1089850
	REF_PROP_DES_RC2D_IS456_C_NAME = 1089851
	REF_PROP_DES_RC2D_IS456_C_force_y = 1089852
	REF_PROP_DES_RC2D_IS456_C_GAMMA_MS = 1089853
	REF_PROP_DES_RC2D_IS456_C_force_yD = 1089854
	REF_PROP_DES_RC2D_IS456_C_force_yDC = 1089855
	REF_PROP_DES_RC2D_IS456_C_ES = 1089856
	REF_PROP_DES_RC2D_IS456_C_FLIM = 1089857
	REF_PROP_DES_RC2D_IS456_C_EPS_PLAS = 1089858
	REF_PROP_DES_RC2D_IS456_C_EPS_PLASC = 1089859
	REF_PROP_DES_RC2D_IS456_C_MOD_GAMMA_MS = 1089860
	REF_PROP_DES_RC2D_IS456_C_MOD_PARAMS = 1089861
	
	REF_PROP_DES_RC_BEAM_BAR_LIM = 1090000
	
	REF_GEN_RESTR = 1101000
	REF_GEN_RESTR_NAME = 1101001
	REF_GEN_RESTR_STAGE = 1101002
	REF_GEN_RESTR_LIST = 1101003
	REF_GEN_RESTR_TYPE = 1101004
	
	REF_JOINT = 1102000
	REF_JOINT_NAME = 1102001
	REF_JOINT_STAGE = 1102002
	REF_JOINT_SLAVE = 1102003
	REF_JOINT_MASTER = 1102004
	REF_JOINT_TYPE = 1102005
	
	REF_RIGID_CONSTR = 1103000
	REF_RIGID_CONSTR_NAME = 1103001
	REF_RIGID_CONSTR_STAGE = 1103002
	REF_RIGID_CONSTR_LIST = 1103003
	REF_RIGID_CONSTR_MASTER = 1103004
	REF_RIGID_CONSTR_TYPE = 1103005
	
	REF_CONST_EQN = 1104000
	REF_CONST_EQN_NAME = 1104001
	REF_CONST_EQN_STAGE = 1104002
	REF_CONST_EQN_SLAVE = 1104003
	REF_CONST_EQN_DIR = 1104004
	REF_CONST_EQN_EQUATION = 1104005
	
	REF_TIED_INTER = 1105000
	REF_TIED_INTER_NAME = 1105001
	REF_TIED_INTER_STAGE = 1105002
	REF_TIED_INTER_SLAVE = 1105003
	REF_TIED_INTER_MASTER = 1105004
	REF_TIED_INTER_TOL = 1105005
	
	REF_ALL_LOADS = 2000200
	
	REF_GRID_POINT_LOAD = 2001000
	REF_GRID_POINT_FRC = 2001001
	REF_GRID_POINT_MOM = 2001002
	
	REF_GRID_LINE_LOAD = 2001100
	REF_GRID_LINE_FRC = 2001101
	REF_GRID_LINE_MOM = 2001102
	
	REF_GRID_AREA_LOAD = 2001200
	REF_GRID_AREA_FRC = 2001201
	REF_GRID_AREA_force_x = 2001202
	REF_GRID_AREA_force_y = 2001203
	REF_GRID_AREA_force_z = 2001204
	
	REF_ND_LOAD = 2002000
	REF_ND_LOAD_FRC = 2002001
	REF_ND_LOAD_MOM = 2002002
	REF_ND_APPL_TRANS = 2002003
	REF_ND_APPL_ROT = 2002004
	REF_ND_SETTLE_TRANS = 2002005
	REF_ND_SETTLE_ROT = 2002006
	
	REF_EL1D_LOAD = 2003000
	REF_EL1D_POINT_FRC = 2003001
	REF_EL1D_POINT_MOM = 2003002
	REF_EL1D_PATCH_FRC = 2003003
	REF_EL1D_PATCH_MOM = 2003004
	REF_EL1D_PRESTR = 2003005
	REF_EL1D_PRESTR_MOM = 2003006
	REF_EL1D_STRAIN = 2003007
	REF_EL1D_INIT_LEN = 2003008
	REF_EL1D_DIST_TRANS = 2003009
	REF_EL1D_DIST_ROT = 2003010
	REF_EL1D_TEMP = 2003011
	REF_EL1D_TEMP_GRAD = 2003012
	
	REF_EL2D_LOAD = 2004000
	REF_EL2D_FACE_PRESS = 2004001
	REF_EL2D_EDGE_PRESS = 2004002
	REF_EL2D_PRESTR = 2004003
	REF_EL2D_PRESTR_MOM = 2004004
	REF_EL2D_STRAIN = 2004005
	REF_EL2D_TEMP = 2004006
	REF_EL2D_TEMP_GRAD = 2004007
	
	#REF_LAST_GR_LOAD									 
	
	REF_LOAD_GRID_PT = 2011000
	REF_LOAD_GRID_PT_NAME = 2011001
	REF_LOAD_GRID_PT_GRID_PL = 2011002
	REF_LOAD_GRID_PT_GRID_X = 2011003
	REF_LOAD_GRID_PT_GRID_Y = 2011004
	REF_LOAD_GRID_PT_CASE = 2011005
	REF_LOAD_GRID_PT_AXIS = 2011006
	REF_LOAD_GRID_PT_DIR = 2011007
	REF_LOAD_GRID_PT_VAL = 2011008
	
	REF_LOAD_GRID_LN = 2011100
	REF_LOAD_GRID_LN_NAME = 2011101
	REF_LOAD_GRID_LN_GRID_PL = 2011102
	REF_LOAD_GRID_LN_POLY = 2011103
	REF_LOAD_GRID_LN_CASE = 2011104
	REF_LOAD_GRID_LN_AXIS = 2011105
	REF_LOAD_GRID_LN_PROJ = 2011106
	REF_LOAD_GRID_LN_DIR = 2011107
	REF_LOAD_GRID_LN_VAL1 = 2011108
	REF_LOAD_GRID_LN_VAL2 = 2011109
	
	REF_LOAD_GRID_AR = 2011200
	REF_LOAD_GRID_AR_NAME = 2011201
	REF_LOAD_GRID_AR_GRID_PL = 2011202
	REF_LOAD_GRID_AR_POLY = 2011203
	REF_LOAD_GRID_AR_CASE = 2011204
	REF_LOAD_GRID_AR_AXIS = 2011205
	REF_LOAD_GRID_AR_PROJ = 2011206
	REF_LOAD_GRID_AR_DIR = 2011207
	REF_LOAD_GRID_AR_VAL = 2011208
	
	REF_LOAD_NODE = 2012000
	REF_LOAD_NODE_NAME = 2012001
	REF_LOAD_NODE_LIST = 2012002
	REF_LOAD_NODE_CASE = 2012003
	REF_LOAD_NODE_AXIS = 2012004
	REF_LOAD_NODE_DIR = 2012005
	REF_LOAD_NODE_VAL = 2012006
	
	REF_APPL_DISP = 2013000
	REF_APPL_DISP_NAME = 2013001
	REF_APPL_DISP_LIST = 2013002
	REF_APPL_DISP_CASE = 2013003
	REF_APPL_DISP_AXIS = 2013004
	REF_APPL_DISP_DIR = 2013005
	REF_APPL_DISP_VAL = 2013006
	
	REF_SETTLE = 2014000
	REF_SETTLE_NAME = 2014001
	REF_SETTLE_LIST = 2014002
	REF_SETTLE_CASE = 2014003
	REF_SETTLE_DIR = 2014004
	REF_SETTLE_VAL = 2014005
	
	REF_LOAD_BEAM = 2015000
	REF_LOAD_BEAM_NAME = 2015001
	REF_LOAD_BEAM_LIST = 2015002
	REF_LOAD_BEAM_CASE = 2015003
	REF_LOAD_BEAM_TYPE = 2015004
	REF_LOAD_BEAM_AXIS = 2015005
	REF_LOAD_BEAM_PROJ = 2015006
	REF_LOAD_BEAM_DIR = 2015007
	REF_LOAD_BEAM_POS1 = 2015008
	REF_LOAD_BEAM_VAL1 = 2015009
	REF_LOAD_BEAM_POS2 = 2015010
	REF_LOAD_BEAM_VAL2 = 2015011
	
	REF_LOAD_PRESTR = 2017000
	REF_LOAD_PRESTR_NAME = 2017001
	REF_LOAD_PRESTR_LIST = 2017002
	REF_LOAD_PRESTR_CASE = 2017003
	REF_LOAD_PRESTR_TYPE = 2017004
	REF_LOAD_PRESTR_FRC = 2017005
	REF_LOAD_PRESTR_OFforce_y = 2017006
	REF_LOAD_PRESTR_OFforce_z = 2017007
	REF_LOAD_PRESTR_INITSTRN = 2017008
	REF_LOAD_PRESTR_LACKFIT = 2017009
	
	REF_LOAD_MEMDIST = 2018000
	REF_LOAD_MEMDIST_NAME = 2018001
	REF_LOAD_MEMDIST_LIST = 2018002
	REF_LOAD_MEMDIST_CASE = 2018003
	REF_LOAD_MEMDIST_DIR = 2018004
	REF_LOAD_MEMDIST_POS = 2018005
	REF_LOAD_MEMDIST_VAL = 2018006
	
	REF_BEAM_TEMP = 2019000
	REF_BEAM_TEMP_NAME = 2019001
	REF_BEAM_TEMP_LIST = 2019002
	REF_BEAM_TEMP_CASE = 2019003
	REF_BEAM_TEMP_TYPE = 2019004
	REF_BEAM_TEMP_CONST = 2019005
	REF_BEAM_TEMP_POS1 = 2019006
	REF_BEAM_TEMP_VAL1 = 2019007
	REF_BEAM_TEMP_POS2 = 2019008
	REF_BEAM_TEMP_VAL2 = 2019009
	
	REF_LOAD_2D_FACE = 2020000
	REF_LOAD_2D_FACE_NAME = 2020001
	REF_LOAD_2D_FACE_LIST = 2020002
	REF_LOAD_2D_FACE_CASE = 2020003
	REF_LOAD_2D_FACE_AXIS = 2020004
	REF_LOAD_2D_FACE_TYPE = 2020005
	REF_LOAD_2D_FACE_DIR = 2020006
	REF_LOAD_2D_FACE_VAL1 = 2020007
	REF_LOAD_2D_FACE_VAL2 = 2020008
	REF_LOAD_2D_FACE_VAL3 = 2020009
	REF_LOAD_2D_FACE_VAL4 = 2020010
	
	REF_LOAD_2D_EDGE = 2021000
	REF_LOAD_2D_EDGE_NAME = 2021001
	REF_LOAD_2D_EDGE_LIST = 2021002
	REF_LOAD_2D_EDGE_CASE = 2021003
	REF_LOAD_2D_EDGE_AXIS = 2021004
	REF_LOAD_2D_EDGE_EDGE = 2021005
	REF_LOAD_2D_EDGE_DIR = 2021006
	REF_LOAD_2D_EDGE_VAL1 = 2021007
	REF_LOAD_2D_EDGE_VAL2 = 2021008
	
	REF_LOAD_2D_INPLANE = 2022000
	REF_LOAD_2D_INPLANE_NAME = 2022001
	REF_LOAD_2D_INPLANE_LIST = 2022002
	REF_LOAD_2D_INPLANE_CASE = 2022003
	REF_LOAD_2D_INPLANE_TYPE = 2022004
	REF_LOAD_2D_INPLANE_DIR = 2022005
	REF_LOAD_2D_INPLANE_VAL = 2022006
	REF_LOAD_2D_INPLANE_OFFSET = 2022007
	REF_LOAD_2D_INPLANE_STRAIN = 2022008
	
	REF_LOAD_2D_TEMP = 2023000
	REF_LOAD_2D_TEMP_NAME = 2023001
	REF_LOAD_2D_TEMP_LIST = 2023002
	REF_LOAD_2D_TEMP_CASE = 2023003
	REF_LOAD_2D_TEMP_TYPE = 2023004
	REF_LOAD_2D_TEMP_CONST = 2023005
	REF_LOAD_2D_TEMP_VAL1_TOP = 2023006
	REF_LOAD_2D_TEMP_VAL1_BOT = 2023007
	REF_LOAD_2D_TEMP_VAL2_TOP = 2023008
	REF_LOAD_2D_TEMP_VAL2_BOT = 2023009
	REF_LOAD_2D_TEMP_VAL3_TOP = 2023010
	REF_LOAD_2D_TEMP_VAL3_BOT = 2023011
	REF_LOAD_2D_TEMP_VAL4_TOP = 2023012
	REF_LOAD_2D_TEMP_VAL4_BOT = 2023013
	
	REF_LOAD_GRAV = 2024000
	REF_LOAD_GRAV_NAME = 2024001
	REF_LOAD_GRAV_LIST = 2024002
	REF_LOAD_GRAV_CASE = 2024003
	REF_LOAD_GRAV_X = 2024004
	REF_LOAD_GRAV_Y = 2024005
	REF_LOAD_GRAV_Z = 2024006
	
	#REF_LAST_LOAD									
	
	#REF_FIRST_RESP									 
	
	REF_RESP_SPECTRA = 2100000
	
	REF_RESP_BASIC = 2101000
	REF_RESP_BASIC_NAME = 2101001
	REF_RESP_BASIC_AXIS = 2101002
	REF_RESP_BASIC_DIR = 2101003
	REF_RESP_BASIC_SPECTRUM = 2101004
	REF_RESP_BASIC_MODE = 2101005
	REF_RESP_BASIC_case_TYPE = 2101006
	REF_RESP_BASIC_DAMPING = 2101007
	
	REF_RESP_case = 2102000
	REF_RESP_case_NAME = 2102001
	REF_RESP_case_CASE_X = 2102002
	REF_RESP_case_FAC_X = 2102003
	REF_RESP_case_CASE_Y = 2102004
	REF_RESP_case_FAC_Y = 2102005
	REF_RESP_case_CASE_Z = 2102006
	REF_RESP_case_FAC_Z = 2102007
	
	REF_DAMPING_TABLE = 2103000
	
	REF_LOAD_CURVE = 2103100
	
	REF_DLF_CURVE = 2103110
	
	REF_FREQ_WT_CURVE = 2103120
	
	#REF_LAST_RESP									
	
	#REF_FIRST_BRIDGE									 
	
	#REF_FIRST_GR_BRIDGE									
	
	REF_BRG_ALIGN_AND_PATH = 3000200
	REF_BRG_INFL_POINT = 3000300
	
	#REF_LAST_GR_BRIDGE									 
	
	REF_ALIGNMENT = 3001000
	REF_ALIGNMENT_NAME = 3001001
	REF_ALIGNMENT_GRID_PL = 3001002
	REF_ALIGNMENT_CHAINAGE = 3001003
	REF_ALIGNMENT_CURVATURE = 3001004
	REF_ALIGNMENT_RADIUS = 3001005
	
	REF_BRG_PATH = 3002000
	REF_BRG_PATH_NAME = 3002001
	REF_BRG_PATH_TYPE = 3002002
	REF_BRG_PATH_GROUP = 3002003
	REF_BRG_PATH_ALIGNMENT = 3002004
	REF_BRG_PATH_LEFT = 3002005
	REF_BRG_PATH_CENTRE = 3002006
	REF_BRG_PATH_RIGHT = 3002007
	REF_BRG_PATH_GAUGE = 3002008
	REF_BRG_PATH_RAIL_FACTOR = 3002009
	
	REF_VEH_STD = 3003000
	REF_VEH_STD_NAME = 3003001
	REF_VEH_STD_WIDTH = 3003002
	REF_VEH_STD_AXLE_POS = 3003003
	REF_VEH_STD_WHEEL_OFFSET = 3003004
	REF_VEH_STD_WHEEL_LOAD_LEFT = 3003005
	REF_VEH_STD_WHEEL_LOAD_RIGHT = 3003006
	
	REF_VEH_USER = 3004000
	REF_VEH_USER_NAME = 3004001
	REF_VEH_USER_WIDTH = 3004002
	REF_VEH_USER_AXLE_POS = 3004003
	REF_VEH_USER_WHEEL_OFFSET = 3004004
	REF_VEH_USER_WHEEL_LOAD_LEFT = 3004005
	REF_VEH_USER_WHEEL_LOAD_RIGHT = 3004006
	
	REF_BRG_VUDL_STD = 3010000
	REF_BRG_VUDL_STD_NAME = 3010001
	REF_BRG_VUDL_STD_NUM_SEG = 3010002
	REF_BRG_VUDL_STD_SEG1_FAC = 3010003
	REF_BRG_VUDL_STD_SEG1_IND = 3010004
	REF_BRG_VUDL_STD_TRANS1 = 3010005
	REF_BRG_VUDL_STD_SEG2_FAC = 3010006
	REF_BRG_VUDL_STD_SEG2_IND = 3010007
	REF_BRG_VUDL_STD_TRANS2 = 3010008
	REF_BRG_VUDL_STD_SEG3_FAC = 3010009
	REF_BRG_VUDL_STD_SEG3_IND = 3010010
	
	REF_BRG_VUDL_USER = 3011000
	REF_BRG_VUDL_USER_NAME = 3011001
	REF_BRG_VUDL_USER_NUM_SEG = 3011002
	REF_BRG_VUDL_USER_SEG1_FAC = 3011003
	REF_BRG_VUDL_USER_SEG1_IND = 3011004
	REF_BRG_VUDL_USER_TRANS1 = 3011005
	REF_BRG_VUDL_USER_SEG2_FAC = 3011006
	REF_BRG_VUDL_USER_SEG2_IND = 3011007
	REF_BRG_VUDL_USER_TRANS2 = 3011008
	REF_BRG_VUDL_USER_SEG3_FAC = 3011009
	REF_BRG_VUDL_USER_SEG3_IND = 3011010
	
	REF_BRG_INFL_EFF_NODE = 3012000
	REF_BRG_INFL_EFF_NODE_NAME = 3012001
	REF_BRG_INFL_EFF_NODE_EFF = 3012002
	REF_BRG_INFL_EFF_NODE_NODE = 3012003
	REF_BRG_INFL_EFF_NODE_FAC = 3012004
	REF_BRG_INFL_EFF_NODE_TYPE = 3012005
	REF_BRG_INFL_EFF_NODE_AXIS = 3012006
	REF_BRG_INFL_EFF_NODE_DIR = 3012007
	
	REF_BRG_INFL_EFF_BEAM = 3013000
	REF_BRG_INFL_EFF_BEAM_NAME = 3013001
	REF_BRG_INFL_EFF_BEAM_EFF = 3013002
	REF_BRG_INFL_EFF_BEAM_BEAM = 3013003
	REF_BRG_INFL_EFF_BEAM_POS = 3013004
	REF_BRG_INFL_EFF_BEAM_FAC = 3013005
	REF_BRG_INFL_EFF_BEAM_TYPE = 3013006
	REF_BRG_INFL_EFF_BEAM_AXIS = 3013007
	REF_BRG_INFL_EFF_BEAM_DIR = 3013008
	
	REF_BRG_PATH_LOAD = 3014000
	REF_BRG_PATH_LOAD_PATH = 3014001
	REF_BRG_PATH_LOAD_TYPE = 3014002
	REF_BRG_PATH_LOAD_VUDL = 3014003
	REF_BRG_PATH_LOAD_VUDL_FAC = 3014004
	REF_BRG_PATH_LOAD_KEL = 3014005
	REF_BRG_PATH_LOAD_VEH = 3014006
	REF_BRG_PATH_LOAD_VEH_FAC = 3014007
	REF_BRG_PATH_LOAD_VEH_EXCL = 3014008
	REF_BRG_PATH_LOAD_VEH2 = 3014009
	REF_BRG_PATH_LOAD_VEH_FAC2 = 3014010
	REF_BRG_PATH_LOAD_VEH_EXCL2 = 3014011
	
	REF_BRG_LOAD_STATIC = 3015000
	REF_BRG_LOAD_STATIC_NAME = 3015001
	REF_BRG_LOAD_STATIC_GROUP = 3015002
	REF_BRG_LOAD_STATIC_PATH = 3015003
	REF_BRG_LOAD_STATIC_TYPE = 3015004
	REF_BRG_LOAD_STATIC_CHAIN_START = 3015005
	REF_BRG_LOAD_STATIC_CHAIN_END = 3015006
	REF_BRG_LOAD_STATIC_VEH = 3015007
	REF_BRG_LOAD_STATIC_FACTOR = 3015008
	REF_BRG_LOAD_STATIC_EFFECT = 3015009
	REF_BRG_LOAD_STATIC_GRP_EFFECT = 3015010
	
	REF_BRG_LOAD_MOVING = 3016000
	REF_BRG_LOAD_MOVING_NAME = 3016001
	REF_BRG_LOAD_MOVING_PATH = 3016002
	REF_BRG_LOAD_MOVING_TYPE = 3016003
	REF_BRG_LOAD_MOVING_CHAIN_START = 3016004
	REF_BRG_LOAD_MOVING_CHAIN_END = 3016005
	REF_BRG_LOAD_MOVING_CHAIN_INT = 3016006
	REF_BRG_LOAD_MOVING_VEH = 3016007
	REF_BRG_LOAD_MOVING_FACTOR = 3016008
	
	REF_BRG_AUTO_PATH = 3017000
	REF_BRG_AUTO_PATH_NAME = 3017001
	REF_BRG_AUTO_PATH_TYPE = 3017002
	REF_BRG_AUTO_PATH_GROUP = 3017003
	REF_BRG_AUTO_PATH_ALIGNMENT = 3017004
	REF_BRG_AUTO_PATH_LEFT = 3017005
	REF_BRG_AUTO_PATH_CENTRE = 3017006
	REF_BRG_AUTO_PATH_RIGHT = 3017007
	REF_BRG_AUTO_PATH_GAUGE = 3017008
	REF_BRG_AUTO_PATH_RAIL_FACTOR = 3017009
	REF_BRG_AUTO_PATH_ANALYSE = 3017010
	
	REF_BRG_AUTO_PATH_LOAD = 3018000
	REF_BRG_AUTO_PATH_LOAD_PATH = 3018001
	REF_BRG_AUTO_PATH_LOAD_TYPE = 3018002
	REF_BRG_AUTO_PATH_LOAD_VUDL = 3018003
	REF_BRG_AUTO_PATH_LOAD_VUDL_FAC = 3018004
	REF_BRG_AUTO_PATH_LOAD_KEL = 3018005
	REF_BRG_AUTO_PATH_LOAD_VEH = 3018006
	REF_BRG_AUTO_PATH_LOAD_VEH_FAC = 3018007
	REF_BRG_AUTO_PATH_LOAD_VEH_EXCL = 3018008
	REF_BRG_AUTO_PATH_LOAD_VEH2 = 3018009
	REF_BRG_AUTO_PATH_LOAD_VEH_FAC2 = 3018010
	REF_BRG_AUTO_PATH_LOAD_VEH_EXCL2 = 3018011
	
	REF_BRG_LOAD_BASIC = 3019000
	REF_BRG_LOAD_BASIC_NAME = 3019001
	REF_BRG_LOAD_BASIC_GROUP = 3019002
	REF_BRG_LOAD_BASIC_PATH = 3019003
	REF_BRG_LOAD_BASIC_TYPE = 3019004
	REF_BRG_LOAD_BASIC_CHAIN_START = 3019005
	REF_BRG_LOAD_BASIC_CHAIN_END = 3019006
	REF_BRG_LOAD_BASIC_VEH = 3019007
	REF_BRG_LOAD_BASIC_FACTOR = 3019008
	REF_BRG_LOAD_BASIC_EFFECT = 3019009
	REF_BRG_LOAD_BASIC_GRP_EFFECT = 3019010
	
	REF_BRG_AUTO_OPTI_SUM = 3020000
	REF_BRG_AUTO_OPTI_SUM_NAME = 3020001
	REF_BRG_AUTO_OPTI_SUM_HBPATH = 3020002
	REF_BRG_AUTO_OPTI_SUM_LANE1PATH = 3020003
	REF_BRG_AUTO_OPTI_SUM_LANE1FAC = 3020004
	REF_BRG_AUTO_OPTI_SUM_LANE2PATH = 3020005
	REF_BRG_AUTO_OPTI_SUM_LANE2FAC = 3020006
	REF_BRG_AUTO_OPTI_SUM_LANE3PATH = 3020007
	REF_BRG_AUTO_OPTI_SUM_LANE3FAC = 3020008
	REF_BRG_AUTO_OPTI_SUM_LANE4PATH = 3020009
	REF_BRG_AUTO_OPTI_SUM_LANE4FAC = 3020010
	REF_BRG_AUTO_OPTI_SUM_TOTEFFECT = 3020011
	REF_BRG_AUTO_OPTI_SUM_FACEFFECT = 3020012
	REF_BRG_AUTO_OPTI_SUM_CRITICAL = 3020013
	
	REF_BRG_AUTO_OPTI_SUM_EC1 = 3021000
	REF_BRG_AUTO_OPTI_SUM_EC1_NAME = 3021001
	REF_BRG_AUTO_OPTI_SUM_EC1_HBPATH = 3021002
	REF_BRG_AUTO_OPTI_SUM_EC1_LANE1PATH = 3021003
	REF_BRG_AUTO_OPTI_SUM_EC1_LANE2PATH = 3021004
	REF_BRG_AUTO_OPTI_SUM_EC1_LANE3PATH = 3021005
	REF_BRG_AUTO_OPTI_SUM_EC1_LANE4PATH = 3021006
	REF_BRG_AUTO_OPTI_SUM_EC1_TOTEFFECT = 3021007
	REF_BRG_AUTO_OPTI_SUM_EC1_FACEFFECT = 3021008
	REF_BRG_AUTO_OPTI_SUM_EC1_CRITICAL = 3021009
	
	REF_BRG_AUTO_OPTI_SUM_US = 3022000
	REF_BRG_AUTO_OPTI_SUM_US_NAME = 3022001
	REF_BRG_AUTO_OPTI_SUM_US_SIDE = 3022002
	REF_BRG_AUTO_OPTI_SUM_US_NUM_LDL = 3022003	 #number of loaded lanes				
	REF_BRG_AUTO_OPTI_SUM_US_LANEA = 3022004
	REF_BRG_AUTO_OPTI_SUM_US_LANEB = 3022005
	REF_BRG_AUTO_OPTI_SUM_US_LANEC = 3022006
	REF_BRG_AUTO_OPTI_SUM_US_TOTEFFECT = 3022007
	REF_BRG_AUTO_OPTI_SUM_US_CRITICAL = 3022008
	
	#REF_LAST_BRIDGE									
	
	#REF_FIRST_STAGE									
	
	REF_STAGE_DEFN = 4001000
	REF_STAGE_DEFN_NAME = 4001001
	REF_STAGE_DEFN_LIST = 4001002
	REF_STAGE_DEFN_PHI = 4001003
	
	REF_STAGE_PROP = 4002000
	REF_STAGE_PROP_STAGE = 4002001
	REF_STAGE_PROP_TYPE = 4002002
	REF_STAGE_PROP_ELEM_PROP = 4002003
	REF_STAGE_PROP_REC = 4002004
	
	#REF_LAST_STAGE								 
	
	#REF_FIRST_GENERAL									
	
	REF_LIST = 5001000
	REF_LIST_NAME = 5001001
	REF_LIST_TYPE = 5001002
	REF_LIST_DEFN = 5001003
	
	REF_ASSEMBLY = 5002000
	REF_ASSEMBLY_NAME = 5002001
	REF_ASSEMBLY_ELEM = 5002002
	REF_ASSEMBLY_TYPE = 5002003
	REF_ASSEMBLY_TOP1 = 5002004
	REF_ASSEMBLY_TOP2 = 5002005
	REF_ASSEMBLY_TOP3 = 5002006
	REF_ASSEMBLY_ITOP = 5002007
	REF_ASSEMBLY_FIT = 5002008
	REF_ASSEMBLY_AXIS = 5002009
	REF_ASSEMBLY_AUTO = 5002010
	REF_ASSEMBLY_END1 = 5002011
	REF_ASSEMBLY_END2 = 5002012
	REF_ASSEMBLY_Y = 5002013
	REF_ASSEMBLY_Z = 5002014
	REF_ASSEMBLY_DEFN = 5002015
	REF_ASSEMBLY_POINTS = 5002016
	REF_ASSEMBLY_SPACING = 5002017
	REF_ASSEMBLY_STOREY = 5002018
	REF_ASSEMBLY_EXPLICIT = 5002019
	
	
	#REF_LAST_GENERAL								 
	
	#REF_FIRST_RESULT		 
	
	#///////////////////////////////////////
	#// nodal results
	#///////////////////////////////////////
	
	REF_DISP = 12001000
	REF_DISP_DX = 12001001
	REF_DISP_DY = 12001002
	REF_DISP_DZ = 12001003
	REF_DISP_TRANS = 12001004
	REF_DISP_RXX = 12001005
	REF_DISP_RYY = 12001006
	REF_DISP_RZZ = 12001007
	REF_DISP_ROT = 12001008
	REF_DISP_DXY = 12001009
	
	REF_VEL = 12002000
	REF_VEL_VX = 12002001
	REF_VEL_VY = 12002002
	REF_VEL_VZ = 12002003
	REF_VEL_TRANS = 12002004
	REF_VEL_VXX = 12002005
	REF_VEL_VYY = 12002006
	REF_VEL_VZZ = 12002007
	REF_VEL_ROT = 12002008
	
	REF_ACC = 12003000
	REF_ACC_AX = 12003001
	REF_ACC_AY = 12003002
	REF_ACC_AZ = 12003003
	REF_ACC_TRANS = 12003004
	REF_ACC_AXX = 12003005
	REF_ACC_AYY = 12003006
	REF_ACC_AZZ = 12003007
	REF_ACC_ROT = 12003008
	
	REF_REAC = 12004000
	REF_REAC_force_x = 12004001
	REF_REAC_force_y = 12004002
	REF_REAC_force_z = 12004003
	REF_REAC_FRC = 12004004
	REF_REAC_moment_xx = 12004005
	REF_REAC_moment_yy = 12004006
	REF_REAC_moment_zz = 12004007
	REF_REAC_MOM = 12004008
	
	REF_FORCE_CONSTR = 12005000
	REF_FORCE_CONSTR_force_x = 12005001
	REF_FORCE_CONSTR_force_y = 12005002
	REF_FORCE_CONSTR_force_z = 12005003
	REF_FORCE_CONSTR_FRC = 12005004
	REF_FORCE_CONSTR_moment_xx = 12005005
	REF_FORCE_CONSTR_moment_yy = 12005006
	REF_FORCE_CONSTR_moment_zz = 12005007
	REF_FORCE_CONSTR_MOM = 12005008
	
	REF_FORCE_NODAL = 12006000
	REF_FORCE_NODAL_force_x = 12006001
	REF_FORCE_NODAL_force_y = 12006002
	REF_FORCE_NODAL_force_z = 12006003
	REF_FORCE_NODAL_FRC = 12006004
	REF_FORCE_NODAL_moment_xx = 12006005
	REF_FORCE_NODAL_moment_yy = 12006006
	REF_FORCE_NODAL_moment_zz = 12006007
	REF_FORCE_NODAL_MOM = 12006008
	
	REF_MASS_NODAL = 12007000
	REF_MASS_NODAL_MASS = 12007001
	REF_MASS_NODAL_IXX = 12007002
	REF_MASS_NODAL_IYY = 12007003
	REF_MASS_NODAL_IZZ = 12007004
	REF_MASS_NODAL_IXY = 12007005
	REF_MASS_NODAL_IYZ = 12007006
	REF_MASS_NODAL_IZX = 12007007
	
	REF_SOIL_NODAL = 12008000
	REF_SOIL_NODAL_AREA_X = 12008001
	REF_SOIL_NODAL_PRESSURE_X = 12008002
	REF_SOIL_NODAL_AREA_Y = 12008003
	REF_SOIL_NODAL_PRESSURE_Y = 12008004
	REF_SOIL_NODAL_AREA_Z = 12008005
	REF_SOIL_NODAL_PRESSURE_Z = 12008006
	REF_SOIL_NODAL_AREA_S = 12008007
	REF_SOIL_NODAL_PRESSURE_S = 12008008
	
	REF_FOOTFALL_RESON = 12009000
	REF_FOOTFALL_RESON_RESP_FACTOR = 12009001
	REF_FOOTFALL_RESON_PEAK_VELOCITY = 12009002
	REF_FOOTFALL_RESON_RMS_VELOCITY = 12009003
	REF_FOOTFALL_RESON_PEAK_ACC = 12009004
	REF_FOOTFALL_RESON_RMS_ACC = 12009005
	REF_FOOTFALL_RESON_CRIT_NODE = 12009006
	REF_FOOTFALL_RESON_CRIT_FREQ = 12009007
	
	REF_FOOTFALL_TRANS = 12009100
	REF_FOOTFALL_TRANS_RESP_FACTOR = 12009101
	REF_FOOTFALL_TRANS_PEAK_VELOCITY = 12009102
	REF_FOOTFALL_TRANS_RMS_VELOCITY = 12009103
	REF_FOOTFALL_TRANS_PEAK_ACC = 12009104
	REF_FOOTFALL_TRANS_RMS_ACC = 12009105
	REF_FOOTFALL_TRANS_CRIT_NODE = 12009106
	REF_FOOTFALL_TRANS_CRIT_FREQ = 12009107
	
	REF_FOOTFALL_SUM = 12009200
	REF_FOOTFALL_SUM_RESON_RESP_FACTOR = 12009201
	REF_FOOTFALL_SUM_TRANS_RESP_FACTOR = 12009202
	REF_FOOTFALL_SUM_MAX_RESP_FACTOR = 12009203
	
	#///////////////////////////////////////
	#// nodal results on elements - table placeholders only; components as per respective nodal tables
	#///////////////////////////////////////
	
	REF_VEL_ON_ELEM = 12502000
	
	REF_ACC_ON_ELEM = 12503000
	
	REF_SOIL_NODAL_ON_ELEM = 12508000
	
	REF_FOOTFALL_RESON_ON_ELEM = 12509000
	
	REF_FOOTFALL_TRANS_ON_ELEM = 12509100
	
	REF_FOOTFALL_SUM_ON_ELEM = 12509200
	
	#///////////////////////////////////////
	#// 0D element results
	#///////////////////////////////////////
	
	REF_DISP_EL0D = 13001000
	REF_DISP_EL0D_DX = 13001001
	REF_DISP_EL0D_DY = 13001002
	REF_DISP_EL0D_DZ = 13001003
	REF_DISP_EL0D_TRANS = 13001004
	REF_DISP_EL0D_RXX = 13001005
	REF_DISP_EL0D_RYY = 13001006
	REF_DISP_EL0D_RZZ = 13001007
	REF_DISP_EL0D_ROT = 13001008
	
	REF_FORCE_EL0D = 13002000
	REF_FORCE_EL0D_force_x = 13002001
	REF_FORCE_EL0D_force_y = 13002002
	REF_FORCE_EL0D_force_z = 13002003
	REF_FORCE_EL0D_FRC = 13002004
	REF_FORCE_EL0D_moment_xx = 13002005
	REF_FORCE_EL0D_moment_yy = 13002006
	REF_FORCE_EL0D_moment_zz = 13002007
	REF_FORCE_EL0D_MOM = 13002008
	
	#///////////////////////////////////////
	#// 1D element results
	#///////////////////////////////////////
	
	REF_DISP_EL1D = 14001000
	REF_DISP_EL1D_DX = 14001001
	REF_DISP_EL1D_DY = 14001002
	REF_DISP_EL1D_DZ = 14001003
	REF_DISP_EL1D_TRANS = 14001004
	REF_DISP_EL1D_RXX = 14001005
	REF_DISP_EL1D_RYY = 14001006
	REF_DISP_EL1D_RZZ = 14001007
	REF_DISP_EL1D_ROT = 14001008
	
	REF_FORCE_EL1D = 14002000
	REF_FORCE_EL1D_FX = 14002001
	REF_FORCE_EL1D_FY = 14002002
	REF_FORCE_EL1D_FZ = 14002003
	REF_FORCE_EL1D_FRC = 14002004
	REF_FORCE_EL1D_MXX = 14002005
	REF_FORCE_EL1D_MYY = 14002006
	REF_FORCE_EL1D_MZZ = 14002007
	REF_FORCE_EL1D_MOM = 14002008
	
	REF_STRESS_EL1D = 14003000
	REF_STRESS_EL1D_A = 14003001
	REF_STRESS_EL1D_SY = 14003002
	REF_STRESS_EL1D_SZ = 14003003
	REF_STRESS_EL1D_BY_POSZ = 14003004
	REF_STRESS_EL1D_BY_NEGZ = 14003005
	REF_STRESS_EL1D_BZ_POSY = 14003006
	REF_STRESS_EL1D_BZ_NEGY = 14003007
	REF_STRESS_EL1D_C1 = 14003008
	REF_STRESS_EL1D_C2 = 14003009
	REF_STRESS_EL1D_CY = 14003010
	REF_STRESS_EL1D_CZ = 14003011
	
	REF_STRESS_EL1D_DRV = 14003200
	REF_STRESS_EL1D_DRV_SY = 14003201
	REF_STRESS_EL1D_DRV_SZ = 14003202
	REF_STRESS_EL1D_DRV_ST = 14003203
	REF_STRESS_EL1D_DRV_V_MISES = 14003204
	
	REF_STRAIN_EL1D = 14003500
	REF_STRAIN_EL1D_A = 14003501
	
	REF_SED_EL1D = 14004000
	REF_SED_EL1D_WEB = 14004001
	REF_SED_EL1D_FLG = 14004002
	REF_SED_EL1D_TOT = 14004003
	
	REF_SED_AVG_EL1D = 14005000
	REF_SED_AVG_EL1D_WEB = 14005001
	REF_SED_AVG_EL1D_FLG = 14005002
	REF_SED_AVG_EL1D_TOT = 14005003
	
	REF_STL_UTIL = 14006000			#per member	
	REF_STL_UTIL_OVER_ALL = 14006001
	REF_STL_UTIL_LCL_case = 14006002
	REF_STL_UTIL_BCK_case = 14006003
	REF_STL_UTIL_LCL_A = 14006004
	REF_STL_UTIL_LCL_SU = 14006005
	REF_STL_UTIL_LCL_SV = 14006006
	REF_STL_UTIL_LCL_moment_xx = 14006007
	REF_STL_UTIL_LCL_MUU = 14006008
	REF_STL_UTIL_LCL_MVV = 14006009
	REF_STL_UTIL_BCK_UU = 14006010
	REF_STL_UTIL_BCK_VV = 14006011
	REF_STL_UTIL_BCK_LT = 14006012
	REF_STL_UTIL_BCK_TOR = 14006013
	REF_STL_UTIL_BCK_FT = 14006014
	
	REF_STL_DES_SUMMARY = 14006200			#per member		 
	REF_STL_DES_CALC_VRB = 14006400			#per member			
	REF_STL_DES_CALC_BRF = 14006500			#per member			
	REF_STL_DES_MEMB_EFF_LEN = 14006600			#per member				
	
	REF_TORCE_LINE = 14007000
	REF_TORCE_LINE_POS = 14007001
	REF_TORCE_LINE_NEG = 14007002
	
	REF_SHEAR_LINE = 14008000
	REF_SHEAR_LINE_POS = 14008001
	REF_SHEAR_LINE_NEG = 14008002
	
	#///////////////////////////////////////
	#// 2D element results
	#///////////////////////////////////////
	
	#REF_FIRST_EL2D_RESULT									
	
	REF_DISP_EL2D = 15001000
	REF_DISP_EL2D_DX = 15001001
	REF_DISP_EL2D_DY = 15001002
	REF_DISP_EL2D_DZ = 15001003
	REF_DISP_EL2D_TRANS = 15001004
	REF_DISP_EL2D_RXX = 15001005
	REF_DISP_EL2D_RYY = 15001006
	REF_DISP_EL2D_RZZ = 15001007
	REF_DISP_EL2D_ROT = 15001008
	
	REF_FORCE_EL2D_DRV = 15002000
	REF_FORCE_EL2D_DRV_PRIN_M = 15002001
	REF_FORCE_EL2D_DRV_PRIN_N = 15002002
	REF_FORCE_EL2D_DRV_MMAX = 15002003
	REF_FORCE_EL2D_DRV_MMIN = 15002004
	REF_FORCE_EL2D_DRV_MANG = 15002005
	REF_FORCE_EL2D_DRV_NMAX = 15002006
	REF_FORCE_EL2D_DRV_NMIN = 15002007
	REF_FORCE_EL2D_DRV_NANG = 15002008
	REF_FORCE_EL2D_DRV_QMAX = 15002009
	
	REF_MOMENT_EL2D_PRJ = 15003000
	REF_MOMENT_EL2D_PRJ_M = 15003001
	REF_MOMENT_EL2D_PRJ_MX = 15003002
	REF_MOMENT_EL2D_PRJ_MY = 15003003
	REF_MOMENT_EL2D_PRJ_MXY = 15003004
	REF_MOMENT_EL2D_PRJ_MXMXY = 15003005
	REF_MOMENT_EL2D_PRJ_MYMYX = 15003006
	
	REF_FORCE_EL2D_PRJ = 15004000
	REF_FORCE_EL2D_PRJ_N = 15004001
	REF_FORCE_EL2D_PRJ_Q = 15004002
	REF_FORCE_EL2D_PRJ_NX = 15004003
	REF_FORCE_EL2D_PRJ_NY = 15004004
	REF_FORCE_EL2D_PRJ_NXY = 15004005
	REF_FORCE_EL2D_PRJ_QX = 15004006
	REF_FORCE_EL2D_PRJ_QY = 15004007
	
	REF_STRESS_EL2D_DRV = 15005000
	REF_STRESS_EL2D_DRV_PRIN = 15005001
	REF_STRESS_EL2D_DRV_MAX = 15005002
	REF_STRESS_EL2D_DRV_MIN = 15005003
	REF_STRESS_EL2D_DRV_ANG = 15005004
	REF_STRESS_EL2D_DRV_MAX_SH = 15005005
	REF_STRESS_EL2D_DRV_V_MISES = 15005006
	REF_STRESS_EL2D_DRV_AVERAGE = 15005007
	
	REF_STRESS_EL2D_AX = 15006000
	REF_STRESS_EL2D_AX_XX = 15006001
	REF_STRESS_EL2D_AX_YY = 15006002
	REF_STRESS_EL2D_AX_ZZ = 15006003
	REF_STRESS_EL2D_AX_XY = 15006004
	REF_STRESS_EL2D_AX_YZ = 15006005
	REF_STRESS_EL2D_AX_ZX = 15006006
	
	REF_STRESS_EL2D_PRJ = 15007000
	REF_STRESS_EL2D_PRJ_XX = 15007001
	REF_STRESS_EL2D_PRJ_YY = 15007002
	REF_STRESS_EL2D_PRJ_ZZ = 15007003
	REF_STRESS_EL2D_PRJ_XY = 15007004
	REF_STRESS_EL2D_PRJ_YZ = 15007005
	REF_STRESS_EL2D_PRJ_ZX = 15007006
	
	REF_EL2D_STRESS_ERROR = 15008000
	REF_EL2D_STRESS_ERROR_ERR = 15008001
	
	REF_SOIL_EL2D = 15009000
	REF_SOIL_EL2D_PRESSURE = 15009001
	
	REF_FORCE_ELWALL = 15009500
	REF_FORCE_ELWALL_force_x = 15009501
	REF_FORCE_ELWALL_force_y = 15009502
	REF_FORCE_ELWALL_force_z = 15009503
	REF_FORCE_ELWALL_FRC = 15009504
	REF_FORCE_ELWALL_moment_xx = 15009505
	REF_FORCE_ELWALL_moment_yy = 15009506
	REF_FORCE_ELWALL_moment_zz = 15009507
	REF_FORCE_ELWALL_MOM = 15009508
	
	REF_RC_SLAB_REINF = 15010000
	REF_RC_SLAB_REINF_NOTE = 15010001
	REF_RC_SLAB_REINF_TOP_A = 15010002
	REF_RC_SLAB_REINF_TOP_B = 15010003
	REF_RC_SLAB_REINF_BOT_A = 15010004
	REF_RC_SLAB_REINF_BOT_B = 15010005
	REF_RC_SLAB_REINF_TOP = 15010006
	REF_RC_SLAB_REINF_BOT = 15010007
	
	#REF_LAST_EL2D_RESULT									 
	
	#///////////////////////////////////////
	#// element gen results
	#///////////////////////////////////////
	
	#REF_FIRST_ELGEN_RESULT									 
	
	REF_STRAIN_ENERGY = 16001000
	REF_STRAIN_ENERGY_ENERGY = 16001001
	REF_VIRTUAL_ENERGY = 16002000
	REF_VIRTUAL_ENERGY_ENERGY = 16002001
	REF_RES_ACCURACY = 16008000
	REF_RES_ACCURACY_SIGFIG = 16008001
	
	#///////////////////////////////////////
	#// assembly results
	#///////////////////////////////////////
	
	REF_DISP_ASSEM = 18001000
	REF_DISP_ASSEM_DX = 18001001
	REF_DISP_ASSEM_DY = 18001002
	REF_DISP_ASSEM_DZ = 18001003
	REF_DISP_ASSEM_TRANS = 18001004
	REF_DISP_ASSEM_RXX = 18001005
	REF_DISP_ASSEM_RYY = 18001006
	REF_DISP_ASSEM_RZZ = 18001007
	REF_DISP_ASSEM_ROT = 18001008
	
	REF_DRIFT_ASSEM = 18001500
	REF_DRIFT_ASSEM_DX = 18001501
	REF_DRIFT_ASSEM_DY = 18001502
	REF_DRIFT_ASSEM_DZ = 18001503
	REF_DRIFT_ASSEM_TRANS = 18001504
	REF_DRIFT_ASSEM_RXX = 18001505
	REF_DRIFT_ASSEM_RYY = 18001506
	REF_DRIFT_ASSEM_RZZ = 18001507
	REF_DRIFT_ASSEM_ROT = 18001508
	REF_DRIFT_ASSEM_DXY = 18001509
	
	REF_DRIFTINDEX_ASSEM = 18001600
	REF_DRIFTINDEX_ASSEM_DX = 18001601
	REF_DRIFTINDEX_ASSEM_DY = 18001602
	REF_DRIFTINDEX_ASSEM_DZ = 18001603
	REF_DRIFTINDEX_ASSEM_TRANS = 18001604
	REF_DRIFTINDEX_ASSEM_RXX = 18001605
	REF_DRIFTINDEX_ASSEM_RYY = 18001606
	REF_DRIFTINDEX_ASSEM_RZZ = 18001607
	REF_DRIFTINDEX_ASSEM_ROT = 18001608
	REF_DRIFTINDEX_ASSEM_DXY = 18001609
	
	REF_FORCE_ASSEM = 18002000
	REF_FORCE_ASSEM_force_x = 18002001
	REF_FORCE_ASSEM_force_y = 18002002
	REF_FORCE_ASSEM_force_z = 18002003
	REF_FORCE_ASSEM_FRC = 18002004
	REF_FORCE_ASSEM_moment_xx = 18002005
	REF_FORCE_ASSEM_moment_yy = 18002006
	REF_FORCE_ASSEM_moment_zz = 18002007
	REF_FORCE_ASSEM_MOM = 18002008
