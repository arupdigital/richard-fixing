from richard import app, celery
from richard.forms import *
from richard.gsa import *
from richard.models import *
import richard.models
from richard.database import init_db, sa_config
import os
from flask import flash, render_template, redirect, url_for, request, Response, send_from_directory, send_file, jsonify
from werkzeug import secure_filename
from pythoncom import CoInitialize
from sqlalchemy import desc, asc, distinct, event, create_engine, inspect
from sqlalchemy.orm import sessionmaker
import json
from zipfile import ZipFile
from glob import glob
from itertools import cycle
import pdb
import time
import pickle

config = {
    'user': 'readwrite',
    'password': 'arupNYsql',
    'host': '10.160.156.226',
    'client_flags': [ClientFlag.LOCAL_FILES]}


@app.route('/optimize/<schema_name>', methods=['GET', 'POST'])
def optimize(schema_name):
    model_optimize.apply_async([schema_name])
    # model_optimize(schema_name)
    return redirect(url_for('schema', schema_name=schema_name))


@app.route('/new_task', methods=['GET', 'POST'])
def new_task():
    # get args
    schema_name = request.args.get('schema_name', type=str)
    t = json.loads(request.args.get('task_data', type=str))

    try:

        sa_config['database'] = schema_name
        engine = create_engine(url.URL(**sa_config))
        Session = sessionmaker(bind=engine)
        db_session = Session()

        # configure schema specific gsa cache folder
        dir = os.path.dirname(__file__)
        gsa_cache_folder = os.path.join(dir, 'gsa_cache', schema_name)
        if not os.path.exists(gsa_cache_folder): os.makedirs(gsa_cache_folder)

        # commit task to db
        task_id = db_session.query(func.max(Task.id)).first()[0] + 1
        task = Task(task_id, t['module'], int(t['step_limit']))
        db_session.add(task)

        # make zero step
        step = Step(task_id, 0)
        db_session.add(step)
        db_session.commit()

        # commit systems to db
        for system_id, s in t['systems'].items():

            a = AppSystem.query.filter_by(module_id=t['module'], system_id=system_id).first()
            result_list = []
            if a.force_x == 1: result_list.append('fx')
            if a.force_y == 1: result_list.append('fy')
            if a.force_z == 1: result_list.append('fz')
            if a.force_srss == 1: result_list.append('frc')
            if a.moment_xx == 1: result_list.append('mxx')
            if a.moment_yy == 1: result_list.append('myy')
            if a.moment_zz == 1: result_list.append('mzz')
            if a.moment_srss == 1: result_list.append('mom')

            system = System(task_id, step.id, system_id, a.position_count, pickle.dumps(result_list))
            db_session.add(system)

            # commit args to db
            for arg_id, arg_value in s['args'].items():
                arg = Arg(task_id, system_id, arg_id, arg_value)
                db_session.add(arg)
            db_session.commit()

            for aa in AppArg.query.filter(AppArg.module_id == t['module'], \
                                          AppArg.system_id == system_id, \
                                          AppArg.unit_conversion != 1):
                arg = db_session.query(Arg).filter_by(task_id=task_id, system_id=system_id, arg_id=aa.arg_id).first()
                arg.arg_value = float(arg.arg_value) * aa.unit_conversion
            db_session.commit()


    except:
        exc_type, exc_value, tb = sys.exc_info()
        db_session.rollback()
        step = db_session.query(Step).filter_by(status='ACTIVE').first()
        task = db_session.query(Task).filter_by(status='ACTIVE').first()
        if step is not None: step.status = 'ERROR'
        if task is not None: task.status = 'ERROR'
        db_session.commit()
        if tb is not None:
            prev = tb
            curr = tb.tb_next
            while curr is not None:
                prev = curr
                curr = curr.tb_next
            print('error_line=' + str(tb.tb_lineno), prev.tb_frame.f_locals)

    finally:
        db_session.close()
        engine.dispose()
        return redirect(url_for('schema', schema_name=schema_name))


@app.route('/richard/diary', methods=['GET', 'POST'])
def diary():
    init_form = InitForm(csrf_enabled=False)

    if init_form.validate_on_submit():

        # Initialize COM
        CoInitialize()

        # initial iteration
        task = Task(0, 'init', 0)
        task.descr = init_form.descr.data
        task.status = 'ACTIVE'
        step = Step(task.id, 0)

        # get GSA model from form and save in cache
        gsamodel = init_form.gsamodel.data
        filename = secure_filename(gsamodel.filename)
        schema_name = filename.split('.')[0]
        create_db(schema_name)

        dir = os.path.dirname(__file__)
        gsa_cache_folder = os.path.join(dir, 'gsa_cache', schema_name)
        if not os.path.exists(gsa_cache_folder): os.makedirs(gsa_cache_folder)
        gwb_path = os.path.join(gsa_cache_folder, '0_0.gwb')
        gsamodel.save(gwb_path)
        step.gwb_path = gwb_path

        # pass record to task for model init to run in background
        model_init.apply_async([task, step, schema_name])
        # model_init(task, step, schema_name)

        return redirect(url_for('diary'))

    schemata = get_schemata()
    return render_template('diary.html', init_form=init_form, schemata=schemata)


@app.route('/richard/diary/<schema_name>', methods=['GET', 'POST'])
def schema(schema_name):
    modules = get_module()
    tasks = get_tasks(schema_name)

    return render_template('schema.html', schema_name=schema_name, tasks=tasks, modules=modules)


@app.route('/update_queue/<schema_name>', methods=['GET'])
def update_queue(schema_name):
    if request.headers.get('accept') == 'text/event-stream':
        tasks = get_tasks(schema_name, ordered=False)
        r = 'data: %s\n\n' % json.dumps(tasks)
        time.sleep(1)  # an artificial delay
        return Response(r, content_type='text/event-stream')


@app.route('/delete/<schema_name>', methods=['POST'])
def delete(schema_name):
    init_form = InitForm(csrf_enabled=False)

    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    # drop schema
    sql = "DROP DATABASE If Exists `" + schema_name + "`;"

    cursor.execute(sql)
    cursor.close()
    cnx.close()

    schemata = get_schemata()
    return redirect(url_for('diary'))


@app.route('/get_step_args', methods=['GET'])
def get_step_args():
    task_id = request.args.get('task_id', type=int)
    step_id = request.args.get('step_id', type=int)
    schema_name = request.args.get('schema_name', type=str)

    task = {'task_id': task_id,
            'step_id': step_id,
            'systems': get_tasks(schema_name)[task_id]['systems'],
            'step': get_tasks(schema_name)[task_id]['steps'][step_id]}

    return jsonify(task)


@app.route('/send_gwb/<schema_name>/<task_id>/<step_id>', methods=['GET'])
def send_gwb(schema_name, task_id, step_id):
    sa_config['database'] = schema_name
    engine = create_engine(url.URL(**sa_config))
    Session = sessionmaker(bind=engine)
    db_session = Session()

    gwb_path = db_session.query(Step).filter_by(task_id=task_id, id=step_id).first().gwb_path
    directory, filename = os.path.split(gwb_path)

    db_session.close()

    outfile = schema_name + '_' + filename
    return send_file(gwb_path, attachment_filename=outfile, as_attachment=True)


@app.route('/richard/about', methods=['GET'])
def about():
    app.config['JSON_CACHE_FOLDER'] = os.path.join(os.path.dirname(__file__), 'json_temp')

    modules = get_module()
    return render_template('about.html', modules=modules)


@app.route('/get_load_cases', methods=['GET'])
def get_load_cases():
    schema_name = request.args.get('schema_name', type=str)

    sa_config['database'] = schema_name
    engine = create_engine(url.URL(**sa_config))
    Session = sessionmaker(bind=engine)
    db_session = Session()

    load_cases = []
    for l in db_session.query(LoadCase):
        load_cases.append(l.name)

    db_session.close()
    return jsonify(json_list=load_cases)


@app.route('/get_list_names', methods=['GET'])
def get_list_names():
    print('GET LIST NAMES')

    schema_name = request.args.get('schema_name', type=str)
    list_type = request.args.get('list_type', type=str)

    sa_config['database'] = schema_name
    engine = create_engine(url.URL(**sa_config))
    Session = sessionmaker(bind=engine)
    db_session = Session()

    list_names = []
    for l in db_session.query(List).filter_by(type=list_type):
        list_names.append(l.name)

    db_session.close()
    return jsonify(json_list=list_names)


@app.route('/get_result_case_ids', methods=['GET'])
def get_result_case_ids():
    schema_name = request.args.get('schema_name', type=str)
    list_name = request.args.get('list_name', type=str)

    sa_config['database'] = schema_name
    engine = create_engine(url.URL(**sa_config))
    Session = sessionmaker(bind=engine)
    db_session = Session()

    for l in db_session.query(List).filter_by(type="CASE", name=list_name).all():
        result_case_ids = l.description.decode("utf-8").replace(' \r', '').split(' ')

    db_session.close()
    return jsonify(json_list=result_case_ids)


@app.route('/get_charts')
def get_charts():
    schema_name = request.args.get('schema_name', type=str)
    task_id = request.args.get('task_id', type=int)
    step_id = request.args.get('step_id', type=int)

    sa_config['database'] = schema_name
    engine = create_engine(url.URL(**sa_config))
    Session = sessionmaker(bind=engine)
    db_session = Session()

    charts = {}
    for c in db_session.query(Chart).filter_by(task_id=task_id, step_id=step_id):
        charts[c.id] = {'task_id': c.task_id,
                        'step_id': c.step_id,
                        'data': pickle.loads(c.data), }

    db_session.close()
    engine.dispose()

    return jsonify(charts)


@app.route('/mex', methods=['GET', 'POST'])
def mex():
    form = JsonForm(csrf_enabled=False)

    if form.validate_on_submit():

        # get GSA model from form and save in cache
        o = form.json.data

        filename = secure_filename(o.filename)
        app.config['JSON_CACHE_FOLDER'] = os.path.join(os.path.dirname(__file__), 'json_temp')
        if not os.path.exists(app.config['JSON_CACHE_FOLDER']):
            os.makedirs(app.config['JSON_CACHE_FOLDER'])
        fp = os.path.join(app.config['JSON_CACHE_FOLDER'], filename)
        o.save(fp)

        return render_template('circle_pack.html', filename=filename)

    return render_template('mex.html', form=form)


@app.route('/mex/send_gh', methods=['POST'])
def send_gh():
    userobj_dir = 'E:/gh_arup/gh_Arup/*/py/*.ghuser'
    userobj = glob(userobj_dir)
    assembly_dir = 'E:/gh_arup/bin/*.gha'
    assembly = glob(assembly_dir)

    outfile = 'E:/gh_arup/gh_Arup.zip'
    with ZipFile(outfile, 'w') as myzip:
        for fp in assembly:
            arcname = os.path.join('assembly', os.path.basename(fp))
            myzip.write(fp, arcname)
        for fp in userobj:
            arcname = os.path.join('userobj', os.path.basename(fp))
            myzip.write(fp, arcname)

    filename = os.path.basename(outfile)
    return send_file(outfile, attachment_filename=filename, as_attachment=True)


@app.route('/mex/circle_pack', methods=['GET', 'POST'])
def circle_pack():
    app.config['JSON_CACHE_FOLDER'] = os.path.join(os.path.dirname(__file__), 'json_temp')
    return render_template('circle_pack.html', filename="flare.json")


# Custom static data
@app.route('/json_temp/<path:filename>')
def json_temp(filename):
    return send_from_directory(app.config['JSON_CACHE_FOLDER'], filename)
