# coding: utf-8
from sqlalchemy import event, DDL, and_, Column, MetaData, DateTime, Float, Index, Boolean, Integer, Numeric, SmallInteger, ForeignKey, String, Table, text, PrimaryKeyConstraint
from sqlalchemy.sql import func
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import object_session, relationship
from sqlalchemy.dialects.mysql import LONGBLOB, BIGINT
from richard.database import Base
from time import gmtime, strftime, time
import math



metadata = MetaData()

#Richard Tables
class AppModule(Base):
	__tablename__ = 'app_module'
	__table_args__ = (
		{'schema': 'richard_v2', 'mysql_engine':'MyISAM'}
	)

	module_id = Column(String(4), primary_key=True, autoincrement=False)
	name = Column(String(30))
	description = Column(String(200))
	pos = Column(Integer, nullable=False)

class AppSystem(Base):
	__tablename__ = 'app_system'
	__table_args__ = (
		PrimaryKeyConstraint('module_id', 'system_id'),
		{'schema': 'richard_v2', 'mysql_engine':'MyISAM'}
	)

	module_id = Column(String(4), nullable=False, server_default=text("'0'"))
	system_id = Column(String(100), nullable=False)
	title = Column(String(100), nullable=False)
	description = Column(String(500), nullable=False)
	pos = Column(Integer, nullable=False)
	opt = Column(String(8))
	add = Column(String(8))
	chk = Column(String(8))
	colour = Column(String(7))
	object = Column(String(10))
	position_count = Column(Integer)
	force_x = Column(Integer)
	force_y = Column(Integer)
	force_z = Column(Integer)
	force_srss = Column(Integer)
	moment_xx = Column(Integer)
	moment_yy = Column(Integer)
	moment_zz = Column(Integer)
	moment_srss = Column(Integer)

class AppArg(Base):
	__tablename__ = 'app_arg'
	__table_args__ = (
		PrimaryKeyConstraint('module_id', 'system_id', 'arg_id'),
		Index('app_arg_module_id_system_id_arg_id_validation', 'module_id', 'system_id', 'arg_id', 'validation'),
		Index('app_arg_module_id_system_id_arg_id_error', 'module_id', 'system_id', 'arg_id', 'error'),
		Index('app_arg_module_id_system_id_arg_id_data_type', 'module_id', 'system_id', 'arg_id', 'data_type'),
		Index('app_arg_module_id_system_id_arg_id_field_type', 'module_id', 'system_id', 'arg_id', 'field_type'),
		Index('app_arg_module_id_system_id_arg_id_units', 'module_id', 'system_id', 'arg_id', 'units'),
		Index('app_arg_module_id_system_id_arg_id_order', 'module_id', 'system_id', 'arg_id', 'pos'),
		Index('app_arg_module_id_system_id_arg_id_visibility', 'module_id', 'system_id', 'arg_id', 'visibility'),
		{'schema': 'richard_v2', 'mysql_engine':'MyISAM'}
	)
	
	module_id = Column(String(4), nullable=False)
	system_id = Column(String(4), nullable=False)
	arg_id = Column(String(100))
	description = Column(String(1000))
	validation = Column(String(100))
	error = Column(String(100))
	data_type = Column(String(30))
	field_type = Column(String(100))
	units = Column(String(10))
	pos = Column(Integer, nullable=False)
	visibility = Column(String(6))
	default = Column(String(100))  
	unit_conversion = Column(Numeric(10, 6), default=1)

	def __init__(self, module_id, system_id, arg_id):
		self.module_id = module_id
		self.system_id = system_id
		self.arg_id = arg_id

class SectionDesign(Base):
	__tablename__ = 'section_design'
	__table_args__ = (
		PrimaryKeyConstraint('system_id', 'effective_length', 'name', 'section_type'),
		Index('name_length_system', 'name', 'effective_length', 'system_id'),
		{'schema': 'richard_v2', 'mysql_engine':'MyISAM'}
	)
	
	system_id = Column(String(4), nullable=False)
	effective_length = Column(Integer, nullable=False)
	name = Column(String(10), nullable=False)
	section_type = Column(String(3), nullable=False)
	fy = Column(Integer, nullable=False)
	E = Column(Integer, nullable=False)
	phi_Pnt = Column(BIGINT, nullable=False)
	phi_Pnc = Column(BIGINT, nullable=False)
	phi_Vnyy = Column(BIGINT, nullable=False)
	phi_Vnzz = Column(BIGINT, nullable=False)
	phi_Mnyy = Column(BIGINT, nullable=False)
	phi_Mnzz = Column(BIGINT, nullable=False)

class SectionLibrary(Base):
	__tablename__ = 'section_library'
	__table_args__ = (
		PrimaryKeyConstraint('system_id', 'name', 'section_type'),
		Index('systemid_sectiontype', 'system_id', 'section_type'),
		{'schema': 'richard_v2', 'mysql_engine':'MyISAM'}
	)
	
	system_id = Column(String(4), nullable=False)
	name = Column(String(30), nullable=False)
	section_type = Column(String(4), nullable=False)
	area = Column(BIGINT(20))

#Model Specific Tables
class Task(Base):
	__tablename__ = 'task'

	id = Column(Integer, primary_key=True, autoincrement=False)
	module_id = Column(String(4))
	status = Column(String(10))
	step_limit = Column(Integer)
	time_limit = Column(DateTime)
	descr = Column(String(200))

	def __init__(self, id, module_id, step_limit):
		self.id = id
		self.module_id = module_id
		self.step_limit = step_limit
		self.status = 'PENDING'

class Step(Base):
	__tablename__ = 'step'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'id'),
		Index('step_status_index', 'status'),
		Index('step_status_step_index', 'status', 'id'),
		Index('step_status_task_index', 'status', 'task_id'),
		Index('step_status_task_id_step_id','status','id','task_id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, autoincrement=False)
	id = Column(Integer, autoincrement=False)
	status = Column(String(10))
	time_0 = Column(DateTime)
	time_1 = Column(DateTime)
	gwb_path = Column(String(260))

	def __init__(self, task_id, id):
		self.id = id
		self.task_id = task_id
		self.status = 'PENDING'	   
		self.time_0 = gmtime()

	def cache_mass(self):
		"""
		save mass for each system in task at this step
		"""

class Profile(Base):
	__tablename__ = 'profile'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, autoincrement=False)
	step_id = Column(Integer, autoincrement=False)
	id = Column(Integer, autoincrement=False)
	time_start = Column(DateTime)
	time_end = Column(DateTime)
	time_elapsed = Column(Integer)

	def __init__(self, task_id, step_id, id):
		self.id = id
		self.task_id = task_id
		self.step_id = step_id
		self.time_start = gmtime()

class System(Base):
	__tablename__ = 'system'
	__table_args__ = (
		PrimaryKeyConstraint('id', 'task_id', 'step_id'),
		Index('system_index', 'id'),
		Index('step_system_index', 'id', 'step_id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	id = Column(String(4), nullable=False)
	elements = Column(LONGBLOB(length=None))
	nodes = Column(LONGBLOB(length=None))
	cases = Column(LONGBLOB(length=None))
	position_count = Column(Integer)
	result_list = Column(LONGBLOB(length=None))
	mass = Column(Integer)

	def __init__(self, task_id, step_id, id, position_count, result_list):
		self.task_id = task_id
		self.step_id = step_id
		self.id = id
		self.position_count = position_count
		self.result_list = result_list

class Arg(Base):
	__tablename__ = 'arg'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'system_id', 'arg_id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	system_id = Column(String(4), nullable=False)
	arg_id = Column(String(100), nullable=False)
	arg_value = Column(String(100))

	def __init__(self, task_id, system_id, arg_id, arg_value):
		self.task_id = task_id
		self.system_id = system_id
		self.arg_id = arg_id
		self.arg_value = arg_value

class SectionProp(Base):
	__tablename__ = 'section_property'
	__table_args__ = (
		Index('section_property_id_index', 'name', 'id'),
		Index('section_property_name_index', 'id', 'name'),
		Index('section_property_area_index', 'id', 'area'),
		Index('section_property_material_index', 'id', 'material_id'),
		Index('section_property_d_index', 'd'),
		Index('section_property_d_id_index', 'd', 'id'),
		Index('section_property_name','name'),
		{'mysql_engine':'MyISAM'}
	)

	keyword = Column(String(100))
	id = Column(Integer, primary_key=True, server_default=text("'0'"))
	geoname = Column(String(50))
	name = Column(String(50))
	material_id = Column(String(50))
	description = Column(String(100))
	area = Column(Numeric(20, 6))
	Iyy = Column(Numeric(20, 6))
	Izz = Column(Numeric(20, 6))
	J = Column(Numeric(20, 6))
	Ky = Column(Numeric(20, 6))
	Kz = Column(Numeric(20, 6))
	d = Column(Numeric(10, 2))

class ForceMoment(Base):
	__tablename__ = 'force_moment'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'element_id', 'position', 'result_case_id', 'envelope_method'),
		Index('force_moment_force_x_index', 'task_id', 'step_id', 'element_id', 'position', 'result_case_id', 'envelope_method', 'force_x'),
		Index('force_moment_envelope_method_moment_yy_index', 'task_id', 'step_id', 'element_id', 'position', 'result_case_id', 'envelope_method', 'moment_yy'),
		Index('force_moment_envelope_method_moment_zz_index', 'task_id', 'step_id', 'element_id', 'position', 'result_case_id', 'envelope_method', 'moment_zz'),
		Index('force_moment_envelope_method_moment_srss_index', 'task_id', 'step_id', 'element_id', 'position', 'result_case_id', 'envelope_method', 'moment_srss'),
		Index('force_moment_envelope_method_moment_yy_zz_index', 'task_id', 'step_id', 'element_id', 'position', 'result_case_id', 'envelope_method', 'moment_yy', 'moment_zz'),
		Index('force_moment_moment_yy_index', 'task_id', 'step_id', 'element_id', 'position', 'result_case_id', 'moment_yy'),
		Index('force_moment_moment_zz_index', 'task_id', 'step_id', 'element_id', 'position', 'result_case_id', 'moment_zz'),
		Index('force_moment_moment_srss_index', 'task_id', 'step_id', 'element_id', 'position', 'result_case_id', 'moment_srss'),
		Index('force_moment_moment_yy_zz_index', 'task_id', 'step_id', 'element_id', 'position', 'result_case_id', 'moment_yy', 'moment_zz'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	element_id = Column(Integer, nullable=False, server_default=text("'0'"))
	position = Column(SmallInteger, nullable=False, server_default=text("'0'"))
	result_case_id = Column(String(10), nullable=False, server_default=text("''"))
	envelope_method = Column(String(10), nullable=False, server_default=text("''"))
	force_x = Column(BIGINT)
	force_y = Column(BIGINT)
	force_z = Column(BIGINT)
	force_srss = Column(BIGINT)
	moment_xx = Column(BIGINT)
	moment_yy = Column(BIGINT)
	moment_zz = Column(BIGINT)
	moment_srss = Column(BIGINT)

class NodeDisp(Base):
	__tablename__ = 'node_displacement'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'node_id', 'result_case_id', 'envelope_method'),
		Index('node_displacement_node_Dz_index', 'task_id', 'step_id', 'node_id', 'result_case_id', 'translation_z'),
		Index('node_displacement_node_Dx_index', 'task_id', 'step_id', 'node_id', 'result_case_id', 'translation_x'),
		Index('node_displacement_node_Ryy_index', 'task_id', 'step_id', 'node_id', 'result_case_id', 'rotation_yy'),
		Index('node_displacement_node_Dxy_index', 'task_id', 'step_id', 'node_id', 'result_case_id', 'translation_xy'),
		Index('node_displacement_node_Trans_index', 'task_id', 'step_id', 'node_id', 'result_case_id', 'translation_srss'),
		Index('node_displacement_node_Rzz_index', 'task_id', 'step_id', 'node_id', 'result_case_id', 'rotation_zz'),
		Index('node_displacement_node_Dy_index', 'task_id', 'step_id', 'node_id', 'result_case_id', 'translation_y'),
		Index('node_displacement_node_Rxx_index', 'task_id', 'step_id', 'node_id', 'result_case_id', 'rotation_xx'),
		Index('node_displacement_node_ROT_index', 'task_id', 'step_id', 'node_id', 'result_case_id', 'rotation_srss'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	node_id = Column(Integer, nullable=False, server_default=text("'0'"))
	result_case_id = Column(String(10))
	envelope_method = Column(String(10))
	translation_x = Column(Numeric(20, 3))
	translation_y = Column(Numeric(20, 3))
	translation_z = Column(Numeric(20, 3))
	translation_srss = Column(Numeric(20, 3))
	rotation_xx = Column(Numeric(20, 3))
	rotation_yy = Column(Numeric(20, 3))
	rotation_zz = Column(Numeric(20, 3))
	rotation_srss = Column(Numeric(20, 3))
	translation_xy = Column(Numeric(20, 3))

class NodeReac(Base):
	__tablename__ = 'node_reaction'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'node_id'),
		Index('node_reaction_node_moment_xx_index', 'node_id', 'moment_xx'),
		Index('node_reaction_node_MOM_index', 'node_id', 'moment_srss'),
		Index('node_reaction_node_force_z_index', 'node_id', 'force_z'),
		Index('node_reaction_node_moment_yy_index', 'node_id', 'moment_yy'),
		Index('node_reaction_node_force_x_index', 'node_id', 'force_x'),
		Index('node_reaction_node_FRC_index', 'node_id', 'force_srss'),
		Index('node_reaction_node_moment_zz_index', 'node_id', 'moment_zz'),
		Index('node_reaction_node_force_y_index', 'node_id', 'force_y'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	node_id = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
	result_case_id = Column(String(10))
	force_x = Column(BIGINT)
	force_y = Column(BIGINT)
	force_z = Column(BIGINT)
	force_srss = Column(BIGINT)
	moment_xx = Column(BIGINT)
	moment_yy = Column(BIGINT)
	moment_zz = Column(BIGINT)
	moment_srss = Column(BIGINT)

class Member(Base):
	__tablename__ = 'member'
	__table_args__ = (
		PrimaryKeyConstraint('id'),
		Index('member_section_property_index', 'id', 'section_property_id'),
		Index('member_length_index', 'id', 'length'),
		Index('member_length_xy_index', 'id', 'length_xy'),
		Index('member_length_design_yy_index', 'id', 'length_design_yy'),
		Index('member_length_design_zz_index', 'id', 'length_design_zz'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))	
	system_id = Column(String(4), nullable=False)
	keyword = Column(String(20))
	id = Column(Integer, nullable=False)
	name = Column(String(10))
	colour = Column(String(10))
	material_type = Column(String(10))
	type = Column(String(10))
	section_property_id = Column(Integer)
	design = Column(Integer)
	restraint = Column(Integer)
	group = Column(Integer)
	node_id_1 = Column(Integer)
	node_id_2 = Column(Integer)
	radius = Column(Integer)
	orient_node = Column(Integer)
	orient_angle = Column(Integer)
	released = Column(String(10))
	release_1 = Column(String(10))
	release_2 = Column(String(10))
	length = Column(Integer)
	length_design_yy = Column(Integer)
	length_design_zz = Column(Integer)
	length_x = Column(Integer)
	length_y = Column(Integer)
	length_z = Column(Integer)
	length_xy = Column(Integer)

	def __init__(self, task_id, step_id, id):
		self.task_id = task_id
		self.step_id = step_id
		self.id = id

	def __repr__(self):
		return "<Member(task_id='%s', step_id='%s', id='%s', section_property_id='%s')>" % (
		self.task_id, self.step_id, self.id, self.section_property_id)

	@hybrid_property
	def gwaSet(self):		
		return ','.join(['SET',
						 'MEMB',
						 str(self.id),
						 str(self.name),
						 str(self.colour),
						 str(self.material_type),
						 str(self.type),
						 str(self.section_property_id),
						 str(self.design),
						 str(self.restraint),
						 str(self.group),
						 str(self.node_id_1),
						 str(self.node_id_2),
						 str(self.radius),
						 str(self.orient_node),
						 str(self.orient_angle),
						 str(self.released),
						 str(self.release_1),
						 str(self.release_2),
						 'NO_OFFSET'])

class List(Base):
	__tablename__ = 'list'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	keyword = Column(String(20))
	id = Column(Integer, nullable=False)
	name = Column(String(100))
	type = Column(String(10))
	description = Column(LONGBLOB(length=None))
	section_property_variance = Column(Integer)
	degrees_of_connectivity = Column(Integer)

	def __init__(self, task_id, step_id, id, name, type):
		self.task_id = task_id
		self.step_id = step_id
		self.id = id
		self.name = name
		self.type = type

	def __repr__(self):
		return "<List(task_id='%s', step_id='%s', keyword='%s', id='%s', name='%s', type='%s', description='%s')>" % (
		self.task_id, self.step_id, self.keyword, self.id, self.name, self.type, self.description)
	
	@hybrid_property
	def gwaSet(self):		
		return ','.join(['Set',
						 'LIST',
						 str(self.id),
						 self.name,
						 self.type,
						 self.description.decode("utf-8")])

class BucklingLoadFactor(Base):
	__tablename__ = 'buckling_load_factor'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'case_id'),
		Index('load_factor_index', 'task_id', 'step_id', 'case_id', 'load_factor'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	keyword = Column(String(20))
	case_id = Column(String(10), nullable=False, server_default=text("''"))
	load_factor = Column(Numeric(20, 3))
	waveform_length = Column(BIGINT)
	dallard_node_id_1 = Column(Integer)
	dallard_node_id_2 = Column(Integer)
	initial_imperfection = Column(Integer)
	dallard_qi = Column(Numeric(10, 3))

class Node(Base):
	__tablename__ = 'node'
	__table_args__ = (
		PrimaryKeyConstraint('id'),
		Index('node_id_x_index', 'id', 'x'),
		Index('node_id_y_index', 'id', 'y'),
		Index('node_id_z_index', 'id', 'z'),
		Index('node_id_diameter_index', 'id', 'diameter'),
		Index('node_id_task_id_step_id_x_index', 'task_id', 'step_id', 'id', 'x'),
		Index('node_id_task_id_step_id_y_index', 'task_id', 'step_id', 'id', 'y'),
		Index('node_id_task_id_step_id_z_index', 'task_id', 'step_id', 'id', 'z'),
		Index('node_id_xyz_index', 'id', 'x', 'y', 'z'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	system_id = Column(String(4))
	keyword = Column(String(20))
	id = Column(Integer, nullable=False, server_default=text("'0'"))
	x = Column(Numeric(20, 3))
	y = Column(Numeric(20, 3))
	z = Column(Numeric(20, 3))
	axis = Column(Numeric(20, 3))
	rx = Column(Numeric(20, 3))
	ry = Column(Numeric(20, 3))
	rz = Column(Numeric(20, 3))
	rxx = Column(Numeric(20, 3))
	ryy = Column(Numeric(20, 3))
	rzz = Column(Numeric(20, 3))
	Kx = Column(Numeric(20, 3))
	Ky = Column(Numeric(20, 3))
	Kz = Column(Numeric(20, 3))
	Kxx = Column(Numeric(20, 3))
	Kyy = Column(Numeric(20, 3))
	Kzz = Column(Numeric(20, 3))
	weight = Column(Numeric(20, 3))
	diameter = Column(Integer)

class NodeLoad(Base):
	__tablename__ = 'node_load'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'name'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	keyword = Column(String(10))
	name = Column(String(100))
	node_ids = Column(String(1000))
	load_case_id = Column(Integer)
	axis = Column(String(20))
	direction = Column(String(10))
	value = Column(Numeric(20, 3))

	def __repr__(self):
		return "<NodeLoad(task_id='%s', step_id='%s', keyword='%s', name='%s', node_ids='%s', load_case_id='%s', value='%s')>" % (
		self.task_id, self.step_id, self.keyword, self.name, self.node_ids, self.load_case_id, self.value)

	def __init__(self, task_id, step_id, name, load_case_id):
		self.task_id = task_id
		self.step_id = step_id
		self.name = name
		self.load_case_id = load_case_id
		axis = 'GLOBAL'
		direction = 'Z'
		value = 0

	@hybrid_property
	def gwaSet(self):		
		return ','.join(['Set',
						'LOAD_NODE.2',
						self.name,
						self.node_ids,
						str(self.load_case_id),
						self.axis,
						self.direction,
						str(self.value)])

class DynamicResponseTask(Base):
	__tablename__ = 'dynamic_response_task'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	keyword = Column(String(10))
	id = Column(Integer)
	name = Column(String(100))
	stage = Column(Integer)
	engine = Column(String(100))
	solver = Column(String(100))
	mode_task = Column(Integer)
	comb = Column(String(100))
	axis = Column(Integer)
	include_x = Column(String(100))
	spectrum_x = Column(Integer)
	modes_x = Column(String(100))
	include_y = Column(String(100))
	spectrum_y = Column(Integer)
	modes_y = Column(String(100))
	include_z = Column(String(100))
	spectrum_z = Column(Integer)
	modes_z = Column(String(100))
	rigorous = Column(String(100))
	rigid = Column(String(100))
	f_rigid = Column(Numeric(20, 3))
	f1 = Column(Numeric(20, 3))
	f2 = Column(Numeric(20, 3))
	damp_calc = Column(String(100))
	damp_scale = Column(String(100))
	damping = Column(Numeric(20, 3))
	result = Column(String(100))
	source = Column(Integer)
	drift = Column(Integer)
	disp = Column(Integer)

class AnalysisCase(Base):
	__tablename__ = 'analysis_case'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	keyword = Column(String(10))
	id = Column(Integer)
	name = Column(String(40))
	analysis_task_id = Column(Integer)
	description = Column(String(20))

class AnalysisTask(Base):
	__tablename__ = 'analysis_task'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	keyword = Column(String(10))
	id = Column(Integer)
	name = Column(String(40))
	stage = Column(Integer)
	solver = Column(String(20))

class CombinationCase(Base):
	__tablename__ = 'combination_case'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	keyword = Column(String(11))
	id = Column(Integer)
	name = Column(String(300))
	description = Column(String(300))
	bridge = Column(String(300))

	def __repr__(self):
		return "<CombinationCase(task_id='%s', step_id='%s', keyword='%s', id='%s', name='%s', description='%s', bridge='%s')>" % (
		self.task_id, self.step_id, self.keyword, self.id, self.name, self.description, self.bridge)

	@hybrid_property
	def gwaSet(self):		
		return ','.join(['SET',
						 'COMBINATION',
						 str(self.id),
						 self.name,
						 self.description,
						 self.bridge])

class LoadCase(Base):
	__tablename__ = 'load_case'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	keyword = Column(String(10))
	id = Column(Integer)
	name = Column(String(40))
	type = Column(String(20))
	bridge = Column(String(20))

class SED(Base):
	__tablename__ = 'strain_energy_density'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'element_id'),
		Index('strain_energy_density_element_id_sed_total_index', 'task_id', 'step_id', 'element_id', 'sed_total'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	element_id = Column(Integer, nullable=False, server_default=text("'0'"))
	position = Column(SmallInteger, nullable=False, server_default=text("'0'"))
	result_case_id = Column(String(10), nullable=False, server_default=text("''"))
	sed_total = Column(Numeric(20, 2))

class Material(Base):
	__tablename__ = 'material'
	__table_args__ = (
		PrimaryKeyConstraint('id'),
		Index('material_material_id_rho_index', 'id', 'rho'),
		{'mysql_engine':'MyISAM'}
	)

	keyword = Column(String(20))
	id = Column(Integer, autoincrement=False)
	model = Column(String(20))
	name = Column(String(20))
	colour = Column(String(20))
	type = Column(String(20))
	foo_1 = Column(String(20))
	alpha = Column(Numeric(10, 10))
	E = Column(Numeric(10, 0))
	nu = Column(Numeric(10, 2))
	rho = Column(Numeric(10, 10))
	alpha = Column(Numeric(10, 10))
	G = Column(Numeric(10, 10))
	damping_ratio = Column(Numeric(10, 2))
	foo_2 = Column(String(20))
	foo_3 = Column(String(20))
	NO_ENV = Column(String(20))

class Element(Base):
	__tablename__ = 'element'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'id'),
		Index('element_id_node_id_1_index', 'id', 'node_id_1'),
		Index('element_id_node_id_2_index', 'id', 'node_id_2'),
		Index('step_element_index', 'step_id', 'id'),
		Index('task_system_index', 'task_id', 'system_id'),
		Index('element_id_type_index', 'id', 'type'),
		Index('element_group_index', 'group_id'),
		Index('element_id_section_property_id_index', 'id', 'section_property_id'),
		Index('element_id_coordinates_1_index', 'id', 'coordinates_1'),
		Index('element_id_coordinates_2_index', 'id', 'coordinates_2'),
		Index('element_id_length_index', 'id', 'length'),
		Index('element_id_length_design_yy_index', 'id', 'length_design_yy'),
		Index('element_id_length_design_zz_index', 'id', 'length_design_zz'),
		Index('element_id_length_xy_index', 'id', 'length_xy'),
		Index('element_id_angle_of_incline_index', 'id', 'angle_of_incline'),
		Index('element_node_id_1_id_index', 'node_id_1', 'id'),
		Index('element_node_id_2_id_index', 'node_id_2', 'id'),
		Index('element_member_section_property_id_index', 'member_id', 'section_property_id'),
		Index('element_step_section_property_group_index', 'step_id', 'section_property_id', 'group_id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	system_id = Column(String(4))
	keyword = Column(String(20))
	id = Column(Integer, nullable=False, server_default=text("'0'"))
	geoname = Column(String(50))
	colour = Column(String(50))
	type = Column(String(50))
	section_property_id = Column(SmallInteger)
	group_id = Column(SmallInteger)
	node_id_1 = Column(Integer, ForeignKey("node.id"))
	node_id_2 = Column(Integer, ForeignKey("node.id"))
	orient_node = Column(Integer)
	orient_angle = Column(Numeric(3, 0))
	released = Column(String(10))
	release_1 = Column(String(10))
	release_2 = Column(String(10))
	dummy = Column(String(10))
	member_id = Column(SmallInteger)
	coordinates_1 = Column(String(50))
	coordinates_2 = Column(String(50))
	length = Column(Integer)
	length_design_yy = Column(Integer)
	length_design_zz = Column(Integer)
	length_x = Column(Integer)
	length_y = Column(Integer)
	length_z = Column(Integer)
	length_xy = Column(Integer)
	angle_of_incline = Column(Integer)

	node_1 = relationship("Node", foreign_keys=[node_id_1])
	node_2 = relationship("Node", foreign_keys=[node_id_2])

	def __repr__(self):
		return "<Element(task_id='%s', step_id='%s', id='%s', geoname='%s', colour='%s', type='%s', section_property_id='%s', group_id='%s', node_id_1='%s', node_id_2='%s', dummy='%s')>" % (
		self.task_id, self.step_id, self.id, self.geoname, self.colour, self.type, self.section_property_id, self.group_id, self.node_id_1, self.node_id_2, self.dummy)

	@hybrid_property
	def gwaSet(self):		
		return ','.join(['SET',
						 'EL',
						 str(self.id),
						 self.geoname,
						 self.colour,
						 self.type,
						 str(self.section_property_id),
						 str(self.group_id),
						 str(self.node_id_1),
						 str(self.node_id_2),
						 str(self.orient_node),
						 str(self.orient_angle),
						 str(self.released),
						 str(self.release_1),
						 str(self.release_2),
						 'NO_OFFSET',
						 str(self.dummy)])

class ResultCase(Base):
	__tablename__ = 'result_case'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'system_id', 'id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False)
	system_id = Column(String(4), nullable=False)
	id = Column(String(10), nullable=False)

	def __init__(self, task_id, system_id, id):
		self.task_id = task_id
		self.system_id = system_id
		self.id = id

class DesignSTRG(Base):
	__tablename__ = 'design_strg'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'system_id', 'element_id', 'result_case_id', 'position', 'envelope_method'),
		Index('strg_task_step_element_result_case_pos_env_index_cap', 'task_id', 'step_id', 'element_id', 'result_case_id', 'position', 'envelope_method'),
		Index('strg_task_step_system_element_util_Pt_index', 'task_id', 'step_id', 'system_id', 'element_id', 'util_Pt'),
		Index('strg_task_step_system_element_util_Pcy_index', 'task_id', 'step_id', 'system_id', 'element_id', 'util_Pcy'),
		Index('strg_task_step_system_element_util_Pcz_index', 'task_id', 'step_id', 'system_id', 'element_id', 'util_Pcz'),
		Index('strg_task_step_system_element_util_Myy_index', 'task_id', 'step_id', 'system_id', 'element_id', 'util_Myy'),
		Index('strg_task_step_system_element_util_Mzz_index', 'task_id', 'step_id', 'system_id', 'element_id', 'util_Mzz'),
		Index('strg_task_step_system_element_util_Ct_index', 'task_id', 'step_id', 'system_id', 'element_id', 'util_Ct'),
		Index('strg_task_step_system_element_util_Ccy_index', 'task_id', 'step_id', 'system_id', 'element_id', 'util_Ccy'),
		Index('strg_task_step_system_element_util_Ccz_index', 'task_id', 'step_id', 'system_id', 'element_id', 'util_Ccz'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer,  nullable=False)
	step_id = Column(Integer,  nullable=False)
	system_id = Column(String(4))
	section_type = Column(String(4))
	element_id = Column(Integer, nullable=False)
	member_id = Column(Integer, nullable=False)
	section_property_id = Column(Integer)
	result_case_id = Column(String(10), nullable=False, server_default=text("''"))
	position = Column(SmallInteger, nullable=False)
	envelope_method = Column(String(10))
	effective_length_yy = Column(Integer)
	effective_length_zz = Column(Integer)
	tension = Column(BIGINT)
	compression = Column(BIGINT)
	moment_yy = Column(BIGINT)
	moment_zz = Column(BIGINT)
	shear_y = Column(BIGINT)
	shear_z = Column(BIGINT)
	util_Pt = Column(Numeric(10, 4))
	util_Pcy = Column(Numeric(10, 4))
	util_Pcz = Column(Numeric(10, 4))
	util_Vy = Column(Numeric(10, 4))
	util_Vz = Column(Numeric(10, 4))
	util_Myy = Column(Numeric(10, 4))
	util_Mzz = Column(Numeric(10, 4))
	util_Ct = Column(Numeric(10, 4))
	util_Ccy = Column(Numeric(10, 4))
	util_Ccz = Column(Numeric(10, 4))
	design_section_property_id = Column(Integer)
	design_util_Pt = Column(Numeric(10, 4))
	design_util_Pcy = Column(Numeric(10, 4))
	design_util_Pcz = Column(Numeric(10, 4))
	design_util_Vy = Column(Numeric(10, 4))
	design_util_Vz = Column(Numeric(10, 4))
	design_util_Myy = Column(Numeric(10, 4))
	design_util_Mzz = Column(Numeric(10, 4))
	design_util_Ct = Column(Numeric(10, 4))
	design_util_Ccy = Column(Numeric(10, 4))
	design_util_Ccz = Column(Numeric(10, 4))

class Chart(Base):
	__tablename__ = 'chart'
	__table_args__ = (
		PrimaryKeyConstraint('task_id', 'step_id', 'id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer,  nullable=False)
	step_id = Column(Integer,  nullable=False)
	id = Column(String(50), nullable=False)
	data = Column(LONGBLOB(length=None))

	def __init__(self, task_id, step_id, id, data):
		self.task_id = task_id
		self.step_id = step_id
		self.id = id
		self.data = data

class Node2Element(Base):
	__tablename__ = 'node_2_element'
	__table_args__ = (
		PrimaryKeyConstraint('node_id','element_id'),
		Index('node_id_index', 'node_id'),
		{'mysql_engine':'MyISAM'}
	)

	task_id = Column(Integer, nullable=False, server_default=text("'0'"))
	step_id = Column(Integer, nullable=False, server_default=text("'0'"))
	node_id = Column(Integer, nullable=False, server_default=text("'0'"))
	element_id = Column(Integer, nullable=False, server_default=text("'0'"))
	element_x = Column(Float(asdecimal=True), nullable=False)
	element_y = Column(Float(asdecimal=True), nullable=False)
	element_z = Column(Float(asdecimal=True), nullable=False)