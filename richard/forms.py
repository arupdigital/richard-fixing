from wtforms import TextField, validators, IntegerField, DecimalField, BooleanField, TextAreaField
from flask_wtf import Form
from flask_wtf.file import FileField, FileRequired, FileAllowed
from werkzeug import secure_filename
import pdb
import os

def FileNameAllowed(form, field):
	filename = field.data.filename
	filename_no_ext, fileExtension = os.path.splitext(filename)
	badChars = [' ', '$', '%', '.', '(', ')']
	if any(char in filename_no_ext for char in badChars):
		raise validators.ValidationError("Filenames cannot contain any of the following characters: '  ',  $,  %,  .,  (,  )")

class RequiredIf(validators.Required):
    # a validator which makes a field required if
    # another field is set and has a truthy value

    def __init__(self, other_field_name, *args, **kwargs):
        self.other_field_name = other_field_name
        super(RequiredIf, self).__init__(*args, **kwargs)

    def __call__(self, form, field):
        other_field = form._fields.get(self.other_field_name)
        if other_field is None:
            raise Exception('no field named "%s" in form' % self.other_field_name)
        if bool(other_field.data):
            super(RequiredIf, self).__call__(form, field)

class InitForm(Form):
    descr = TextAreaField('description', validators=[validators.Length(max=200)])
    gsamodel = FileField('GSA Model', validators=[
        FileAllowed(['gwb'], 'GSA Models Only!'),
        FileNameAllowed,
        validators.Required()])

class JsonForm(Form):
    json = FileField('JSON', validators=[
        FileAllowed(['json'], 'JSON only!'),
        validators.Required()])

class UploadForm(Form):
    file_upload = FileField('CSV File Upload', validators=[
        FileAllowed(['csv'], 'CSV Files Only!'),
        FileNameAllowed,
        validators.Required()])